package com.ncf.admin.service.site;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.admin.entity.BuyerGenre;
import com.ncf.admin.entity.ContentGenre;
import com.ncf.admin.entity.ExhibitorsGenre;
import com.ncf.admin.entity.Genre;
import com.ncf.admin.repository.BuyerGenreRepository;
import com.ncf.admin.repository.ContentGenreRepository;
import com.ncf.admin.repository.ExhibitorsGenreRepository;
import com.ncf.admin.repository.GenreRepository;
import com.ncf.admin.repository.specification.GenreSpecification;
import com.ncf.admin.response.CommonResponse;
import com.ncf.admin.response.result.ResultCode;

@Service
public class GenreServiceImpl implements GenreService {

	@Autowired private GenreSpecification genreSpecification;
	@Autowired private GenreRepository genreRepository;
	@Autowired private ExhibitorsGenreRepository exhibitorsGenreRepository;
	@Autowired private BuyerGenreRepository buyerGenreRepository;
	@Autowired private ContentGenreRepository contentGenreRepository;
	
	@Override
	public List<Genre> rp(int page, int count, String sort, String record, String search, int status) {
		if (page != 0) page = (page * count);
		return genreSpecification.rp(page, count, sort, record, search, status);
	}
	
	@Override
	public Genre ro(Long gId) {
		return genreRepository.findByGid(gId);
	}

	@Override
	public CommonResponse cud(HttpServletRequest request) {
		if (request.getParameter("cudType") == null) return CommonResponse.of("data processing type is null", ResultCode.FAIL);
		if (request.getParameter("cudType").equals("create")) {
			String name = request.getParameter("name");
			String engName = request.getParameter("engName");
			int maxSort = genreSpecification.max("sort");
			System.out.println("maxSort : " + maxSort);
			Genre g = new Genre();
			g.setName(name);
			g.setEngName(engName);
			g.setSort((maxSort+1));
			genreRepository.save(g);
			return CommonResponse.of("등록 되었습니다.", ResultCode.SUCCESS);
		} else if (request.getParameter("cudType").equals("update") || request.getParameter("cudType").equals("column") || request.getParameter("cudType").equals("delete")) {
			if (request.getParameter("gId") == null) return CommonResponse.of("genre id is null", ResultCode.FAIL);
			Genre g = genreRepository.findByGid(Long.parseLong(request.getParameter("gId")));
			if (g == null) return CommonResponse.of("category not found", ResultCode.FAIL);
			if (request.getParameter("cudType").equals("update")) {
				if (request.getParameter("name") == null) return CommonResponse.of("parameter name is null", ResultCode.FAIL);
				g.setName(request.getParameter("name"));
				g.setEngName(request.getParameter("engName"));
				g.setUpdateDate(new Date());
				genreRepository.save(g);
				return CommonResponse.of("수정 되었습니다.", ResultCode.SUCCESS);
			} else if (request.getParameter("cudType").equals("column")) {
				if (request.getParameter("key") == null) return CommonResponse.of("parameter key is null", ResultCode.FAIL);
				if (request.getParameter("value") == null) return CommonResponse.of("parameter value is null", ResultCode.FAIL);
				if (request.getParameter("key").equals("status")) {
					g.setStatus(Integer.parseInt(request.getParameter("value")));
					g.setUpdateDate(new Date());
					genreRepository.save(g);
					return CommonResponse.of("수정 되었습니다.", ResultCode.SUCCESS);
				} else {
					return CommonResponse.of("please use the normal path", ResultCode.FAIL);
				}
			} else { // delete
				List<ExhibitorsGenre> egList = exhibitorsGenreRepository.findByGenre(g);
				List<BuyerGenre> bgList = buyerGenreRepository.findByGenre(g);
				List<ContentGenre> cgList = contentGenreRepository.findByGenre(g);
				for (ExhibitorsGenre eg : egList) {
					exhibitorsGenreRepository.delete(eg);
				}
				for (BuyerGenre bg : bgList) {
					buyerGenreRepository.delete(bg);
				}
				for (ContentGenre cg : cgList) {
					contentGenreRepository.delete(cg);
				}
				genreRepository.delete(g);
				return CommonResponse.of("삭제 되었습니다.", ResultCode.SUCCESS);
			}
		} else {
			return CommonResponse.of("invalid data processing type", ResultCode.FAIL);
		}
	}
	
	@Override
	public CommonResponse sort(Long gId, String upDown) {
		Genre g = genreRepository.findByGid(gId);
		if (upDown.equals("up")) {
			if (g.getSort() == 1L) {
				return CommonResponse.of("더이상 올릴 수 없습니다.", ResultCode.FAIL);
			} else {
				Genre before = genreRepository.findBySort((g.getSort()-1));
				g.setSort((g.getSort()-1));
				before.setSort((before.getSort()+1));
				genreRepository.save(g);
				genreRepository.save(before);
				return CommonResponse.of("success", ResultCode.SUCCESS);
			}
		} else {
			Genre after = genreRepository.findBySort((g.getSort()+1));
			if (after == null) {
				return CommonResponse.of("더이상 내릴 수 없습니다.", ResultCode.FAIL);
			} else {
				g.setSort((g.getSort()+1));
				after.setSort((after.getSort()-1));
				genreRepository.save(g);
				genreRepository.save(after);
				return CommonResponse.of("success", ResultCode.SUCCESS);
			}
		}
	}

}
