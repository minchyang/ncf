package com.ncf.admin.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.admin.entity.ContactUs;
import com.ncf.admin.entity.Role;
import com.ncf.admin.repository.ContactUsRepository;
import com.ncf.admin.repository.specification.ContactUsSpecification;
import com.ncf.admin.response.CommonResponse;
import com.ncf.admin.response.result.ResultCode;

@Service
public class ContactUsServiceImpl implements ContactUsService {

	@Autowired private ContactUsSpecification contactUsSpecification;
	@Autowired private ContactUsRepository contactUsRepository;
	
	@Override
	public CommonResponse cud(HttpServletRequest request) {
		
		if (request.getParameter("cudType").equals("delete")) {
			Long cId = Long.parseLong(request.getParameter("cId"));
			ContactUs c = contactUsRepository.findByCid(cId);
			contactUsRepository.delete(c);
		}
		
		return CommonResponse.of("삭제 되었습니다.", ResultCode.SUCCESS);
	}
	
	@Override
	public ContactUs ro(Long cId) {
		return contactUsRepository.findByCid(cId);
	}
	
	@Override
	public List<ContactUs> vrp(int page, int count, String sort, String record, String search, int status, String role) {
		if (page != 0) page = (page * count);
		Role r = Role.VISITOR;
		return contactUsSpecification.vrp(page, count, sort, record, search, status, r);
	}
	
	@Override
	public List<ContactUs> erp(int page, int count, String sort, String record, String search, int status,
			String role) {
		if (page != 0) page = (page * count);
		Role r = Role.EXHIBITORS;
		return contactUsSpecification.erp(page, count, sort, record, search, status, r);
	}

	@Override
	public List<ContactUs> brp(int page, int count, String sort, String record, String search, int status,
			String role) {
		if (page != 0) page = (page * count);
		Role r = Role.BUYER;
		return contactUsSpecification.brp(page, count, sort, record, search, status, r);
	}

}
