package com.ncf.admin.service.site;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.admin.entity.Category;
import com.ncf.admin.entity.ExhibitorsCategory;
import com.ncf.admin.repository.CategoryRepository;
import com.ncf.admin.repository.ExhibitorsCategoryRepository;
import com.ncf.admin.repository.specification.CategorySpecification;
import com.ncf.admin.response.CommonResponse;
import com.ncf.admin.response.result.ResultCode;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired private CategorySpecification categorySpecification;
	@Autowired private CategoryRepository categoryRepository;
	@Autowired private ExhibitorsCategoryRepository exhibitorsCategoryRepository;
	
	@Override
	public Category ro(Long cId) {
		return categoryRepository.findByCid(cId);
	}
	
	@Override
	public List<Category> rp(int page, int count, String sort, String record, String search, int status) {
		if (page != 0) page = (page * count);
		return categorySpecification.rp(page, count, sort, record, search, status);
	}
	
	@Override
	public CommonResponse cud(HttpServletRequest request) {
		if (request.getParameter("cudType") == null) return CommonResponse.of("data processing type is null", ResultCode.FAIL);
		if (request.getParameter("cudType").equals("create")) {
			String name = request.getParameter("name");
			Category c = new Category();
			int maxSort = categorySpecification.max("sort");
			c.setName(name);
			c.setSort((maxSort+1));
			categoryRepository.save(c);
			return CommonResponse.of("등록 되었습니다.", ResultCode.SUCCESS);
		} else if (request.getParameter("cudType").equals("update") || request.getParameter("cudType").equals("column") || request.getParameter("cudType").equals("delete")) {
			if (request.getParameter("cId") == null) return CommonResponse.of("category id is null", ResultCode.FAIL);
			Category c = categoryRepository.findByCid(Long.parseLong(request.getParameter("cId")));
			if (c == null) return CommonResponse.of("category not found", ResultCode.FAIL);
			if (request.getParameter("cudType").equals("update")) {
				if (request.getParameter("name") == null) return CommonResponse.of("parameter name is null", ResultCode.FAIL);
				c.setName(request.getParameter("name"));
				c.setUpdateDate(new Date());
				categoryRepository.save(c);
				return CommonResponse.of("수정 되었습니다.", ResultCode.SUCCESS);
			} else if (request.getParameter("cudType").equals("column")) {
				if (request.getParameter("key") == null) return CommonResponse.of("parameter key is null", ResultCode.FAIL);
				if (request.getParameter("value") == null) return CommonResponse.of("parameter value is null", ResultCode.FAIL);
				if (request.getParameter("key").equals("status")) {
					c.setStatus(Integer.parseInt(request.getParameter("value")));
					c.setUpdateDate(new Date());
					categoryRepository.save(c);
					return CommonResponse.of("수정 되었습니다.", ResultCode.SUCCESS);
				} else {
					return CommonResponse.of("please use the normal path", ResultCode.FAIL);
				}
			} else { // delete
				List<ExhibitorsCategory> list = exhibitorsCategoryRepository.findByCategory(c);
				for (ExhibitorsCategory ec : list) {
					exhibitorsCategoryRepository.delete(ec);
				}
				categoryRepository.delete(c);
				return CommonResponse.of("삭제 되었습니다.", ResultCode.SUCCESS);
			}
			
		} else {
			return CommonResponse.of("invalid data processing type", ResultCode.FAIL);
		}
	}
	
	@Override
	public CommonResponse sort(Long cId, String upDown) {
		Category c = categoryRepository.findByCid(cId);
		if (upDown.equals("up")) {
			if (c.getSort() == 1L) {
				return CommonResponse.of("더이상 올릴 수 없습니다.", ResultCode.FAIL);
			} else {
				Category before = categoryRepository.findBySort((c.getSort()-1));
				c.setSort((c.getSort()-1));
				before.setSort((before.getSort()+1));
				categoryRepository.save(c);
				categoryRepository.save(before);
				return CommonResponse.of("success", ResultCode.SUCCESS);
			}
		} else {
			Category after = categoryRepository.findBySort((c.getSort()+1));
			if (after == null) {
				return CommonResponse.of("더이상 내릴 수 없습니다.", ResultCode.FAIL);
			} else {
				c.setSort((c.getSort()+1));
				after.setSort((after.getSort()-1));
				categoryRepository.save(c);
				categoryRepository.save(after);
				return CommonResponse.of("success", ResultCode.SUCCESS);
			}
		}
	}

}
