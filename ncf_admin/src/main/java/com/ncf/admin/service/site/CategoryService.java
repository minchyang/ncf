package com.ncf.admin.service.site;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.ncf.admin.entity.Category;
import com.ncf.admin.response.CommonResponse;

public interface CategoryService {

	public List<Category> rp(int page, int count, String sort, String record, String search, int status);
	public Category ro(Long cId);
	public CommonResponse cud(HttpServletRequest request);
	public CommonResponse sort(Long cId, String upDown);
	
}
