package com.ncf.admin.service.site;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.ncf.admin.entity.Genre;
import com.ncf.admin.response.CommonResponse;

public interface GenreService {

	public List<Genre> rp(int page, int count, String sort, String record, String search, int status);
	public Genre ro(Long gId);
	public CommonResponse cud(HttpServletRequest request);
	public CommonResponse sort(Long gId, String upDown);
	
}
