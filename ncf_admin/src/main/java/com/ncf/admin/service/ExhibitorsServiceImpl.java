package com.ncf.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.admin.entity.Exhibitors;
import com.ncf.admin.repository.ExhibitorsRepository;

@Service
public class ExhibitorsServiceImpl implements ExhibitorsService {

	@Autowired private ExhibitorsRepository exhibitorsRepository;
	
	@Override
	public Exhibitors ro(Long eId) {
		return exhibitorsRepository.findByEid(eId);
	}

}
