package com.ncf.admin.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.admin.annotation.NoLogging;
import com.ncf.admin.entity.Buyer;
import com.ncf.admin.entity.Exhibitors;
import com.ncf.admin.entity.Role;
import com.ncf.admin.entity.User;
import com.ncf.admin.entity.UserRole;
import com.ncf.admin.repository.BuyerRepository;
import com.ncf.admin.repository.ExhibitorsRepository;
import com.ncf.admin.repository.UserRepository;
import com.ncf.admin.repository.UserRoleRepository;
import com.ncf.admin.repository.specification.UserSpecification;
import com.ncf.admin.response.CommonResponse;
import com.ncf.admin.response.UserResponse;
import com.ncf.admin.response.result.ResultCode;
import com.ncf.admin.security.auth.JwtAuthenticationToken;
import com.ncf.admin.security.model.UserContext;

@Service
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class UserServiceImpl implements UserService {

	@Autowired private UserSpecification userSpecification;
	@Autowired private UserRepository userRepository;
	@Autowired private UserRoleRepository userRoleRepository;
	@Autowired private ExhibitorsRepository exhibitorsRepository;
	@Autowired private BuyerRepository buyerRepository;

	@NoLogging
	@Override
	public UserResponse findEmail(String name, String phone) {
		return UserResponse.of("User Not Found", ResultCode.FAIL, null);
	}

	@NoLogging
	@Override
	public UserResponse findOne(JwtAuthenticationToken token) {
		UserContext userContext = (UserContext)token.getPrincipal();
		User user = userRepository.findByUserId(userContext.getUserId());
		
		if (user != null) return UserResponse.of("success", ResultCode.SUCCESS, user);
		else return UserResponse.of("failed", ResultCode.FAIL, null);
	}

	@NoLogging
	@Override
	public User findByUserId(String userId) {
		return userRepository.findByUserId(userId);
	}

	@Override
	public CommonResponse passwordCheck(String password) {
		User user = userRepository.findByUserId("admin");
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		if (passwordEncoder.matches(password, user.getPassword())) return CommonResponse.of("새 비밀번호를 입력 해주세요.", ResultCode.SUCCESS);
		else return CommonResponse.of("기존 비밀번호를 정확히 입력 해주세요.", ResultCode.FAIL);
	}
	
	@Override
	public List<User> rp(int page, int count, String sort, String record, String search, int status, String role) {
		if (page != 0) page = (page * count);
		int roleType = 0;
		if (role.equals("exhibitors")) roleType = 1;
		else if (role.equals("buyer")) roleType = 2;
		else roleType = 0;
		return userSpecification.rp(page, count, sort, record, search, status, roleType);
	}
	
	@Override
	public CommonResponse cud(MultipartHttpServletRequest request) {
		if (request.getParameter("cudType") == null) return CommonResponse.of("데이터 처리 유형이 빈값입니다.", ResultCode.FAIL);
		if (request.getParameter("cudType").equals("column")) {
			if (request.getParameter("userId") == null) return CommonResponse.of("회원 아이디가 빈값입니다.", ResultCode.FAIL);
			User user = userRepository.findByUserId(request.getParameter("userId"));
			if (user == null) return CommonResponse.of("존재하지 않는 회원입니다.", ResultCode.FAIL);
			if (request.getParameter("key").equals("status")) {
				user.setStatus(Integer.parseInt(request.getParameter("value")));
				if (user.getExhibitors() != null) {
					Exhibitors e = user.getExhibitors();
					e.setStatus(Integer.parseInt(request.getParameter("value")));
					exhibitorsRepository.save(e);
				} else if (user.getBuyer() != null) {
					Buyer b = user.getBuyer();
					b.setStatus(Integer.parseInt(request.getParameter("value")));
					buyerRepository.save(b);
				}
			}
			if (request.getParameter("key").equals("role")) {
				userRoleRepository.delete(user.getRoles().get(0));
				UserRole.Id userRoleId = null;
				UserRole ur = new UserRole();
				List<UserRole> urs = new ArrayList<>();
				if (request.getParameter("value").equals("exhibitors")) {
					userRoleId = new UserRole.Id(user.getUserId(), Role.EXHIBITORS);
					Exhibitors e = user.getExhibitors();
					e.setStatus(0);
					exhibitorsRepository.save(e);
				} else {
					userRoleId = new UserRole.Id(user.getUserId(), Role.BUYER);
					Buyer b = user.getBuyer();
					b.setStatus(0);
					buyerRepository.save(b);
				}
				ur.setId(userRoleId);
				urs.add(ur);
				userRoleRepository.save(ur);
				user.setRoles(urs);
			}
			userRepository.save(user);
			return CommonResponse.of("수정 되었습니다.", ResultCode.SUCCESS);
		} else {
			return CommonResponse.of("올바른 경로로 접근 해주세요.", ResultCode.FAIL);
		}
	}
	
	@Override
	public User findByUserIdAndRole(String userId, String strRole) {
		List<Role> roles = new ArrayList<>();
		if (strRole.equals("buyer")) {
			roles.add(Role.BUYER);
			roles.add(Role.WAIT);
		} else if (strRole.equals("exhibitors")) {
			roles.add(Role.EXHIBITORS);
			roles.add(Role.WAIT);
		} else {
			roles.add(Role.VISITOR);
		}
		return userRepository.findByUserIdAndRole(userId, roles);
	}
	
}
