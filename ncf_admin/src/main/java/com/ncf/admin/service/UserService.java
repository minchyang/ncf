package com.ncf.admin.service;

import java.util.List;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.admin.annotation.NoLogging;
import com.ncf.admin.entity.User;
import com.ncf.admin.response.CommonResponse;
import com.ncf.admin.response.UserResponse;
import com.ncf.admin.security.auth.JwtAuthenticationToken;

public interface UserService {
	
	@NoLogging
	public User findByUserId(String userId);
	
	@NoLogging
	public UserResponse findOne(JwtAuthenticationToken token);
	
	@NoLogging
    public UserResponse findEmail(String name, String phone);
	
	public CommonResponse passwordCheck(String password);
	public List<User> rp(int page, int count, String sort, String record, String search, int status, String role);
	public CommonResponse cud(MultipartHttpServletRequest request);
	public User findByUserIdAndRole(String userId, String role);
	
}
