package com.ncf.admin.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.ncf.admin.entity.ContactUs;
import com.ncf.admin.response.CommonResponse;

public interface ContactUsService {

	public List<ContactUs> vrp(int page, int count, String sort, String record, String search, int status, String role);
	public List<ContactUs> erp(int page, int count, String sort, String record, String search, int status, String role);
	public List<ContactUs> brp(int page, int count, String sort, String record, String search, int status, String role);
	public CommonResponse cud(HttpServletRequest request);
	public ContactUs ro(Long cId);
	
}
