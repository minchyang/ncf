package com.ncf.admin.common;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public final class DateUtil {
	private static TimeZone timeZone;

	static {
		try {
			timeZone = TimeZone.getTimeZone("GMT+09:00");
		} catch (Exception e) {
		}
	}

	public static Date getDate() {

		Calendar cal = Calendar.getInstance(timeZone, Locale.KOREAN);

		return cal.getTime();
	}

	public static Date getDate(long offset) {

		Calendar cal = Calendar.getInstance(timeZone, Locale.KOREAN);
		cal.setTime(new Date(cal.getTime().getTime() + (offset * 1000)));

		return cal.getTime();
	}

	public static Date getDate(Date date, long offset) {

		return new Date(date.getTime() + (offset * 1000));
	}

	public static String getDateString(String format) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat(format);
		simpleFormat.setTimeZone(timeZone);
		return simpleFormat.format(getDate());
	}

	public static String getDateString() {
		return getDateString("yyyy-MM-dd HH:mm:ss");
	}

	public static long getDateLong(String format) {

		SimpleDateFormat simpleFormat = new SimpleDateFormat(format);
		simpleFormat.setTimeZone(timeZone);
		return Long.parseLong(simpleFormat.format(getDate()));
	}

	public static long getDateLong() {
		return getDateLong("yyyyMMddHHmmss");
	}

	public static long getDateLongS() {
		return getDateLong("yyyyMMddHHmmssSSS");
	}

	public static Date getDate(int year, int month, int day, int hour,
			int minute, int second) {

		GregorianCalendar cal = new GregorianCalendar(timeZone, Locale.KOREAN);
		cal.set(year, month - 1, day, hour, minute, second);
		return cal.getTime();
	}

	public static String dateToString(Date date, String format) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat(format);
		simpleFormat.setTimeZone(timeZone);
		return simpleFormat.format(date);
	}

	public static String getDateToString(String format) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat(format);
		Calendar now = Calendar.getInstance(timeZone, Locale.KOREAN);
		return simpleFormat.format(now.getTime());
	}

	public static Date stringToDate(String dateString, String format)
			throws ParseException {

		SimpleDateFormat simpleFormat = new SimpleDateFormat(format);
		simpleFormat.setTimeZone(timeZone);
		return simpleFormat.parse(dateString);
	}

	public static long dateToLong(Date date, String format) {

		SimpleDateFormat simpleFormat = new SimpleDateFormat(format);
		simpleFormat.setTimeZone(timeZone);
		return Long.parseLong(simpleFormat.format(date));
	}

	public static Date longToDate(long dateLong, String format)
			throws ParseException {

		SimpleDateFormat simpleFormat = new SimpleDateFormat(format);
		simpleFormat.setTimeZone(timeZone);
		return simpleFormat.parse(Long.toString(dateLong));
	}

	public static String longToString(long dateLong, String format)
			throws ParseException {
		return dateToString(longToDate(dateLong, "yyyyMMddHHmmss"), format);
	}

	public static int getAfterDays(Date date1, Date date2) {

		return (int) ((date1.getTime() - date2.getTime()) / 86400000);
	}

	public static int getAfterSeconds(Date date1, Date date2) {

		return (int) ((date1.getTime() - date2.getTime()) / 1000);
	}

	public static int getAfterMilliSeconds(Date date1, Date date2) {

		return (int) (date1.getTime() - date2.getTime());
	}

	/**
	 * �ش� ���� ù ��° ��¥�� ���Ѵ�.
	 * @param year
	 * @param month
	 * @param format
	 * @return
	 */
	public static String getCurMonthFirstDate(String year, String month,
			String format) {

		Calendar cal = Calendar.getInstance(timeZone, Locale.KOREAN);

		int curYear = Integer.parseInt(year);
		int curMonth = Integer.parseInt(month);

		cal.set(curYear, curMonth - 1, 1);
		int curMinDay = cal.getActualMinimum(Calendar.DATE);

		Date curDate = DateUtil.getDate(curYear, curMonth, curMinDay, 0, 0, 0);

		return DateUtil.dateToString(curDate, format);
	}

	/**
	 * ���� ������ ���Ѵ�.
	 * @return
	 */
	public static String getDay()
	{
		Calendar cal = Calendar.getInstance(timeZone, Locale.KOREAN);
		int curDay = cal.get(Calendar.DAY_OF_WEEK);
		String[] days = {"", "��", "��", "ȭ", "��", "��", "��", "��"};
		return days[curDay];
	}

	/**
	 * ���� ������ ���ڷ� ���Ѵ�.
	 * @return
	 */
	public static int getIntDay()
	{
		Calendar cal = Calendar.getInstance(timeZone, Locale.KOREAN);
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	public static boolean isPastDay(String date) {
		boolean result = false;
		try {
			Date pdate = DateUtil.stringToDate(date, "yyyyMMddHHmmss");
			Date cdate = DateUtil.getDate();
			int days = DateUtil.getAfterDays(cdate, pdate);
			if (days > 3) {
				result = true;
			} else {
				result = false;
			}
		} catch (Exception e) {
		}
		return result;
	}



	public static void main(String[] args) throws Exception {
//		System.out.println(getAfterDays(1));
//		System.out.println(passwordEncoding.encode("appletest"));
		String file= "/review/1118/review_1031174711481.jpg";
		String[] fileName = file.split("[.]");
		System.out.println(fileName[0] + "_thumb." + fileName[1]);
//		
		//KakaoClient client = new KakaoClient(BeautyConstants.KAKO_API_KEY);
		//client.getUserData("VbVSEQF6uxTwCxkiYNiv9_a_1GfP0TeY-yek4AopdeIAAAFa-s6KZg");
		
		 DecimalFormat df=new DecimalFormat("#.#");
		 double number = 3;
//		 df.format(number);
		 System.out.println(df.format(number));
		//System.out.println(client.getAuth("authorization_code", "http://localhost:8000", "I5kzaKmfgQH9L3516DVniYWIaYUd49iaHjMfADhi5HYSuY3n858qls85YfVCRLRU3MVmzAopdgcAAAFa-m7p9g"));
		
//		System.out.println(getBefor30Days());
		//		String coo = "37.4787416,126.8818627";
		//		String[] split = coo.split(",");
		//		System.out.println(split[0] + " | " + split[1]);
		//	  Date dt_month = DateUtil.stringToDate("1982-02-04 12:45:00", "yyyy-MM-dd HH:mm:ss");
		//	  Date dt_year = DateUtil.stringToDate("1982-06-06 09:36:00", "yyyy-MM-dd HH:mm:ss");
		//	  System.out.println(DateUtil.getAfterSeconds(dt_year,dt_month));
		 
		 
//		 
//		 StringBuffer sb = new StringBuffer();
//			sb.append("[발그레] 모바일 앱 설치 \n");
//			sb.append("☞아이폰 http://goo.gl/VhEDb3 \n");
//			sb.append("☞안드로이드 http://goo.gl/jnVskP");
//			HashMap<String, String> set = new HashMap<String, String>();		
//			set.put("to", "01091549861"); // 수신번호
//			set.put("from", "028557976"); // 발신번호
//			set.put("text", sb.toString()); // 문자내용
//			set.put("type", "lms"); // 문자 타입
//
//			SmsResult smsResult = sendSms(set);
//
//
//			set.clear();
//			set = null;
	}
//	public static SmsResult sendSms(HashMap<String, String> set) {
//		/*
//		 * 서버에서 받은 API_KEY, API_SECRET를 입력해주세요.
//		 */
//		Coolsms coolsms = new Coolsms("NCS578ED45B4F58E", "286732C34A302048B87EBB3EEF1E7388");
//		JSONObject result = coolsms.send(set); // 보내기&전송결과받기
//
//		SmsResult smsResult = new SmsResult();
//		if ((boolean) result.get("status")) {
//			// 메시지 보내기 성공 및 전송결과 출력
//			System.out.println("성공");            
//			System.out.println(result.get("group_id")); // 그룹아이디
//			System.out.println(result.get("result_code")); // 결과코드
//			System.out.println(result.get("result_message"));  // 결과 메시지
//			System.out.println(result.get("success_count")); // 성공 수
//			System.out.println(result.get("error_count"));  // 여러개 보낼시 오류난 메시지 수
//			 
//			smsResult.setGroupId(result.get("group_id").toString());
//			smsResult.setResultCode(result.get("result_code").toString());
//			smsResult.setResultMessage(result.get("result_message").toString());
//
//		} else {
//			// 메시지 보내기 실패
//			
//			System.out.println("실패");
//			System.out.println(result.get("code")); // REST API 에러코드
//			System.out.println(result.get("message")); // 에러메시지
//			 
//			smsResult.setGroupId("");
//			smsResult.setResultCode(result.get("code").toString());
//			smsResult.setResultMessage(result.get("message").toString());
//		}        
//		return smsResult;
//	}
	public static String getAfterYears(int year) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyyMMddHHmm", Locale.KOREA);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.YEAR, year);
		return simpleFormat.format(now.getTime());
	}

	public static String getAfterYears(int year, String format) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat(format, Locale.KOREA);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.YEAR, year);
		return simpleFormat.format(now.getTime());
	}

	public static String getAfterMonths(int month) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyyMMddHHmm", Locale.KOREA);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MONTH, month);
		return simpleFormat.format(now.getTime());
	}

	public static String getAfterMonths(int month, String format) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat(format, Locale.KOREA);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MONTH, month);
		return simpleFormat.format(now.getTime());
	}

	public static Date getBefor30Days() {
		Calendar cal = Calendar.getInstance();
		cal.add(cal.MONTH, -1);
		cal.get(cal.DATE);

		//		java.util.Date weekago = cal.getTime();
		//		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",
		//				Locale.getDefault());
		return cal.getTime();
		//		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyyMMddHHmm", Locale.KOREA);
		//		Calendar now = Calendar.getInstance();
		//		now.add(Calendar.DATE, day);
		//		return simpleFormat.format(now.getTime());
	}
	
	/**
	 * ���� ��¥�� ����, day�� ���� ��¥�� ����
	 * @param day
	 * @return
	 */
	public static String getAfterDays(int day) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyyMMddHHmm", Locale.KOREA);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, day);
		return simpleFormat.format(now.getTime());
	}

	public static String getAfterDays(int day, String format) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat(format, Locale.KOREA);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, day);
		return simpleFormat.format(now.getTime());
	}

	public static String getAfterHours(int hour) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyyMMddHHmm", Locale.KOREA);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.HOUR_OF_DAY, hour);
		return simpleFormat.format(now.getTime());
	}

	public static String getAfterMinute(int minute) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyyMMddHHmm", Locale.KOREA);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MINUTE, minute);
		return simpleFormat.format(now.getTime());
	}

	public static String getyyyyMMddHHmmssSSS() {
		return getDateString("yyyyMMddHHmmssSSS");
	}

	public boolean isHoliday(Date date) 
	{
		boolean isHoliday = false;

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
				|| cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			isHoliday = true;
		}

		// Now write logic to check the date for potential
		// matches among a list of public holidays stored
		// in an external location
		return isHoliday;
	}

	public static boolean isHoliday() 
	{
		boolean isHoliday = false;

		Calendar cal = Calendar.getInstance();

		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
				|| cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			isHoliday = true;
		}

		// Now write logic to check the date for potential
		// matches among a list of public holidays stored
		// in an external location
		return isHoliday;
	}

	/**
	 * Calendar���� YYYYMMDDH24MISSMMM ����
	 * 
	 * @param c
	 *            Calendar
	 * @param isMillisecond
	 *            Millisecond �߰� ����
	 * @return YYYYMMDDH24MISSMMM
	 */
	public static String toDTime(Calendar c, boolean isMillisecond) {

		StringBuffer sb = new StringBuffer(17);

		/** �� */
		if (c.get(Calendar.YEAR) < 10)
			sb.append('0');
		sb.append(c.get(Calendar.YEAR));

		/** �� */
		if (c.get(Calendar.MONTH) + 1 < 10)
			sb.append('0');
		sb.append(c.get(Calendar.MONTH) + 1);

		/** �� */
		if (c.get(Calendar.DAY_OF_MONTH) < 10)
			sb.append('0');
		sb.append(c.get(Calendar.DAY_OF_MONTH));

		/** �� */
		if (c.get(Calendar.HOUR_OF_DAY) < 10)
			sb.append('0');
		sb.append(c.get(Calendar.HOUR_OF_DAY));

		/** �� */
		if (c.get(Calendar.MINUTE) < 10)
			sb.append('0');
		sb.append(c.get(Calendar.MINUTE));

		/** �� */
		if (c.get(Calendar.SECOND) < 10)
			sb.append('0');
		sb.append(c.get(Calendar.SECOND));

		/** MILLISECOND */
		if (isMillisecond) {
			int mil = c.get(Calendar.MILLISECOND);
			if(mil == 0){
				sb.append("000");
			}else if(mil < 10){
				sb.append("00");
			}else if(mil < 100){
				sb.append("0");
			}

			sb.append(mil);
		}

		return sb.toString();
	}	

	public static Date getYesterday ( Date today )
	{
		if ( today == null ) 
			throw new IllegalStateException ( "today is null" );
		Date yesterday = new Date ( );
		yesterday.setTime ( today.getTime ( ) - ( (long) 1000 * 60 * 60 * 24 ) );

		return yesterday;
	}
	
	public static Date get7DayAgoDate() {
		Calendar cal = Calendar.getInstance(new SimpleTimeZone(0x1ee6280,  "KST"));
		cal.add(Calendar.DATE, -7);
		Date weekago = cal.getTime();
		//SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
		return weekago;
	}
	
	public static Date getDayAgoDate(int day) {
		Calendar cal = Calendar.getInstance(new SimpleTimeZone(0x1ee6280,  "KST"));
		cal.add(Calendar.DATE, day);
		Date weekago = cal.getTime();
		//SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
		return weekago;
	}
	
	public static Date getDayAgoDate2(int day) {
		Calendar cal = Calendar.getInstance(new SimpleTimeZone(0x1ee6280,  "KST"));
		cal.add(Calendar.DATE, -day);
		Date weekago = cal.getTime();
		//SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
		return weekago;
	}
	
	public static Date get6MonthAfterDate() {
		Calendar cal = Calendar.getInstance(new SimpleTimeZone(0x1ee6280,  "KST"));
		cal.add(Calendar.MONTH, +6);
		Date weekago = cal.getTime();
		//SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
		return weekago;
	}
	
	public static String getBeforeDays(int day, String format) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat(format, Locale.KOREA);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, -day);
		return simpleFormat.format(now.getTime());
	}

	public static Date getAfterDate(String date, int day, String format) throws ParseException {
		System.out.println(date);
		  SimpleDateFormat simpleFormat = new SimpleDateFormat(format); 
		  Calendar cal = Calendar.getInstance();
		  cal.setTime(stringToDate(date, format));
		  cal.add(Calendar.DATE, day);
		  System.out.println(simpleFormat.format(cal.getTime()));
		  return simpleFormat.parse(simpleFormat.format(cal.getTime()));

	}
	
}
