package com.ncf.admin.entity;

public enum Role {
	// enumerated type (서로 연관된 상수들의 집합)
    WAIT, VISITOR, EXHIBITORS, BUYER, ADMIN;
    
    public String authority() {
        return "ROLE_" + this.name();
    }
}
