package com.ncf.admin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "EXHIBITORS_CONTENT")
@ApiModel
public class ExhibitorsContent {

	@Id
	@ApiModelProperty(value = "exhibitors content id [primary key]")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ec_id")
	@Getter @Setter
	private Long ecId;
	
	@ApiModelProperty(value = "exhibitors, 참가사 [Exhibitors, entity]")
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "e_id")
	@Getter @Setter
	private Exhibitors exhibitors;
	
	@ApiModelProperty(value = "content, 컨텐츠 [Content, entity]")
	@ManyToOne
	@JoinColumn(name = "c_id")
	@Getter @Setter
	private Content content;
	
}
