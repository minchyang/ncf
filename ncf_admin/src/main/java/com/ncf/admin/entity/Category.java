package com.ncf.admin.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "CATEGORY")
@ApiModel
public class Category {

	@Id
	@ApiModelProperty(value = "category id [primary key]")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "c_id")
	@Getter @Setter
	private Long cid;
	
	@ApiModelProperty(value = "name, 이름")
	@Column(name = "name")
	@Getter @Setter
	private String name;
	
	@ApiModelProperty(value = "eng name, 영문명")
	@Column(name = "engName")
	@Getter @Setter
	private String engName;
	
	@ApiModelProperty(value = "status, 상태값 [0:사용, 1:미사용]")
	@Column(name = "status", columnDefinition = "INT(1) default '0'")
	@Getter @Setter
	private int status;
	
	@ApiModelProperty(value = "sort, 정렬")
	@Column(name = "sort")
	@Getter @Setter
	private int sort;
	
	@ApiModelProperty(value = "registration date")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "regDate")
	@Getter @Setter
	private Date regDate = new Date();
	
	@ApiModelProperty(value = "update date")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updateDate")
	@Getter @Setter
	private Date updateDate = new Date();
	
}
