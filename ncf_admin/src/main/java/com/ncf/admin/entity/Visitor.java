package com.ncf.admin.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "VISITOR")
@ApiModel
public class Visitor {

	@Id
	@ApiModelProperty(value = "visitor id [primary key]")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "v_id")
	@Getter @Setter
	private Long vid;
	
	@ApiModelProperty(value = "front phone, 휴대폰 앞자리")
	@Column(name = "frontPhone")
	@Getter @Setter
	private String frontPhone;
	
	@ApiModelProperty(value = "phone, 핸드폰번호")
	@Column(name = "phone")
	@Getter @Setter
	private String phone;
	
	@ApiModelProperty(value = "position, 소속")
	@Column(name = "position")
	@Getter @Setter
	private String position;
	
	@ApiModelProperty(value = "registration date, 등록일")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "regDate")
	@Getter @Setter
	private Date regDate = new Date();
	
}
