package com.ncf.admin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "EXHIBITORS_GENRE")
@ApiModel
public class ExhibitorsGenre { 

	@Id
	@ApiModelProperty(value = "exhibitors genre id [primary key]")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "eg_id")
	@Getter @Setter
	private Long egId;
	
	@ApiModelProperty(value = "exhibitors, 참가사 [Exhibitors, entity]")
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "e_id")
	@Getter @Setter
	private Exhibitors exhibitors;
	
	@ApiModelProperty(value = "genre, 장르 [Genre, entity]")
	@ManyToOne
	@JoinColumn(name = "g_id")
	@Getter @Setter
	private Genre genre;
	
}
