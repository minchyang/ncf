package com.ncf.admin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "BUYER_GENRE")
@ApiModel
public class BuyerGenre {

	@Id
	@ApiModelProperty(value = "buyer genre id [primary key]")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "bg_id")
	@Getter @Setter
	private Long bgId;
	
	@ApiModelProperty(value = "buyer, 바이어 [Buyer, entity]")
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "b_id")
	@Getter @Setter
	private Buyer buyer;
	
	@ApiModelProperty(value = "genre, 장르 [Genre, entity]")
	@ManyToOne
	@JoinColumn(name = "g_id")
	@Getter @Setter
	private Genre genre;
	
}
