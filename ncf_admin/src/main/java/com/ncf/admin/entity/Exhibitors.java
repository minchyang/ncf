package com.ncf.admin.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "EXHIBITORS")
@ApiModel
public class Exhibitors {

	@Id
	@ApiModelProperty(value = "exhibitors id [primary key]")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "e_id")
	@Getter @Setter
	private Long eid;
	
	@ApiModelProperty(value = "logo, 로고 이미지")
	@Column(name = "logo")
	@Getter @Setter
	private String logo;
	
	@ApiModelProperty(value = "company name, 회사명")
	@Column(name = "companyName")
	@Getter @Setter
	private String companyName;
	
	@ApiModelProperty(value = "manager name, 담당자명")
	@Column(name = "managerName")
	@Getter @Setter
	private String managerName;
	
	@ApiModelProperty(value = "front phone, 휴대폰 앞자리")
	@Column(name = "frontPhone")
	@Getter @Setter
	private String frontPhone;
	
	@ApiModelProperty(value = "phone, 휴대폰 번호")
	@Column(name = "phone")
	@Getter @Setter
	private String phone;
	
	@ApiModelProperty(value = "position, 직책")
	@Column(name = "position")
	@Getter @Setter
	private String position;
	
	@ApiModelProperty(value = "national code, 국가 번호")
	@Column(name = "nationalCode")
	@Getter @Setter
	private String nationalCode;
	
	@ApiModelProperty(value = "tel, 전화번호")
	@Column(name = "tel")
	@Getter @Setter
	private String tel;
	
	@ApiModelProperty(value = "site, 사이트")
	@Column(name = "site")
	@Getter @Setter
	private String site;
	
	@ApiModelProperty(value = "establish year, 설립년")
	@Column(name = "establishYear", length = 4)
	@Getter @Setter
	private String establishYear;
	
	@ApiModelProperty(value = "establish month, 설립월")
	@Column(name = "establishMonth", length = 2)
	@Getter @Setter
	private String establishMonth;
	
	@ApiModelProperty(value = "establish day, 설립일")
	@Column(name = "establishDay", length = 2)
	@Getter @Setter
	private String establishDay;
	
	@ApiModelProperty(value = "ceo name, 대표자명")
	@Column(name = "ceoName")
	@Getter @Setter
	private String ceoName;
	
	@ApiModelProperty(value = "promotional video type, 홍보 영상 유형 [0:youtube, 1:vimeo, 2:none]")
	@Column(name = "promotionalVideoType", columnDefinition = "INT(1) default '2'")
	@Getter @Setter
	private int promotionalVideoType;
	
	@ApiModelProperty(value = "promotional video url, 홍보 영상 URL")
	@Column(name = "promotionalVideoUrl")
	@Getter @Setter
	private String promotionalVideoUrl;
	
	@ApiModelProperty(value = "about us, 회사소개")
	@Lob
	@Column(name = "aboutUs")
	@Getter @Setter
	private String aboutUs;
	
	@ApiModelProperty(value = "brochure, 브로슈어")
	@Column(name = "brochure")
	@Getter @Setter
	private String brochure;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ApiModelProperty(value = "exhibitors genre, 참가사 장르 [List<ExhibitorsGenre>, Entity]")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "exhibitors")
	@Getter @Setter
	private List<ExhibitorsGenre> exhibitorsGenre;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ApiModelProperty(value = "exhibitors content, 참가사 컨텐츠 [List<ExhibitorsContent>, entity]")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "exhibitors")
	@Getter @Setter
	private List<ExhibitorsContent> exhibitorsContent;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ApiModelProperty(value = "exhibitors category, 참가사 카테고리(도시) [List<ExhibitorsCategory>, entity]]")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "exhibitors")
	@Getter @Setter
	private List<ExhibitorsCategory> exhibitorsCategory;
	
	@ApiModelProperty(value = "privacy agree, 개인정보처리 방침 동의 [0:동의, 1:미동의]")
	@Column(name = "privacyAgree", columnDefinition = "INT(1) default '0'")
	@Getter @Setter
	private int privacyAgree;
	
	@ApiModelProperty(value = "english logo, 영문 로고 이미지")
	@Column(name = "engLogo")
	@Getter @Setter
	private String engLogo;
	
	@ApiModelProperty(value = "english company name, 영문 회사명")
	@Column(name = "engCompanyName")
	@Getter @Setter
	private String engCompanyName;
	
	@ApiModelProperty(value = "english manager name, 영문 담당자명")
	@Column(name = "engManagerName")
	@Getter @Setter
	private String engManagerName;
	
	@ApiModelProperty(value = "english ceo name, 영문 대표자명")
	@Column(name = "engCeoName")
	@Getter @Setter
	private String engCeoName;
	
	@ApiModelProperty(value = "english city, 영문 도시")
	@Column(name = "engCity")
	@Getter @Setter
	private String engCity;
	
	@ApiModelProperty(value = "engAboutUs, 영문 회사소개")
	@Lob
	@Column(name = "engAboutUs")
	@Getter @Setter
	private String engAboutUs;
	
	@ApiModelProperty(value = "status [0:노출, 1:미노출]")
	@Column(name = "status", columnDefinition = "INT(1) default '1'")
	@Getter @Setter
	private int status;
	
	@ApiModelProperty(value = "registration date, 등록일")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "regDate")
	@Getter @Setter
	private Date regDate = new Date();
	
}
