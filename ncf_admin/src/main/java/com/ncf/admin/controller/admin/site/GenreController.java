package com.ncf.admin.controller.admin.site;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncf.admin.entity.Genre;
import com.ncf.admin.response.CommonResponse;
import com.ncf.admin.service.site.GenreService;

@Controller
@RequestMapping(value = "/admin/site/genre")
public class GenreController {

	@Autowired private GenreService genreService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list() {
		return "/admin/site/genre/list";
	}
	
	@RequestMapping(value = "/rp", method = RequestMethod.POST)
	@ResponseBody
	public List<Genre> rp(
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "30") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "asc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "sort") String record, 
			@RequestParam(value = "search", required = true, defaultValue = "") String search, 
			@RequestParam(value = "status", required = true, defaultValue = "0") int status) {
		return genreService.rp(page, count, sort, record, search, status);
	}
	
	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public String detail(Model model, 
			@RequestParam(value = "gId", required = false) Long cId) {
		if (cId != null) model.addAttribute("obj", genreService.ro(cId));
		return "/admin/site/genre/detail";
	}
	
	@RequestMapping(value = "/cud", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse cud(HttpServletRequest request) {
		return genreService.cud(request);
	}
	
	@RequestMapping(value = "/sort", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse sort(
			@RequestParam(value = "gId", required = true) Long cId, 
			@RequestParam(value = "upDown", required = true) String upDown) {
		return genreService.sort(cId, upDown);
	}
	
}
