package com.ncf.admin.controller.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ncf.admin.entity.Role;
import com.ncf.admin.entity.User;
import com.ncf.admin.security.model.UserContext;
import com.ncf.admin.service.UserService;

@Controller
public class MainController {

	@Autowired private UserService userService;
	
	@RequestMapping(value = {"/", "/admin/main"}, method = RequestMethod.GET)
	public String main(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!auth.getPrincipal().equals("anonymousUser")) {
			User user = userService.findByUserId(((UserContext)auth.getPrincipal()).getUserId());
			if (user != null) {
				boolean bool = false;
				for (int i=0; i<user.getRoles().size(); i++) {
					if (user.getRoles().get(i).getRole().equals(Role.ADMIN)) bool = true;
				}
				if (bool) {
					return "/admin/main/index";
				} else {
					new SecurityContextLogoutHandler().logout(request, response, auth);
					return "redirect:/login";
				}
			} else {
				new SecurityContextLogoutHandler().logout(request, response, auth);
				return "redirect:/login";
			}
		} else {
			return "redirect:/login";
		}
		
	}
	
}
