package com.ncf.admin.controller.anonymous;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ncf.admin.common.PasswordEncoding;

@Controller
@RequestMapping(value = "/login")
public class LoginController {

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String login(Model model, 
			@RequestParam(value = "error", required = false) String error) {
		model.addAttribute("error", error);
		return "/anonymous/login/login";
	}
	
}
