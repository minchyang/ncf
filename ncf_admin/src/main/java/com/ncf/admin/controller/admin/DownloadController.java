package com.ncf.admin.controller.admin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ncf.admin.entity.Exhibitors;
import com.ncf.admin.service.ExhibitorsService;

@RestController
@RequestMapping(value = "/admin/download")
public class DownloadController {

	@Autowired private ExhibitorsService exhibitorsService;
	
	@RequestMapping(value = "/brochure", method = RequestMethod.GET)
	public void download(HttpSession session, HttpServletResponse response, HttpServletRequest request, 
			@RequestParam(value = "eId", required = true) Long eId) throws Exception {
		Exhibitors ex = exhibitorsService.ro(eId);
		System.out.println(ex.getCompanyName());
		if (ex != null) {
			String filePath = "";
			filePath = "http://nexfairimg.cafe24.com"+ex.getBrochure();
			URL url = null;
	        InputStream in = null;
	        OutputStream out = null;
	        File target = new File(filePath);
	        try {
	            String fileName = target.getName();
	            String header = request.getHeader("User-Agent");
	 
	            if(header.contains("MSIE") || header.contains("Trident")) {
	                fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
	                response.setHeader("Content-Disposition", "attachment; filename="+ fileName +";");
	            } else {
	                fileName = new String(fileName.getBytes("UTF-8"), "iso-8859-1");
	                response.setHeader("Content-Disposition", "attachment; filename=\""+ fileName +"\"");
	            }
	            
	            response.setHeader("Pragma", "no-cache;");
	            response.setContentType("application/octet-stream");
	            response.setHeader("Content-Transfer-Encoding", "binary");
	            
	            out = response.getOutputStream();
	            url = new URL(filePath);
	            // 만약 프로토콜이 https 라면 https SSL을 무시하는 로직을 수행해주어야 한다.('https 인증서 무시' 라는 키워드로 구글에 검색하면 많이 나옵니다.)
	            in = url.openStream();
	 
	            while(true) {
	                int data = in.read();
	                if (data == -1) break;
	                out.write(data);
	            }
	            in.close();
	            out.close();
	        } catch (Exception e) {
	            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	        } finally {
	            if(in != null) in.close();
	            if(out != null) out.close();
	        }
		} else {
			throw new Exception();
		}
	}
	
	void setDisposition(String fileName, HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("setDisposition() start");
		String browser = getBrowser(request);
		String dispositionPrefix = "attachment; filename=";
		String encodedFileName = null;
		
		if (browser.equals("MSIE")) {
			encodedFileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
		} else if (browser.equals("Firefox")) {
			encodedFileName = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.equals("Opera")) {
			encodedFileName = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.equals("Chrome")) {
			StringBuffer sb = new StringBuffer();
			for (int i=0; i<fileName.length(); i++) {
				char c = fileName.charAt(i);
				if (c > '~') {
					sb.append(URLEncoder.encode("" + c, "UTF-8"));
				} else {
					sb.append(c);
				}
			}
			encodedFileName = sb.toString();
		} else {
			throw new IOException("Not supported browser");
		}
		
		response.setHeader("Content-Disposition", dispositionPrefix + encodedFileName);
		
		System.out.println("setDisposition() end");
		if ("Opera".equals(browser)) {
			response.setContentType("application/octet-stream;charset=UTF-8");
		}
	}
	
	private String getBrowser(HttpServletRequest request) {
		System.out.println("getBrowser() start");
		String header = request.getHeader("User-Agent");
		
		if (header.indexOf("MSIE") > -1) {
			return "MSIE";
		} else if (header.indexOf("Chrome") > -1) {
			return "Chrome";
		} else if (header.indexOf("Opera") > -1) {
			return "Opera";
		} else if (header.indexOf("Firefox") > -1) {
			return "Firefox";
		} else if (header.indexOf("Mozilla") > -1) {
			if (header.indexOf("Firefox") > -1) {
				return "Firefox";
			} else {
				return "MSIE";
			}
		}
		return "MSIE";
		
	}
	
}
