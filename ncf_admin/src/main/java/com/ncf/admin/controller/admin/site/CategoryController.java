package com.ncf.admin.controller.admin.site;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncf.admin.entity.Category;
import com.ncf.admin.response.CommonResponse;
import com.ncf.admin.service.site.CategoryService;

@Controller
@RequestMapping(value = "/admin/site/category")
public class CategoryController {

	@Autowired private CategoryService categoryService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list() {
		return "/admin/site/category/list";
	}
	
	@RequestMapping(value = "/rp", method = RequestMethod.POST)
	@ResponseBody
	public List<Category> rp(
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "30") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "asc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "sort") String record, 
			@RequestParam(value = "search", required = true, defaultValue = "") String search, 
			@RequestParam(value = "status", required = true, defaultValue = "0") int status) {
		return categoryService.rp(page, count, sort, record, search, status);
	}
	
	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public String detail(Model model, 
			@RequestParam(value = "cId", required = false) Long cId) {
		if (cId != null) model.addAttribute("obj", categoryService.ro(cId));
		return "/admin/site/category/detail";
	}
	
	@RequestMapping(value = "/cud", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse cud(HttpServletRequest request) {
		return categoryService.cud(request);
	}
	
	@RequestMapping(value = "/sort", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse sort(
			@RequestParam(value = "cId", required = true) Long cId, 
			@RequestParam(value = "upDown", required = true) String upDown) {
		return categoryService.sort(cId, upDown);
	}
	
}
