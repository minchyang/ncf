package com.ncf.admin.controller.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncf.admin.entity.ContactUs;
import com.ncf.admin.response.CommonResponse;
import com.ncf.admin.service.ContactUsService;

@Controller
@RequestMapping(value = "/admin/contactUs")
public class ContactUsController {

	@Autowired private ContactUsService contactUsService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(
			@RequestParam(value = "type", required = false, defaultValue = "visitor") String type) {
		if (type.equals("exhibitors")) return "/admin/contactUs/exhibitors/list";
		else if (type.equals("buyer")) return "/admin/contactUs/buyer/list";
		else return "/admin/contactUs/visitor/list";
	}
	
	@RequestMapping(value = "/cud", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse cud(HttpServletRequest request) {
		return contactUsService.cud(request);
	}
	
	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public String detail(Model model, 
			@RequestParam(value = "cId", required = true) Long cId, 
			@RequestParam(value = "type", required = true) String type) {
		model.addAttribute("obj", contactUsService.ro(cId));
		if (type.equals("visitor")) return "/admin/contactUs/visitor/detail";
		else if (type.equals("exhibitors")) return "/admin/contactUs/exhibitors/detail";
		else return "/admin/contactUs/buyer/detail";
	}
	
	@RequestMapping(value = "/vrp", method = RequestMethod.POST)
	@ResponseBody
	public List<ContactUs> vrp(
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "25") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "desc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "regDate") String record, 
			@RequestParam(value = "search", required = true, defaultValue = "") String search, 
			@RequestParam(value = "status", required = true, defaultValue = "2") int status, 
			@RequestParam(value = "role", required = true, defaultValue = "visitor") String role) {
		return contactUsService.vrp(page, count, sort, record, search, status, role);
	}
	
	@RequestMapping(value = "/erp", method = RequestMethod.POST)
	@ResponseBody
	public List<ContactUs> erp(
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "25") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "desc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "regDate") String record, 
			@RequestParam(value = "search", required = true, defaultValue = "") String search, 
			@RequestParam(value = "status", required = true, defaultValue = "2") int status, 
			@RequestParam(value = "role", required = true, defaultValue = "visitor") String role) {
		return contactUsService.erp(page, count, sort, record, search, status, role);
	}
	
	@RequestMapping(value = "/brp", method = RequestMethod.POST)
	@ResponseBody
	public List<ContactUs> brp(
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "25") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "desc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "regDate") String record, 
			@RequestParam(value = "search", required = true, defaultValue = "") String search, 
			@RequestParam(value = "status", required = true, defaultValue = "2") int status, 
			@RequestParam(value = "role", required = true, defaultValue = "visitor") String role) {
		return contactUsService.brp(page, count, sort, record, search, status, role);
	}
	
}
