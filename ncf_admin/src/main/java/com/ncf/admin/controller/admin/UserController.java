package com.ncf.admin.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.admin.entity.User;
import com.ncf.admin.response.CommonResponse;
import com.ncf.admin.service.UserService;

@Controller
@RequestMapping(value = "/admin/user")
public class UserController {

	@Autowired private UserService userService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(
			@RequestParam(value = "role", required = true, defaultValue = "visitor") String role) {
		if (!role.equals("exhibitors") && !role.equals("buyer")) role = "visitor";
		return "/admin/user/"+role+"/list";
	}
	
	@RequestMapping(value = "/rp", method = RequestMethod.POST)
	@ResponseBody
	public List<User> rp(
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "25") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "desc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "regDate") String record, 
			@RequestParam(value = "search", required = true, defaultValue = "") String search, 
			@RequestParam(value = "status", required = true, defaultValue = "3") int status, 
			@RequestParam(value = "role", required = true, defaultValue = "visitor") String role) {
		return userService.rp(page, count, sort, record, search, status, role);
	}
	
	@RequestMapping(value = "/cud", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse cud(MultipartHttpServletRequest request) {
		return userService.cud(request);
	}
	
	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public String detail(Model model, 
			@RequestParam(value = "userId", required = true) String userId, 
			@RequestParam(value = "role", required = true, defaultValue = "visitor") String role) {
		if (!role.equals("exhibitors") && !role.equals("buyer")) role = "visitor";
		model.addAttribute("obj", userService.findByUserIdAndRole(userId, role));
		return "/admin/user/"+role+"/detail";
	}
	
}
