package com.ncf.admin.security.model.token;

public interface JwtToken {
    String getToken();
}
