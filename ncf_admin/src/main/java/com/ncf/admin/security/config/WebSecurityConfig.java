package com.ncf.admin.security.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncf.admin.security.auth.ajax.AjaxAuthenticationProvider;
import com.ncf.admin.security.auth.ajax.AjaxLoginProcessingFilter;
import com.ncf.admin.security.auth.jwt.JwtAuthenticationProvider;
import com.ncf.admin.security.auth.jwt.JwtTokenAuthenticationProcessingFilter;
import com.ncf.admin.security.auth.jwt.SkipPathRequestMatcher;
import com.ncf.admin.security.auth.jwt.extractor.TokenExtractor;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    public static final String JWT_TOKEN_HEADER_PARAM = "X-Authorization";
    public static final String FORM_BASED_LOGIN_ENTRY_POINT = "/api/auth/login";
    public static final String TOKEN_BASED_AUTH_ENTRY_POINT = "/api/v*/**";
    public static final String TOKEN_REFRESH_ENTRY_POINT = "/api/auth/token";
    public static final String ADMIN_POINT = "/admin/**";
    
    @Autowired private AuthenticationSuccessHandler successHandler;
    @Autowired private AuthenticationFailureHandler failureHandler;
    @Autowired private AjaxAuthenticationProvider ajaxAuthenticationProvider;
    @Autowired private JwtAuthenticationProvider jwtAuthenticationProvider;
    
    @Autowired private TokenExtractor tokenExtractor;
    
    @Autowired private AuthenticationManager authenticationManager;
    
    @Autowired private ObjectMapper objectMapper;
       
    @Bean
    protected AjaxLoginProcessingFilter buildAjaxLoginProcessingFilter() throws Exception {
        AjaxLoginProcessingFilter filter = new AjaxLoginProcessingFilter(FORM_BASED_LOGIN_ENTRY_POINT, successHandler, failureHandler, objectMapper);
        filter.setAuthenticationManager(this.authenticationManager);
        return filter;
    }
    
    @Bean
    protected JwtTokenAuthenticationProcessingFilter buildJwtTokenAuthenticationProcessingFilter() throws Exception {
        List<String> pathsToSkip = Arrays.asList(TOKEN_REFRESH_ENTRY_POINT, FORM_BASED_LOGIN_ENTRY_POINT, ADMIN_POINT);
        SkipPathRequestMatcher matcher = new SkipPathRequestMatcher(pathsToSkip, TOKEN_BASED_AUTH_ENTRY_POINT);
        JwtTokenAuthenticationProcessingFilter filter 
            = new JwtTokenAuthenticationProcessingFilter(failureHandler, tokenExtractor, matcher);
        filter.setAuthenticationManager(this.authenticationManager);
        return filter;
    }
    
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(ajaxAuthenticationProvider);
        auth.authenticationProvider(jwtAuthenticationProvider);
    }
    
    @Bean
    protected BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.headers().frameOptions().disable();
        http
        .csrf().disable(); // We don't need CSRF for JWT based authentication
        /*.exceptionHandling() */
        /*.authenticationEntryPoint(this.authenticationEntryPoint);*/
        
    	http.authorizeRequests()
			.antMatchers("/vendors/**", "/resources/**", "/webjars/**", "/dist/**", "/static/**", "/img/**", "/css/**", "/js/**", "/fonts/**").permitAll();

    	http.authorizeRequests()
			.antMatchers("/login", "/logout").anonymous()
			.antMatchers("/admin/**").hasAnyRole("ADMIN")
			.and()
		.formLogin()
			.loginPage("/login")
			.loginProcessingUrl("/login/process")
			.failureUrl("/login?error=undefined")
			.usernameParameter("username")
			.passwordParameter("password")
			//.defaultSuccessUrl("/admin/main", true)
			.and()
		.logout()
			.logoutUrl("/logout")
			.logoutSuccessUrl("/login");
//		.and()
//			.addFilterBefore(buildAdminLoginProcessingFilter(), UsernamePasswordAuthenticationFilter.class)
//			.authenticationProvider(customAuthenticationProvider);
    		
    	http.exceptionHandling().accessDeniedPage("/admin/main");
    	http.sessionManagement().invalidSessionUrl("/admin/main");
    	http.authorizeRequests()
        .and()
            .sessionManagement()
//            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
 
        .and()
            .authorizeRequests()
                .antMatchers(FORM_BASED_LOGIN_ENTRY_POINT).permitAll() // Login end-point
                .antMatchers(TOKEN_REFRESH_ENTRY_POINT).permitAll() // Token refresh end-point
                .antMatchers("/api/**").permitAll()
        		.antMatchers("/webjars/**").permitAll()
        .and()
            .authorizeRequests()
                .antMatchers(TOKEN_BASED_AUTH_ENTRY_POINT).authenticated() // Protected API End-points
        .and()
            .addFilterBefore(buildAjaxLoginProcessingFilter(), UsernamePasswordAuthenticationFilter.class)
            .addFilterBefore(buildJwtTokenAuthenticationProcessingFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}

