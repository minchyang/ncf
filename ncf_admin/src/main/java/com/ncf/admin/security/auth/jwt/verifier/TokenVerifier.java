package com.ncf.admin.security.auth.jwt.verifier;

public interface TokenVerifier {
    public boolean verify(String jti);
}
