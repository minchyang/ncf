package com.ncf.admin.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.admin.entity.ContactUs;

@Repository
@Qualifier(value = "contactUsRepository")
@RestResource(exported = false)
public interface ContactUsRepository extends JpaRepository<ContactUs, Long>, JpaSpecificationExecutor<ContactUs> {

	public ContactUs findByCid(Long cId);
	
}
