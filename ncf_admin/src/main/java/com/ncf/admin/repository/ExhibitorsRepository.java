package com.ncf.admin.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.admin.entity.Exhibitors;

@Repository
@Qualifier(value = "exhibitorsRepository")
@RestResource(exported = false)
public interface ExhibitorsRepository extends JpaRepository<Exhibitors, Long>{

	public Exhibitors findByEid(Long eId);
	
}
