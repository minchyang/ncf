package com.ncf.admin.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.admin.entity.Category;
import com.ncf.admin.entity.ExhibitorsCategory;

@Repository
@Qualifier(value = "exhibitorsCategoryRepository")
@RestResource(exported = false)
public interface ExhibitorsCategoryRepository extends JpaRepository<ExhibitorsCategory, Long>, JpaSpecificationExecutor<ExhibitorsCategory> {

	public List<ExhibitorsCategory> findByCategory(Category category);
	
}
