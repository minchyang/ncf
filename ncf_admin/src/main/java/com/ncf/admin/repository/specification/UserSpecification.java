package com.ncf.admin.repository.specification;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.ncf.admin.entity.Buyer;
import com.ncf.admin.entity.Exhibitors;
import com.ncf.admin.entity.Role;
import com.ncf.admin.entity.User;
import com.ncf.admin.entity.UserRole;

@Service
public class UserSpecification {

	@PersistenceContext EntityManager em;
	
	public List<User> rp(int page, int count, String sort, String record, String search, int status, int roleType) {
		
		String jpql = "";
		System.out.println(status);
		if (roleType == 1) jpql += "SELECT DISTINCT u.userId, u.email, v.companyName, v.managerName, u.status, u.regDate, r.role FROM User u ";
		else if (roleType == 2) jpql += "SELECT DISTINCT u.userId, u.email, v.managerName, u.status, u.regDate, r.role FROM User u ";
		else jpql += "SELECT DISTINCT u.userId, u.email, u.joinType, u.status, u.regDate, r.role FROM User u ";
		jpql += "LEFT JOIN u.roles r ";
		if (roleType == 1) jpql += "LEFT JOIN u.exhibitors v ";
		else if (roleType == 2) jpql += "LEFT JOIN u.buyer v ";
		else jpql += "LEFT JOIN u.visitor v ";
		jpql += "WHERE (u.userId LIKE CONCAT('%',:search,'%') "
		+ "OR u.email LIKE CONCAT('%',:search,'%') ";
		if (roleType == 1) {
			jpql += "OR v.companyName LIKE CONCAT('%',:search,'%') "
					+ "OR v.managerName LIKE CONCAT('%',:search,'%') "
					+ "OR v.tel LIKE CONCAT('%',:search,'%') "
					+ "OR v.ceoName LIKE CONCAT('%',:search,'%') "
					+ "OR v.engCompanyName LIKE CONCAT('%',:search,'%') "
					+ "OR v.engManagerName LIKE CONCAT('%',:search,'%') "
					+ "OR v.engCeoName LIKE CONCAT('%',:search,'%') "
					+ "OR v.engCity LIKE CONCAT('%',:search,'%')) ";
		} else if (roleType == 2) {
			jpql += "OR v.country LIKE CONCAT('%',:search,'%') "
					+ "OR v.managerName LIKE CONCAT('%',:search,'%') "
					+ "OR v.phone LIKE CONCAT('%',:search,'%') "
					+ "OR v.position LIKE CONCAT('%',:search,'%') "
					+ "OR v.tel LIKE CONCAT('%',:search,'%')) ";
		} else {
			jpql += "OR v.phone LIKE CONCAT('%',:search,'%') "
					+ "OR v.position LIKE CONCAT('%',:search,'%')) ";
		}
		jpql += "AND u.roleType = :roleType ";
		if (status == 0) {
			jpql += "AND r.role = 'WAIT' ";
		} else if (status == 1) {
			jpql += "AND u.status = 0 ";
		} else if (status == 2) {
			jpql += "AND u.status = 1 ";
		}
		jpql += "ORDER BY u."+record+" "+sort;
		Query query = em.createQuery(jpql);
		query.setParameter("search", search);
		query.setParameter("roleType", roleType);
		query.setFirstResult(page);
		query.setMaxResults(count);
		List<Object> list = query.getResultList();
		List<User> resultList = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date regDate = new Date();
		for (Object obj : list) {
			Object[] results = (Object[])obj;
			User u = new User();
			u.setUserId(results[0].toString());
			if (results[1] != null) u.setEmail(results[1].toString());
			if (roleType == 1) {
				Exhibitors e = new Exhibitors();
				e.setCompanyName(results[2].toString());
				e.setManagerName(results[3].toString());
				u.setStatus(Integer.parseInt(results[4].toString()));
				try {
					regDate = sdf.parse(results[5].toString().substring(0,19));
				} catch (ParseException pe) {
					pe.printStackTrace();
					regDate = null;
				}
				
				UserRole.Id userRoleId = null;
				UserRole ur = new UserRole();
				List<UserRole> urs = new ArrayList<>();
				if (results[6].toString().equals("EXHIBITORS")) {
					userRoleId = new UserRole.Id(results[0].toString(), Role.EXHIBITORS);
					ur.setRole(Role.EXHIBITORS);
				} else {
					userRoleId = new UserRole.Id(results[0].toString(), Role.WAIT);
					ur.setRole(Role.WAIT);
				}
				ur.setId(userRoleId);
				urs.add(ur);
				u.setRoles(urs);
				
				u.setRegDate(regDate);
				u.setExhibitors(e);
				resultList.add(u);
			} else if (roleType == 2) {
				Buyer b = new Buyer();
				b.setManagerName(results[2].toString());
				u.setStatus(Integer.parseInt(results[3].toString()));
				try {
					regDate = sdf.parse(results[4].toString().substring(0,19));
				} catch (ParseException pe) {
					pe.printStackTrace();
					regDate = null;
				}
				
				UserRole.Id userRoleId = null;
				UserRole ur = new UserRole();
				List<UserRole> urs = new ArrayList<>();
				if (results[5].toString().equals("BUYER")) {
					userRoleId = new UserRole.Id(results[0].toString(), Role.BUYER);
					ur.setRole(Role.BUYER);
				} else {
					userRoleId = new UserRole.Id(results[0].toString(), Role.WAIT);
					ur.setRole(Role.WAIT);
				}
				ur.setId(userRoleId);
				urs.add(ur);
				u.setRoles(urs);
				
				u.setRegDate(regDate);
				u.setBuyer(b);
				resultList.add(u);
			} else {
				u.setJoinType(results[2].toString());
				u.setStatus(Integer.parseInt(results[3].toString()));
				try {
					regDate = sdf.parse(results[4].toString().substring(0,19));
				} catch (ParseException pe) {
					pe.printStackTrace();
					regDate = null;
				}
				
				UserRole.Id userRoleId = null;
				UserRole ur = new UserRole();
				List<UserRole> urs = new ArrayList<>();
				if (results[5].toString().equals("VISITOR")){
					userRoleId = new UserRole.Id(results[0].toString(), Role.VISITOR);
					ur.setRole(Role.VISITOR);
				} else {
					userRoleId = new UserRole.Id(results[0].toString(), Role.WAIT);
					ur.setRole(Role.WAIT);
				}
				ur.setId(userRoleId);
				urs.add(ur);
				u.setRoles(urs);
				
				u.setRegDate(regDate);
				resultList.add(u);
			}
		}
		return resultList;
	}
	
}
