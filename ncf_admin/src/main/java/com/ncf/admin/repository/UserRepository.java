package com.ncf.admin.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.admin.entity.Role;
import com.ncf.admin.entity.User;

@Repository
@Qualifier(value = "userRepository")
@RestResource(exported = false)
public interface UserRepository extends JpaRepository<User, String>, JpaSpecificationExecutor<User> {

	@Query("select u from User u left join fetch u.roles r where u.userId=:userId and r.id.role in :roles")
	public User findByUserIdAndRole(@Param("userId") String userId, @Param("roles") List<Role> roles);
	
	public User findByUserId(String userId);
	public User findByEmailAndUserId(String email, String phone);
	
}