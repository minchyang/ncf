package com.ncf.admin.repository.specification;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import com.ncf.admin.entity.Genre;

@Service
public class GenreSpecification {

	@PersistenceContext private EntityManager em;
	
	public List<Genre> rp(int page, int count, String sort, String record, String search, int status) {
		String jpql = "SELECT DISTINCT g FROM Genre g "
				+ "WHERE g.name LIKE CONCAT('%',:search,'%') ";
				if (status != 2) jpql += "AND g.status = :status ";
				jpql += "ORDER BY g."+record+" "+sort;
		TypedQuery<Genre> query = em.createQuery(jpql, Genre.class);
		query.setParameter("search", search);
		if (status != 2) query.setParameter("status", status);
		query.setFirstResult(page);
		query.setMaxResults(count);
		return query.getResultList();
	}
	
	public int max(String column) {
		String jpql = "SELECT DISTINCT MAX(g.sort) FROM Genre g ";
		Query query = em.createQuery(jpql);
		Object obj = query.getSingleResult();
		if (obj != null) return (Integer)obj;
		else return 0;
	}
	
}
