package com.ncf.admin.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.admin.entity.Genre;

@Repository
@Qualifier(value = "genreRepository")
@RestResource(exported = false)
public interface GenreRepository extends JpaRepository<Genre, Long>, JpaSpecificationExecutor<Genre> {

	public Genre findByGid(Long gId);
	public Genre findBySort(int sort);
	
}
