package com.ncf.admin.repository.specification;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import com.ncf.admin.entity.Category;

@Service
public class CategorySpecification {

	@PersistenceContext private EntityManager em;
	
	public List<Category> rp(int page, int count, String sort, String record, String search, int status) {
		String jpql = "SELECT DISTINCT c FROM Category c "
				+ "WHERE c.name LIKE CONCAT('%',:search,'%') ";
				if (status != 2) jpql += "AND c.status = :status ";
				jpql += "ORDER BY c."+record+" "+sort;
		TypedQuery<Category> query = em.createQuery(jpql, Category.class);
		query.setParameter("search", search);
		if (status != 2) query.setParameter("status", status);
		query.setFirstResult(page);
		query.setMaxResults(count);
		return query.getResultList();
	}
	
	public int max(String column) {
		String jpql = "SELECT DISTINCT MAX(c.sort) FROM Category c ";
		Query query = em.createQuery(jpql);
		Object obj = query.getSingleResult();
		if (obj != null) return (Integer)obj;
		else return 0;
	}
	
}
