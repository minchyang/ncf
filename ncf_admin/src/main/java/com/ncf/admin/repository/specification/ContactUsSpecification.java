package com.ncf.admin.repository.specification;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import com.ncf.admin.entity.ContactUs;
import com.ncf.admin.entity.Role;

@Service
public class ContactUsSpecification {

	@PersistenceContext EntityManager em;
	
	public List<ContactUs> vrp(int page, int count, String sort, String record, String search, int status, Role role) {
		System.out.println("page : " + page);
		System.out.println("count : " + count);
		System.out.println("sort : " + sort);
		System.out.println("record : " + record);
		System.out.println("search : " + search);
		System.out.println("role : " + role);
		String jpql = "SELECT DISTINCT c FROM ContactUs c "
				+ "LEFT JOIN c.questioner q "
				+ "LEFT JOIN c.answerer a "
				+ "INNER JOIN q.visitor v "
				+ "WHERE (q.email LIKE CONCAT ('%',:search,'%') "
				+ "OR c.subject LIKE CONCAT ('%',:search,'%') "
				+ "OR v.phone LIKE CONCAT ('%',:search,'%')) "
				+ "ORDER BY c."+record+" "+sort;
		TypedQuery<ContactUs> query = em.createQuery(jpql, ContactUs.class);
		query.setParameter("search", search);
		query.setFirstResult(page);
		query.setMaxResults(count);
		return query.getResultList();
	}
	
	public List<ContactUs> erp(int page, int count, String sort, String record, String search, int status, Role role) {
		System.out.println("page : " + page);
		System.out.println("count : " + count);
		System.out.println("sort : " + sort);
		System.out.println("record : " + record);
		System.out.println("search : " + search);
		System.out.println("role : " + role);
		String jpql = "SELECT DISTINCT c FROM ContactUs c "
				+ "LEFT JOIN c.questioner q "
				+ "LEFT JOIN c.answerer a "
				+ "INNER JOIN q.exhibitors e "
				+ "WHERE (q.email LIKE CONCAT ('%',:search,'%') "
				+ "OR c.subject LIKE CONCAT ('%',:search,'%') "
				+ "OR e.phone LIKE CONCAT ('%',:search,'%') "
				+ "OR e.companyName LIKE CONCAT ('%',:search,'%')) "
				+ "ORDER BY c."+record+" "+sort;
		TypedQuery<ContactUs> query = em.createQuery(jpql, ContactUs.class);
		query.setParameter("search", search);
		query.setFirstResult(page);
		query.setMaxResults(count);
		return query.getResultList();
	}
	
	public List<ContactUs> brp(int page, int count, String sort, String record, String search, int status, Role role) {
		System.out.println("page : " + page);
		System.out.println("count : " + count);
		System.out.println("sort : " + sort);
		System.out.println("record : " + record);
		System.out.println("search : " + search);
		System.out.println("role : " + role);
		String jpql = "SELECT DISTINCT c FROM ContactUs c "
				+ "LEFT JOIN c.questioner q "
				+ "LEFT JOIN c.answerer a "
				+ "INNER JOIN q.buyer b "
				+ "WHERE (q.email LIKE CONCAT ('%',:search,'%') "
				+ "OR c.subject LIKE CONCAT ('%',:search,'%') "
				+ "OR b.phone LIKE CONCAT ('%',:search,'%') "
				+ "OR b.companyName LIKE CONCAT ('%',:search,'%')) "
				+ "ORDER BY c."+record+" "+sort;
		TypedQuery<ContactUs> query = em.createQuery(jpql, ContactUs.class);
		query.setParameter("search", search);
		query.setFirstResult(page);
		query.setMaxResults(count);
		return query.getResultList();
	}
	
}
