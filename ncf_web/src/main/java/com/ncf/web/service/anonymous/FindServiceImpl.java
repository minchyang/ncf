package com.ncf.web.service.anonymous;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.web.entity.User;
import com.ncf.web.mail.MailService;
import com.ncf.web.repository.UserRepository;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.response.result.ResultCode;

@Service
public class FindServiceImpl implements FindService {
	
	@Autowired private MailService mailService;
	@Autowired private UserRepository userRepository;
	
	@Override
	public CommonResponse password(HttpServletRequest request) {
		String userId = request.getParameter("userId");
		String email = request.getParameter("email");
		User user = userRepository.findByUserIdAndEmail(userId, email);
		if (user == null) return CommonResponse.of("입력하신 정보에 일치하는 회원을 찾을 수 없습니다.", ResultCode.FAIL);
		
		String subject = "[NextContentFair] 비밀번호 찾기";
		String content = user.getUserId()+" 회원님의 비밀번호는 [ "+user.getPassword()+" ] 입니다.";
		Boolean tof = mailService.sendMail(user.getEmail(), subject, content);
		
		if (tof) return CommonResponse.of("등록하신 이메일 주소로 메일이 발송 되었습니다.", ResultCode.SUCCESS);
		else return CommonResponse.of("이메일 발송에 실패 했습니다.", ResultCode.FAIL);
	}

}
