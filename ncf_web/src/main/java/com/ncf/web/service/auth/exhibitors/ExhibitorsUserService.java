package com.ncf.web.service.auth.exhibitors;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.web.response.CommonResponse;

public interface ExhibitorsUserService {

	public CommonResponse update(MultipartHttpServletRequest request);
	
}
