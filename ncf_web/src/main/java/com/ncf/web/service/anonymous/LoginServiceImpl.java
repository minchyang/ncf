package com.ncf.web.service.anonymous;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.web.entity.Role;
import com.ncf.web.entity.User;
import com.ncf.web.repository.UserRepository;
import com.ncf.web.response.UserResponse;
import com.ncf.web.response.result.ResultCode;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired private UserRepository userRepository;
	
	@Override
	public UserResponse login(HttpServletRequest request) {
		
		String userId = request.getParameter("userId");
		String password = request.getParameter("password");
		String strRole = request.getParameter("role");
		
		Role role = null;
		if (strRole.equals("BUYER")) role = Role.BUYER;
		else if (strRole.equals("EXHIBITORS")) role = Role.EXHIBITORS;
		else role = Role.VISITOR;
		
		User user = userRepository.findByUserIdAndPasswordAndRole(userId, password, role);
		if (user == null) {
			if (role.equals(Role.EXHIBITORS) || role.equals(Role.BUYER)) {
				user = userRepository.findByUserIdAndPasswordAndRole(userId, password, Role.WAIT);
				if (user == null) {
					return UserResponse.of("아이디, 비밀번호를 확인 해주세요.", ResultCode.FAIL, null);
				} else {
					if (role.equals(Role.EXHIBITORS) && user.getRoleType() == 1) {
						return UserResponse.of("관리자 승인 대기중입니다.", ResultCode.FAIL, null);
					} else if (role.equals(Role.BUYER) && user.getRoleType() == 2) {
						return UserResponse.of("관리자 승인 대기중입니다.", ResultCode.FAIL, null);
					} else {
						return UserResponse.of("아이디, 비밀번호를 확인 해주세요.", ResultCode.FAIL, null);
					}
				}
			} else {
				return UserResponse.of("아이디, 비밀번호를 확인 해주세요.", ResultCode.FAIL, null);
			}
		} else {
			System.out.println(user.getUserId());
			System.out.println(user.getStatus());
			if (user.getStatus() == 1) {
				return UserResponse.of("관리자에 의해 활동이 정지된 계정입니다.", ResultCode.FAIL, null);
			} else {
				return UserResponse.of("success", ResultCode.SUCCESS, user);
			}
		}
	}
	
}
