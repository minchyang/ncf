package com.ncf.web.service.anonymous;

import java.util.List;

import com.ncf.web.entity.Category;

public interface CategoryService {

	public List<Category> rp(int page, int count, String sort, String record, String search, int status);
	public Category ro(Long cId);
	
}
