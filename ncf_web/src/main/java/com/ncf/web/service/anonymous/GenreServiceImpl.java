package com.ncf.web.service.anonymous;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.web.entity.Genre;
import com.ncf.web.repository.GenreRepository;
import com.ncf.web.repository.specification.GenreSpecification;

@Service
public class GenreServiceImpl implements GenreService {

	@Autowired private GenreSpecification genreSpecification;
	@Autowired private GenreRepository genreRepository;
	
	@Override
	public List<Genre> rp(int page, int count, String sort, String record, String search, int status) {
		if (page != 0) page = (page * count);
		return genreSpecification.rp(page, count, sort, record, search, status);
	}
	
	@Override
	public Genre ro(Long gId) {
		return genreRepository.findByGid(gId);
	}

}
