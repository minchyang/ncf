package com.ncf.web.service.auth.exhibitors;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.web.entity.Content;
import com.ncf.web.entity.ContentGenre;
import com.ncf.web.entity.Exhibitors;
import com.ncf.web.entity.ExhibitorsContent;
import com.ncf.web.entity.Genre;
import com.ncf.web.entity.User;
import com.ncf.web.repository.ContentGenreRepository;
import com.ncf.web.repository.ContentRepository;
import com.ncf.web.repository.ExhibitorsContentRepository;
import com.ncf.web.repository.ExhibitorsRepository;
import com.ncf.web.repository.GenreRepository;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.response.result.ResultCode;
import com.ncf.web.service.anonymous.UploadService;

@Service
public class ExhibitorsContentServiceImpl implements ExhibitorsContentService {

	@Autowired private UploadService uploadService;
	@Autowired private ExhibitorsContentRepository exhibitorsContentRepository;
	@Autowired private ExhibitorsRepository exhibitorsRepository;
	@Autowired private ContentRepository contentRepository;
	@Autowired private ContentGenreRepository contentGenreRepository;
	@Autowired private GenreRepository genreRepository;
	
	@Override
	public ExhibitorsContent findOne(Long ecId) {
		return exhibitorsContentRepository.findByEcId(ecId);
	}
	
	@Transactional
	@Override
	public CommonResponse cud(User user, MultipartHttpServletRequest request) {
		
		if (user == null) return CommonResponse.of("회원을 찾을 수 없습니다.", ResultCode.FAIL);
		if (request.getParameter("cudType") == null || request.getParameter("cudType").equals("")) return CommonResponse.of("데이터 처리 유형을 찾을 수 없습니다.", ResultCode.FAIL);
		
		if (request.getParameter("cudType").equals("create")) {
			
			Exhibitors e = user.getExhibitors();
			ExhibitorsContent ec = new ExhibitorsContent();
			Content c = new Content();
			
			MultipartFile thumbnail = request.getFile("thumbnail");
			String name = request.getParameter("name");
			String[] genre = request.getParameterValues("genre");
			String genreDefault = request.getParameter("genreDefault");
			String contentVideoType = request.getParameter("contentVideoType");
			String contentVideoUrl = request.getParameter("contentVideoUrl");
			String yearOfProduction = request.getParameter("yearOfProduction");
			String contentIntroduction = request.getParameter("contentIntroduction");
			String engName = request.getParameter("engName");
			String engContentIntroduction = request.getParameter("engContentIntroduction");
			
			System.out.println("thumbnail : " + thumbnail);
			System.out.println("name  : " + name);
			System.out.println("genre : " + genre.length);
			System.out.println("contentVideoType : " + contentVideoType);
			System.out.println("contentVideoUrl : " + contentVideoUrl);
			System.out.println("yearOfProduction : " + yearOfProduction);
			System.out.println("contentIntroduction : " + contentIntroduction);
			System.out.println("engName : " + engName);
			System.out.println("engContentIntroduction : " + engContentIntroduction);
			
			c.setName(name);
			if (contentVideoType != null && !contentVideoType.equals("") && !contentVideoType.equals("2") && !contentVideoType.equals("null")) {
				c.setContentVideoType(Integer.parseInt(contentVideoType));
				System.out.println("요기?");
				try {
					if (contentVideoType.equals("0")) { // youtube
						contentVideoUrl = contentVideoUrl.substring(contentVideoUrl.length()-11, contentVideoUrl.length());
						contentVideoUrl = contentVideoUrl.replace("watch?v=", "");
						int idx1 = contentVideoUrl.indexOf("&t=");
						if (idx1 != -1) contentVideoUrl = contentVideoUrl.substring(0, idx1);
						int idx2 = contentVideoUrl.indexOf("?");
						if (idx2 != -1) contentVideoUrl = contentVideoUrl.substring(0, idx2);
					} else { // vimeo
						System.out.println("else");
						contentVideoUrl = contentVideoUrl.substring(contentVideoUrl.length()-9, contentVideoUrl.length());
					}
				} catch (Exception ex) {
					contentVideoUrl = "";
				}
				c.setContentVideoUrl(contentVideoUrl);
			} else {
				c.setContentVideoType(2);
				c.setContentVideoUrl(null);
			}
			c.setYearOfProduction(yearOfProduction);
			c.setContentIntroduction(contentIntroduction);
			c.setEngName(engName);
			c.setEngContentIntroduction(engContentIntroduction);
			contentRepository.save(c);
			
			List<ContentGenre> cgs = new ArrayList<>();
			String parseGenre = genre[0];
			String[] genres = parseGenre.split(",");
			
			for (int i=0; i<genres.length; i++) {
				Genre g = genreRepository.findByGid(Long.parseLong(genres[i]));
				ContentGenre cg = new ContentGenre();
				cg.setContent(c);
				cg.setGenre(g);
				cg = contentGenreRepository.save(cg);
				cgs.add(cg);
			}
			
			if (thumbnail != null) {
				CommonResponse logo_res = uploadService.upload(thumbnail, "/ncf/user/image");
				if (logo_res.getResultCode().equals(ResultCode.SUCCESS)) c.setThumbnail(logo_res.getMessage());
				else return CommonResponse.of("썸네일 이미지 등록에 실패 했습니다.", ResultCode.FAIL);
			}
			
			c.setContentGenre(cgs);
			c = contentRepository.save(c);
			
			ec.setContent(c);
			ec.setExhibitors(e);
			exhibitorsContentRepository.save(ec); 
			
			return CommonResponse.of("등록 되었습니다.", ResultCode.SUCCESS);
			
		} else if (request.getParameter("cudType").equals("update")) {
			if (request.getParameter("ecId") == null || request.getParameter("ecId").equals("")) return CommonResponse.of("콘텐츠 ID를 찾을 수 없습니다.", ResultCode.FAIL);
			ExhibitorsContent ec = exhibitorsContentRepository.findByEcId(Long.parseLong(request.getParameter("ecId")));
			if (ec == null) return CommonResponse.of("콘텐츠를 찾을 수 없습니다.", ResultCode.FAIL);
			Content c = ec.getContent();
			
			MultipartFile thumbnail = request.getFile("thumbnail");
			String name = request.getParameter("name");
			String[] genre = request.getParameterValues("genre");
			String genreDefault = request.getParameter("genreDefault");
			String contentVideoType = request.getParameter("contentVideoType");
			String contentVideoUrl = request.getParameter("contentVideoUrl");
			String yearOfProduction = request.getParameter("yearOfProduction");
			String contentIntroduction = request.getParameter("contentIntroduction");
			String engName = request.getParameter("engName");
			String engContentIntroduction = request.getParameter("engContentIntroduction");
			
			System.out.println("thumbnail : " + thumbnail);
			System.out.println("name  : " + name);
			System.out.println("genre : " + genre.length);
			System.out.println("genreDefault : " + genreDefault);
			System.out.println("contentVideoType : " + contentVideoType);
			System.out.println("contentVideoUrl : " + contentVideoUrl);
			System.out.println("yearOfProduction : " + yearOfProduction);
			System.out.println("contentIntroduction : " + contentIntroduction);
			System.out.println("engName : " + engName);
			System.out.println("engContentIntroduction : " + engContentIntroduction);
			
			List<ContentGenre> cgs = new ArrayList<>();
			if (genreDefault.equals("1")) {
				String parseGenre = genre[0];
				String[] genres = parseGenre.split(",");
				List<ContentGenre> prevGenre = c.getContentGenre();
				c.setContentGenre(null);
				contentRepository.save(c);
				for (int i=0; i<prevGenre.size(); i++) {
					ContentGenre cg = contentGenreRepository.findByCgId(prevGenre.get(i).getCgId());
					contentGenreRepository.delete(cg);
				}
				
				for (int i=0; i<genres.length; i++) {
					Genre g = genreRepository.findByGid(Long.parseLong(genres[i]));
					ContentGenre cg = new ContentGenre();
					cg.setContent(c);
					cg.setGenre(g);
					cg = contentGenreRepository.save(cg);
					cgs.add(cg);
				}
			}
			
			if (thumbnail != null) {
				CommonResponse logo_res = uploadService.upload(thumbnail, "/ncf/user/image");
				if (logo_res.getResultCode().equals(ResultCode.SUCCESS)) c.setThumbnail(logo_res.getMessage());
				else return CommonResponse.of("썸네일 이미지 등록에 실패 했습니다.", ResultCode.FAIL);
			}
			
			c.setName(name);
			if (genreDefault.equals("1")) c.setContentGenre(cgs);
			
			if (contentVideoType != null && !contentVideoType.equals("") && !contentVideoType.equals("2") && !contentVideoType.equals("null")) {
				c.setContentVideoType(Integer.parseInt(contentVideoType));
				try {
					if (contentVideoType.equals("0")) { // youtube
						int idx1 = contentVideoUrl.lastIndexOf("/");
						contentVideoUrl = contentVideoUrl.substring((idx1+1));
						contentVideoUrl = contentVideoUrl.replace("watch?v=", "");
						int idx2 = contentVideoUrl.indexOf("&t=");
						if (idx2 != -1) contentVideoUrl = contentVideoUrl.substring(0, idx2);
						int idx3 = contentVideoUrl.indexOf("?");
						if (idx3 != -1) contentVideoUrl = contentVideoUrl.substring(0, idx3);
					} else { // vimeo
						contentVideoUrl = contentVideoUrl.substring(contentVideoUrl.length()-9, contentVideoUrl.length());
					}
				} catch (Exception ex) {
					contentVideoUrl = "";
				}
				c.setContentVideoUrl(contentVideoUrl);
			} else {
				c.setContentVideoType(2);
				c.setContentVideoUrl(null);
			}
			
			c.setYearOfProduction(yearOfProduction);
			c.setContentIntroduction(contentIntroduction);
			c.setEngName(engName);
			c.setEngContentIntroduction(engContentIntroduction);
			contentRepository.save(c);
			
			return CommonResponse.of("수정 되었습니다.", ResultCode.SUCCESS);
		} else if (request.getParameter("cudType").equals("delete")) {
			
			if (request.getParameter("ecId") == null || request.getParameter("ecId").equals("")) return CommonResponse.of("콘텐츠 ID를 찾을 수 없습니다.", ResultCode.FAIL);
			ExhibitorsContent ec = exhibitorsContentRepository.findByEcId(Long.parseLong(request.getParameter("ecId")));
			if (ec == null) return CommonResponse.of("콘텐츠를 찾을 수 없습니다.", ResultCode.FAIL);
			Exhibitors e = ec.getExhibitors();
			List<ExhibitorsContent> prevEcs = e.getExhibitorsContent();
			List<ExhibitorsContent> ecs = new ArrayList<>();
			for (int i=0; i<prevEcs.size(); i++) {
				if (prevEcs.get(i).getEcId() != ec.getEcId()) {
					ecs.add(prevEcs.get(i));
				}
			}
			e.setExhibitorsContent(null);
			exhibitorsRepository.save(e);
			e.setExhibitorsContent(ecs);
			exhibitorsRepository.save(e);
			Content c = ec.getContent();
			contentRepository.delete(c);
			exhibitorsContentRepository.delete(ec);
			return CommonResponse.of("삭제 되었습니다.", ResultCode.SUCCESS);
			
		} else {
			
			return CommonResponse.of("데이터 처리 유형이 잘못 되었습니다.", ResultCode.FAIL);
		}
		
	}

}
