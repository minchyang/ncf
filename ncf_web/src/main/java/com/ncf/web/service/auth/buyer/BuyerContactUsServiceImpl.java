package com.ncf.web.service.auth.buyer;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.web.entity.Buyer;
import com.ncf.web.entity.ContactUs;
import com.ncf.web.entity.Exhibitors;
import com.ncf.web.entity.User;
import com.ncf.web.repository.BuyerRepository;
import com.ncf.web.repository.ContactUsRepository;
import com.ncf.web.repository.ExhibitorsRepository;
import com.ncf.web.repository.UserRepository;
import com.ncf.web.repository.specification.ContactUsSpecification;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.response.result.ResultCode;

@Service
public class BuyerContactUsServiceImpl implements BuyerContactUsService {

	@Autowired private ContactUsSpecification contactUsSpecification;
	@Autowired private ExhibitorsRepository exhibitorsRepository;
	@Autowired private BuyerRepository buyerRepository;
	@Autowired private UserRepository userRepository;
	@Autowired private ContactUsRepository contactUsRepository;
	
	@Override
	public CommonResponse insert(HttpServletRequest request, User questioner) {
		
		if (questioner == null) return CommonResponse.of("세션이 종료 되었습니다. 다시 로그인 해주세요.", ResultCode.FAIL);
		
		Long id = Long.parseLong(request.getParameter("id"));
		String type = request.getParameter("type");
		String subject = request.getParameter("subject");
		String description = request.getParameter("description");
		
		ContactUs c = new ContactUs();
		
		if (type.equals("e")) { // exhibitors
			
			Exhibitors e = exhibitorsRepository.findByEidAndStatus(id, 0);
			if (e == null) return CommonResponse.of("참가사를 찾을 수 없습니다.", ResultCode.FAIL);
			User answerer = userRepository.findByExhibitors(e);
			if (answerer == null) return CommonResponse.of("참가사 회원을 찾을 수 없습니다.", ResultCode.FAIL);
			c.setAnswerer(answerer);
			c.setQuestioner(questioner);
			c.setSubject(subject);
			c.setDescription(description);
			contactUsRepository.save(c);
			return CommonResponse.of("문의 완료 되었습니다.", ResultCode.SUCCESS);
			
		} else { // buyer
			
			Buyer b = buyerRepository.findByBid(id);
			if (b == null) return CommonResponse.of("바이어를 찾을 수 없습니다.", ResultCode.FAIL);
			User answerer = userRepository.findByBuyer(b);
			if (answerer == null) return CommonResponse.of("바이어 회원을 찾을 수 없습니다.", ResultCode.FAIL);
			if (answerer.getUserId().equals(questioner.getUserId())) return CommonResponse.of("자신에게는 문의 하실 수 없습니다.", ResultCode.FAIL);
			c.setAnswerer(answerer);
			c.setQuestioner(questioner);
			c.setSubject(subject);
			c.setDescription(description);
			contactUsRepository.save(c);
			return CommonResponse.of("문의 완료 되었습니다.", ResultCode.SUCCESS);
			
		}
		
	}
	
	@Override
	public List<ContactUs> qRp(int page, int count, String sort, String record, String searchType, String search, User questioner) {
		if (page != 0) page = (page * count);
		return contactUsSpecification.qRp(page, count, sort, record, searchType, search, questioner.getUserId());
	}
	
	@Override
	public List<ContactUs> aRp(int page, int count, String sort, String record, String searchType, String search, User answerer) {
		if (page != 0) page = (page * count);
		return contactUsSpecification.aRp(page, count, sort, record, searchType, search, answerer.getUserId());
	}

}
