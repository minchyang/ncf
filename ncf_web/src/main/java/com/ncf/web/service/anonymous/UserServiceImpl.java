package com.ncf.web.service.anonymous;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.web.entity.Buyer;
import com.ncf.web.entity.BuyerGenre;
import com.ncf.web.entity.Category;
import com.ncf.web.entity.Content;
import com.ncf.web.entity.ContentGenre;
import com.ncf.web.entity.Exhibitors;
import com.ncf.web.entity.ExhibitorsCategory;
import com.ncf.web.entity.ExhibitorsContent;
import com.ncf.web.entity.ExhibitorsGenre;
import com.ncf.web.entity.Genre;
import com.ncf.web.entity.Role;
import com.ncf.web.entity.User;
import com.ncf.web.entity.UserRole;
import com.ncf.web.entity.Visitor;
import com.ncf.web.repository.BuyerGenreRepository;
import com.ncf.web.repository.BuyerRepository;
import com.ncf.web.repository.CategoryRepository;
import com.ncf.web.repository.ContentRepository;
import com.ncf.web.repository.ExhibitorsCategoryRepository;
import com.ncf.web.repository.ExhibitorsContentRepository;
import com.ncf.web.repository.ExhibitorsGenreRepository;
import com.ncf.web.repository.ExhibitorsRepository;
import com.ncf.web.repository.GenreRepository;
import com.ncf.web.repository.UserRepository;
import com.ncf.web.repository.UserRoleRepository;
import com.ncf.web.repository.VisitorRepository;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.response.UserResponse;
import com.ncf.web.response.result.ResultCode;

@Service
public class UserServiceImpl implements UserService {

	@Autowired private UploadService uploadService;
	@Autowired private UserRepository userRepository;
	@Autowired private UserRoleRepository userRoleRepository;
	@Autowired private VisitorRepository visitorRepository;
	@Autowired private CategoryRepository categoryRepository;
	@Autowired private GenreRepository genreRepository;
	@Autowired private ContentRepository contentRepository;
	@Autowired private ExhibitorsRepository exhibitorsRepository;
	@Autowired private ExhibitorsCategoryRepository exhibitorsCategoryRepository;
	@Autowired private ExhibitorsContentRepository exhibitorsContentRepository;
	@Autowired private ExhibitorsGenreRepository exhibitorsGenreRepository;
	@Autowired private BuyerGenreRepository buyerGenreRepository;
	@Autowired private BuyerRepository buyerRepository;
	
	@Override
	public User findByUserIdAndPassword(String userId, String password) {
		return userRepository.findByUserIdAndPassword(userId, password);
	}
	
	@Override
	public CommonResponse duplicateCheck(String column, String value) {
		if (column.equals("userId")) {
			if (userRepository.findByUserId(value) == null) return CommonResponse.of("success", ResultCode.SUCCESS);
			else return CommonResponse.of("이미 존재하는 아이디입니다.", ResultCode.FAIL);
		} else if (column.equals("email")) {
			if (userRepository.findByEmail(value) == null) return CommonResponse.of("success", ResultCode.SUCCESS);
			else return CommonResponse.of("이미 존재하는 이메일입니다.", ResultCode.FAIL);
		} else {
			return null;
		}
	}
	
	@Override
	@Transactional
	public UserResponse cud(MultipartHttpServletRequest request) {
		String userId = request.getParameter("userId");
		String password = request.getParameter("password");
		String joinType = request.getParameter("joinType");
		String type = request.getParameter("type");
		String email = request.getParameter("email");
		
		User user = new User();
		
		if (joinType.equals("N") || joinType.equals("K") || joinType.equals("G")) {
			
			user.setUserId(userId);
			user.setPassword(password);
			user.setJoinType(joinType);
			user.setRoleType(0);
			if (email != null && !email.equals("")) user.setEmail(email);
			user = userRepository.save(user);
			
			UserRole.Id userRoleId = null;
			UserRole ur = new UserRole();
			List<UserRole> urs = new ArrayList<>();
			
			userRoleId = new UserRole.Id(userId, Role.VISITOR);
			ur.setRole(Role.VISITOR);
			ur.setId(userRoleId);
			urs.add(ur);
			userRoleRepository.save(ur);
			user.setRoles(urs);
			userRepository.save(user);
			
			return UserResponse.of("가입 되었습니다.", ResultCode.SUCCESS, user);
			
		} else {
			
			if (type.equals("exhibitors")) {
				
				if (userRepository.findByUserId(userId) != null) return UserResponse.of("이미 사용중인 아이디입니다.", ResultCode.FAIL, null);
				if (userRepository.findByEmail(email) != null) return UserResponse.of("이미 사용중인 이메일입니다.", ResultCode.FAIL, null);
				
				MultipartFile logo = request.getFile("logo");
				String companyName = request.getParameter("companyName");
				String managerName = request.getParameter("managerName");
				String tel = request.getParameter("tel");
				String nationalCode = request.getParameter("nationalCode");
				String frontPhone = request.getParameter("frontPhone");
				String phone = request.getParameter("phone");
				String position = request.getParameter("position");
				String site = request.getParameter("site");
				String establishYear = request.getParameter("establishYear");
				String establishMonth = request.getParameter("establishMonth");
				String establishDay = request.getParameter("establishDay");
				String ceoName = request.getParameter("ceoName");
				String[] category = request.getParameterValues("category");
				String promotionalVideoType = request.getParameter("promotionalVideoType");
				String promotionalVideoUrl = request.getParameter("promotionalVideoUrl");
				String aboutUs = request.getParameter("aboutUs");
				MultipartFile brochure = request.getFile("brochure");
				String[] genre = request.getParameterValues("genre");
				MultipartFile engLogo = request.getFile("engLogo");
				String engCompanyName = request.getParameter("engCompanyName");
				String engManagerName = request.getParameter("engManagerName");
				String engCeoName = request.getParameter("engCeoName");
				String engCity = request.getParameter("engCity");
				String engAboutUs = request.getParameter("engAboutUs");
				int c_length = Integer.parseInt(request.getParameter("c_length"));
				
				System.out.println("logo : " + logo);
				System.out.println("companyName : " + companyName);
				System.out.println("managerName : " + managerName);
				System.out.println("tel : " + tel);
				System.out.println("nationalCode : " + nationalCode);
				System.out.println("phone : " + phone);
				System.out.println("site : " + site);
				System.out.println("establishYear : " + establishYear);
				System.out.println("establishMonth : " + establishMonth);
				System.out.println("establishDay : " + establishDay);
				System.out.println("ceoName : "  + ceoName);
				System.out.println("category : " + category[0]);
				System.out.println("promotionalVideoType : " + promotionalVideoType);
				System.out.println("promotionalVideoUrl : " + promotionalVideoUrl);
				System.out.println("aboutUs : " + aboutUs);
				System.out.println("brochure : " + brochure);
				System.out.println("genre : " + genre[0]);
				System.out.println("engLogo : " + engLogo);
				System.out.println("engCompanyName : " + engCompanyName);
				System.out.println("engManagerName : " + engManagerName);
				System.out.println("engCeoName : " + engCeoName);
				System.out.println("engCity : " + engCity);
				System.out.println("engAboutUs : " + engAboutUs);
				System.out.println("c_length : " + c_length);
				
				Exhibitors e = new Exhibitors();
				List<ExhibitorsCategory> ecs = new ArrayList<>();
				List<ExhibitorsGenre> egs = new ArrayList<>();
				List<ExhibitorsContent> ecs2 = new ArrayList<>();
				
				MultipartFile cThumbnail1, cThumbnail2, cThumbnail3, cThumbnail4;
				String cThumbnailPath1 = null, cThumbnailPath2 = null, cThumbnailPath3 = null, cThumbnailPath4 = null;
				String[] c_genre1 = null;
				String[] c_genre2 = null;
				String[] c_genre3 = null;
				String[] c_genre4 = null;
				String[] cGenre1 = null;
				String[] cGenre2 = null;
				String[] cGenre3 = null;
				String[] cGenre4 = null;
				
				String[] str_cName = request.getParameterValues("c_name");
				String parse_cName = str_cName[0];
				String[] cName = parse_cName.split(",");
				
				String[] str_cYearOfProduction = request.getParameterValues("c_yearOfProduction");
				String parse_cYearOfProduction = str_cYearOfProduction[0];
				String[] cYearOfProduction = parse_cYearOfProduction.split(",");
				
				String[] str_cContentIntroduction = request.getParameterValues("c_contentIntroduction");
				String parse_cContentIntroduction = str_cContentIntroduction[0];
				String[] cContentIntroduction = parse_cContentIntroduction.split(",");
				
				String[] str_cContentVideoType = request.getParameterValues("c_contentVideoType");
				String parse_cContentVideoType = str_cContentVideoType[0];
				String[] cContentVideoType = parse_cContentVideoType.split(",");
				
				String[] str_cContentVideoUrl = request.getParameterValues("c_contentVideoUrl");
				String parse_cContentVideoUrl = str_cContentVideoUrl[0];
				String[] cContentVideoUrl = parse_cContentVideoUrl.split(",");
				
				String[] str_cEngName = request.getParameterValues("c_engName");
				String parse_cEngName = str_cEngName[0];
				String[] cEngName = parse_cEngName.split(",");
				
				String[] str_cEngContentIntroduction = request.getParameterValues("c_engContentIntroduction");
				String parse_cEngContentIntroduction = str_cEngContentIntroduction[0];
				String[] cEngContentIntroduction = parse_cEngContentIntroduction.split(",");
				
				CommonResponse logo_res = uploadService.upload(logo, "/ncf/user/image");
				if (logo_res.getResultCode().equals(ResultCode.SUCCESS)) e.setLogo(logo_res.getMessage());
				else return UserResponse.of("로고 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
				if (brochure != null) {
					CommonResponse brochure_res = uploadService.upload(brochure, "/ncf/user/file");
					if (brochure_res.getResultCode().equals(ResultCode.SUCCESS)) e.setBrochure(brochure_res.getMessage());
					else return UserResponse.of("브로슈어 파일 등록에 실패 했습니다.", ResultCode.FAIL, null);
				}
				CommonResponse engLogo_res = uploadService.upload(engLogo, "/ncf/user/image");
				if (engLogo_res.getResultCode().equals(ResultCode.SUCCESS)) e.setEngLogo(engLogo_res.getMessage());
				else return UserResponse.of("영문 로고 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
				
				if (c_length == 1) {
					cThumbnail1 = request.getFile("c_thumbnail1");
					if (cThumbnail1 != null) {
						CommonResponse cThumbnail_1 = uploadService.upload(cThumbnail1, "/ncf/user/image");
						if (cThumbnail_1.getResultCode().equals(ResultCode.SUCCESS)) cThumbnailPath1 = cThumbnail_1.getMessage();
						else return UserResponse.of("콘텐츠 썸네일 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
					} else {
						cThumbnailPath1 = null;
					}
					
					c_genre1 = request.getParameterValues("c_genre1");
					String parseGenre1 = c_genre1[0];
					cGenre1 = parseGenre1.split(",");
				} else if (c_length == 2) {
					cThumbnail1 = request.getFile("c_thumbnail1");
					cThumbnail2 = request.getFile("c_thumbnail2");
					System.out.println("---------------------------thumbnail---------------------------");
					System.out.println(cThumbnail1);
					System.out.println(cThumbnail2);
					if (cThumbnail1 != null) {
						CommonResponse cThumbnail_1 = uploadService.upload(cThumbnail1, "/ncf/user/image");
						if (cThumbnail_1.getResultCode().equals(ResultCode.SUCCESS)) {
							System.out.println("cThumbnailPath1 : " + cThumbnail_1.getMessage());
							cThumbnailPath1 = cThumbnail_1.getMessage();
						} else {
							return UserResponse.of("콘텐츠 썸네일 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
						}
					} else {
						cThumbnailPath1 = null;
					}
					if (cThumbnail2 != null) {
						CommonResponse cThumbnail_2 = uploadService.upload(cThumbnail2, "/ncf/user/image");
						if (cThumbnail_2.getResultCode().equals(ResultCode.SUCCESS)) {
							System.out.println("cThumbnailPath2 : " + cThumbnail_2.getMessage());
							cThumbnailPath2 = cThumbnail_2.getMessage();
						} else {
							return UserResponse.of("콘텐츠 썸네일 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
						}
					} else {
						cThumbnailPath2 = null;
					}
					c_genre1 = request.getParameterValues("c_genre1");
					c_genre2 = request.getParameterValues("c_genre2");
					String parseGenre1 = c_genre1[0];
					String parseGenre2 = c_genre2[0];
					cGenre1 = parseGenre1.split(",");
					cGenre2 = parseGenre2.split(",");
				} else if (c_length == 3) {
					cThumbnail1 = request.getFile("c_thumbnail1");
					cThumbnail2 = request.getFile("c_thumbnail2");
					cThumbnail3 = request.getFile("c_thumbnail3");
					if (cThumbnail1 != null) {
						CommonResponse cThumbnail_1 = uploadService.upload(cThumbnail1, "/ncf/user/image");
						if (cThumbnail_1.getResultCode().equals(ResultCode.SUCCESS)) cThumbnailPath1 = cThumbnail_1.getMessage();
						else return UserResponse.of("콘텐츠 썸네일 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
					} else {
						cThumbnailPath1 = null;
					}
					if (cThumbnail2 != null) {
						CommonResponse cThumbnail_2 = uploadService.upload(cThumbnail2, "/ncf/user/image");
						if (cThumbnail_2.getResultCode().equals(ResultCode.SUCCESS)) cThumbnailPath2 = cThumbnail_2.getMessage();
						else return UserResponse.of("콘텐츠 썸네일 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
					} else {
						cThumbnailPath2 = null;
					}
					if (cThumbnail3 != null) {
						CommonResponse cThumbnail_3 = uploadService.upload(cThumbnail3, "/ncf/user/image");
						if (cThumbnail_3.getResultCode().equals(ResultCode.SUCCESS)) cThumbnailPath3 = cThumbnail_3.getMessage();
						else return UserResponse.of("콘텐츠 썸네일 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
					} else {
						cThumbnailPath3 = null;
					}
					c_genre1 = request.getParameterValues("c_genre1");
					c_genre2 = request.getParameterValues("c_genre2");
					c_genre3 = request.getParameterValues("c_genre3");
					String parseGenre1 = c_genre1[0];
					String parseGenre2 = c_genre2[0];
					String parseGenre3 = c_genre3[0];
					cGenre1 = parseGenre1.split(",");
					cGenre2 = parseGenre2.split(",");
					cGenre3 = parseGenre3.split(",");
				} else {
					cThumbnail1 = request.getFile("c_thumbnail1");
					cThumbnail2 = request.getFile("c_thumbnail2");
					cThumbnail3 = request.getFile("c_thumbnail3");
					cThumbnail4 = request.getFile("c_thumbnail4");
					if (cThumbnail1 != null) {
						System.out.println("cThumbnail1 != null");
						CommonResponse cThumbnail_1 = uploadService.upload(cThumbnail1, "/ncf/user/image");
						if (cThumbnail_1.getResultCode().equals(ResultCode.SUCCESS)) {
							System.out.println(cThumbnail_1.getMessage());
							cThumbnailPath1 = cThumbnail_1.getMessage();
						} else {
							return UserResponse.of("콘텐츠 썸네일 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
						}
					} else {
						cThumbnailPath1 = null;
					}
					if (cThumbnail2 != null) {
						System.out.println("cThumbnail2 != null");
						CommonResponse cThumbnail_2 = uploadService.upload(cThumbnail2, "/ncf/user/image");
						if (cThumbnail_2.getResultCode().equals(ResultCode.SUCCESS)) {
							System.out.println(cThumbnail_2.getMessage());
							cThumbnailPath2 = cThumbnail_2.getMessage();
						} else {
							return UserResponse.of("콘텐츠 썸네일 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
						}
					} else {
						cThumbnailPath2 = null;
					}
					if (cThumbnail3 != null) {
						CommonResponse cThumbnail_3 = uploadService.upload(cThumbnail3, "/ncf/user/image");
						if (cThumbnail_3.getResultCode().equals(ResultCode.SUCCESS)) cThumbnailPath3 = cThumbnail_3.getMessage();
						else return UserResponse.of("콘텐츠 썸네일 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
					} else {
						cThumbnailPath3 = null;
					}
					if (cThumbnail4 != null) {
						CommonResponse cThumbnail_4 = uploadService.upload(cThumbnail4, "/ncf/user/image");
						if (cThumbnail_4.getResultCode().equals(ResultCode.SUCCESS)) cThumbnailPath4 = cThumbnail_4.getMessage();
						else return UserResponse.of("콘텐츠 썸네일 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
					} else {
						cThumbnailPath4 = null;
					}
					c_genre1 = request.getParameterValues("c_genre1");
					c_genre2 = request.getParameterValues("c_genre2");
					c_genre3 = request.getParameterValues("c_genre3");
					c_genre4 = request.getParameterValues("c_genre4");
					String parseGenre1 = c_genre1[0];
					String parseGenre2 = c_genre2[0];
					String parseGenre3 = c_genre3[0];
					String parseGenre4 = c_genre4[0];
					cGenre1 = parseGenre1.split(",");
					cGenre2 = parseGenre2.split(",");
					cGenre3 = parseGenre3.split(",");
					cGenre4 = parseGenre4.split(",");
				}
				
				user.setUserId(userId);
				user.setPassword(password);
				user.setEmail(email);
				user.setJoinType(joinType);
				user.setStatus(0);
				user.setRoleType(1);
				
				user = userRepository.save(user);
				
				UserRole.Id userRoleId = null;
				UserRole ur = new UserRole();
				List<UserRole> urs = new ArrayList<>();
				
				userRoleId = new UserRole.Id(userId, Role.WAIT);
				ur.setRole(Role.WAIT);
				ur.setId(userRoleId);
				urs.add(ur);
				userRoleRepository.save(ur);
				user.setRoles(urs);
				userRepository.save(user);
				
				e.setAboutUs(aboutUs);
				e.setManagerName(managerName);
				e.setCeoName(ceoName);
				e.setCompanyName(companyName);
				e.setEstablishYear(establishYear);
				if (Integer.parseInt(establishMonth) < 10) establishMonth = "0"+establishMonth;
				e.setEstablishMonth(establishMonth);
				if (Integer.parseInt(establishDay) < 10) establishDay = "0"+establishDay;
				e.setEstablishDay(establishDay);
				e.setEngAboutUs(engAboutUs);
				e.setEngCeoName(engCeoName);
				e.setEngCompanyName(engCompanyName);
				e.setEngManagerName(engManagerName);
				e.setEngCity(engCity);
				e.setTel(tel);
				e.setNationalCode(nationalCode);
				e.setFrontPhone(frontPhone);
				e.setPhone(phone);
				e.setPosition(position);
				e.setSite(site);
				e.setStatus(1);
				if (promotionalVideoType != null && !promotionalVideoType.equals("2") && !promotionalVideoType.equals("") && !promotionalVideoType.equals("null")) {
					e.setPromotionalVideoType(Integer.parseInt(promotionalVideoType));
					try {
						if (promotionalVideoType.equals("0")) { // youtube
							System.out.println("if");
							promotionalVideoUrl = promotionalVideoUrl.substring(promotionalVideoUrl.length()-11, promotionalVideoUrl.length());
						} else { // vimeo
							System.out.println("else");
							promotionalVideoUrl = promotionalVideoUrl.substring(promotionalVideoUrl.length()-9, promotionalVideoUrl.length());
						}
					} catch (Exception ex) {
						promotionalVideoUrl = "";
					}
					System.out.println("promotionalVideoUrl : " + promotionalVideoUrl);
					e.setPromotionalVideoUrl(promotionalVideoUrl);
				} else {
					e.setPromotionalVideoType(2);
					e.setPromotionalVideoUrl(null);
				}
				e.setPrivacyAgree(0);
				
				e = exhibitorsRepository.save(e);
				
				for (int i=0; i<category.length; i++) {
					ExhibitorsCategory ec = new ExhibitorsCategory();
					Category c = categoryRepository.findByCid(Long.parseLong(category[i]));
					ec.setExhibitors(e);
					ec.setCategory(c);
					exhibitorsCategoryRepository.save(ec);
					ecs.add(ec);
				}
				e.setExhibitorsCategory(ecs);
				
				for (int i=0; i<c_length; i++) {
					
					Content c = new Content();
					ExhibitorsContent ec = new ExhibitorsContent();
					
					if (i == 0) {
						System.out.println("if : " + cThumbnailPath1);
						c.setThumbnail(cThumbnailPath1);
					} else if (i == 1) {
						System.out.println("else if : " + cThumbnailPath2);
						c.setThumbnail(cThumbnailPath2);
					} else if (i == 2) {
						System.out.println("else if 2 : " + cThumbnailPath3);
						c.setThumbnail(cThumbnailPath3);
					} else if (i == 3) {
						System.out.println("else if 3 : " + cThumbnailPath4);
						c.setThumbnail(cThumbnailPath4);
					}
					
					if (cName.length != 0) {
						if (cName[i] != null && !cName[i].equals("")) c.setName(cName[i]);
					}
					
					if (cYearOfProduction[i] != null && !cYearOfProduction[i].equals("")) c.setYearOfProduction(cYearOfProduction[i]);
					if (cContentIntroduction.length != 0) {
						if (cContentIntroduction[i] != null && !cContentIntroduction[i].equals("")) c.setContentIntroduction(cContentIntroduction[i]);
					}
					
					if (cContentVideoType[i] != null && !cContentVideoType[i].equals("2") && !cContentVideoType[i].equals("") && !cContentVideoType[i].equals("null")) {
						c.setContentVideoType(Integer.parseInt(cContentVideoType[i]));
						try {
							if (cContentVideoType[i].equals("0")) { // youtube
								int idx1 = cContentVideoUrl[i].lastIndexOf("/");
								cContentVideoUrl[i] = cContentVideoUrl[i].substring((idx1+1));
								cContentVideoUrl[i] = cContentVideoUrl[i].replace("watch?v=", "");
								int idx2 = cContentVideoUrl[i].indexOf("&t=");
								if (idx2 != -1) cContentVideoUrl[i] = cContentVideoUrl[i].substring(0, idx2);
								int idx3 = cContentVideoUrl[i].indexOf("?");
								if (idx3 != -1) cContentVideoUrl[i] = cContentVideoUrl[i].substring(0, idx3);
							} else { // vimeo
								System.out.println("else");
								cContentVideoUrl[i] = cContentVideoUrl[i].substring(cContentVideoUrl[i].length()-9, cContentVideoUrl[i].length());
							}
						} catch (Exception ex) {
							cContentVideoUrl[i] = "";
						}
						System.out.println("cContentVideoUrl[i] : " + cContentVideoUrl[i]);
						c.setContentVideoUrl(cContentVideoUrl[i]);
					} else {
						c.setContentVideoType(2);
						c.setContentVideoUrl(null);
					}
					
					c.setEngName(cEngName[i]);
					c.setEngContentIntroduction(cEngContentIntroduction[i]);
					
					c = contentRepository.save(c);
					
					if (i == 0) {
						List<ContentGenre> cgs = new ArrayList<>();
						for (int j=0; j<cGenre1.length; j++) {
							Genre g = genreRepository.findByGid(Long.parseLong(cGenre1[j]));
							ContentGenre cg = new ContentGenre();
							cg.setGenre(g);
							cg.setContent(c);
							cgs.add(cg);
						}
						c.setContentGenre(cgs);
					} else if (i == 1) {
						List<ContentGenre> cgs = new ArrayList<>();
						for (int j=0; j<cGenre2.length; j++) {
							Genre g = genreRepository.findByGid(Long.parseLong(cGenre2[j]));
							ContentGenre cg = new ContentGenre();
							cg.setGenre(g);
							cg.setContent(c);
							cgs.add(cg);
						}
						c.setContentGenre(cgs);
					} else if (i == 2) {
						List<ContentGenre> cgs = new ArrayList<>();
						for (int j=0; j<cGenre3.length; j++) {
							Genre g = genreRepository.findByGid(Long.parseLong(cGenre3[j]));
							ContentGenre cg = new ContentGenre();
							cg.setGenre(g);
							cg.setContent(c);
							cgs.add(cg);
						}
						c.setContentGenre(cgs);
					} else {
						List<ContentGenre> cgs = new ArrayList<>();
						for (int j=0; j<cGenre4.length; j++) {
							Genre g = genreRepository.findByGid(Long.parseLong(cGenre4[j]));
							ContentGenre cg = new ContentGenre();
							cg.setGenre(g);
							cg.setContent(c);
							cgs.add(cg);
						}
						c.setContentGenre(cgs);
					}
					c = contentRepository.save(c);
					ec.setContent(c);
					ec.setExhibitors(e);
					ec = exhibitorsContentRepository.save(ec);
					ecs2.add(ec);
				}
				e.setExhibitorsContent(ecs2);
				
				String parseGenre = genre[0];
				String[] genres = parseGenre.split(",");
				System.out.println("genres.length : " + genres.length);
				for (int i=0; i<genres.length; i++) {
					Genre g = genreRepository.findByGid(Long.parseLong(genres[i]));
					ExhibitorsGenre eg = new ExhibitorsGenre();
					eg.setExhibitors(e);
					eg.setGenre(g);
					eg = exhibitorsGenreRepository.save(eg);
					egs.add(eg);
				}
				e.setExhibitorsGenre(egs);
				e = exhibitorsRepository.save(e);
				user.setExhibitors(e);
				return UserResponse.of("success", ResultCode.SUCCESS, user);
				
			} else if (type.equals("buyer")) {
				
				if (userRepository.findByUserId(userId) != null) return UserResponse.of("이미 사용중인 아이디입니다.", ResultCode.FAIL, null);
				if (userRepository.findByEmail(email) != null) return UserResponse.of("이미 사용중인 이메일입니다.", ResultCode.FAIL, null);
				
				MultipartFile logo = request.getFile("logo");
				String companyName = request.getParameter("companyName");
				String country = request.getParameter("country");
				String site = request.getParameter("site");
				String aboutUs = request.getParameter("aboutUs");
				int languageSpoken = Integer.parseInt(request.getParameter("languageSpoken"));
				int division = Integer.parseInt(request.getParameter("division"));
				int counselingPlace = Integer.parseInt(request.getParameter("counselingPlace"));
				String companyAddress = request.getParameter("companyAddress");
				String managerName = request.getParameter("managerName");
				String frontPhone = request.getParameter("frontPhone");
				String phone = request.getParameter("phone");
				String position = request.getParameter("position");
				String nationalCode = request.getParameter("nationalCode");
				String tel = request.getParameter("tel");
				MultipartFile businessCard = request.getFile("businessCard");
				String lang = request.getParameter("lang");
				
				String[] genre = request.getParameterValues("genre");
				
				List<BuyerGenre> bgs = new ArrayList<>();
				Buyer b = new Buyer();
				
				CommonResponse logo_res = uploadService.upload(logo, "/ncf/user/image");
				if (logo_res.getResultCode().equals(ResultCode.SUCCESS)) b.setLogo(logo_res.getMessage());
				else return UserResponse.of("로고 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
				
				CommonResponse business_res = uploadService.upload(businessCard, "/ncf/user/image");
				if (business_res.getResultCode().equals(ResultCode.SUCCESS)) b.setBusinessCard(business_res.getMessage());
				else return UserResponse.of("명함 이미지 등록에 실패 했습니다.", ResultCode.FAIL, null);
				
				String parseGenre = genre[0];
				String[] genres = parseGenre.split(",");
				for (int i=0; i<genre.length; i++) {
					Genre g = genreRepository.findByGid(Long.parseLong(genres[i]));
					BuyerGenre bg = new BuyerGenre();
					bg.setBuyer(b);
					bg.setGenre(g);
					bg = buyerGenreRepository.save(bg);
					bgs.add(bg);
				}
				
				user.setUserId(userId);
				user.setPassword(password);
				user.setEmail(email);
				user.setJoinType(joinType);
				user.setStatus(0);
				user.setRoleType(2);
				
				user = userRepository.save(user);
				
				UserRole.Id userRoleId = null;
				UserRole ur = new UserRole();
				List<UserRole> urs = new ArrayList<>();
				
				userRoleId = new UserRole.Id(userId, Role.WAIT);
				ur.setRole(Role.WAIT);
				ur.setId(userRoleId);
				urs.add(ur);
				userRoleRepository.save(ur);
				user.setRoles(urs);
				userRepository.save(user);
				
				b.setCompanyName(companyName);
				b.setCountry(country);
				b.setSite(site);
				b.setBuyerGenre(bgs);
				b.setAboutUs(aboutUs);
				b.setLanguageSpoken(languageSpoken);
				b.setDivision(division);
				b.setCounselingPlace(counselingPlace);
				b.setCompanyAddress(companyAddress);
				b.setManagerName(managerName);
				b.setFrontPhone(frontPhone);
				b.setPhone(phone);
				b.setPosition(position);
				b.setNationalCode(nationalCode);
				b.setTel(tel);
				b.setLang(lang);
				b.setStatus(1);
				
				b = buyerRepository.save(b);
				user.setBuyer(b);
				user = userRepository.save(user);
				return UserResponse.of("success", ResultCode.SUCCESS, user);
				
			} else if (type.equals("visitor")) {
				
				String phone = request.getParameter("phone");
				String position = request.getParameter("position");
				
				user.setUserId(userId);
				user.setPassword(password);
				user.setJoinType(joinType);
				user.setEmail(email);
				user.setRoleType(0);
				user = userRepository.save(user);
				
				Visitor visitor = new Visitor();
				
				visitor.setPhone("010"+phone);
				visitor.setPosition(position);
				visitor = visitorRepository.save(visitor);
				
				user.setVisitor(visitor);
				
				UserRole.Id userRoleId = null;
				UserRole ur = new UserRole();
				List<UserRole> urs = new ArrayList<>();
				
				userRoleId = new UserRole.Id(userId, Role.VISITOR);
				ur.setRole(Role.VISITOR);
				ur.setId(userRoleId);
				urs.add(ur);
				userRoleRepository.save(ur);
				user.setRoles(urs);
				userRepository.save(user);
				
				return UserResponse.of("가입 되었습니다.", ResultCode.SUCCESS, user);
				
			} else {
				
				return UserResponse.of("올바른 경로로 접근 해주세요.", ResultCode.FAIL, null);
				
			}
		}
	}
	
	@Override
	public List<User> rpByExhibitors(int page, int count, String sort, String record, String search, Long[] cIds) {
		if (page != 0) page = (page * count);
		
		
		return null;
	}
	
	@Override
	public User findOne(String userId) {
		User user = userRepository.findByUserId(userId);
		if (user == null) return null;
		else return user;
	}
	
	@Override
	public User findByExhibitors(Long eId) {
		Exhibitors e = exhibitorsRepository.findByEidAndStatus(eId, 0);
		return userRepository.findByExhibitors(e);
	}

	@Override
	public User findByBuyer(Long bId) {
		Buyer b = buyerRepository.findByBid(bId);
		return userRepository.findByBuyer(b);
	}

}
