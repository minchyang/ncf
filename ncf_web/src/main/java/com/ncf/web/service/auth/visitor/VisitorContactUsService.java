package com.ncf.web.service.auth.visitor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.ncf.web.entity.ContactUs;
import com.ncf.web.entity.User;
import com.ncf.web.response.CommonResponse;

public interface VisitorContactUsService {

	public CommonResponse insert(HttpServletRequest request, User questioner);
	public List<ContactUs> qRp(int page, int count, String sort, String record, String searchType, String search, User questioner);
	
}
