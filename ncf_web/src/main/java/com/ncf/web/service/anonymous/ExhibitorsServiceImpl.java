package com.ncf.web.service.anonymous;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.web.entity.Exhibitors;
import com.ncf.web.entity.Genre;
import com.ncf.web.entity.Role;
import com.ncf.web.repository.ExhibitorsRepository;
import com.ncf.web.repository.GenreRepository;
import com.ncf.web.repository.specification.ExhibitorsSpecification;
import com.ncf.web.repository.specification.UserSpecification;
import com.ncf.web.response.ExhibitorsObjectResponse;
import com.ncf.web.response.ExhibitorsResponse;
import com.ncf.web.response.result.ResultCode;

@Service
public class ExhibitorsServiceImpl implements ExhibitorsService {

	@Autowired private UserSpecification userSpecification;
	@Autowired private ExhibitorsSpecification exhibitorsSpecification;
	@Autowired private GenreRepository genreRepository;
	@Autowired private ExhibitorsRepository exhibitorsRepository;
	
	@Override
	public Exhibitors ro(Long eId) {
		return exhibitorsRepository.findByEidAndStatus(eId, 0);
	}
	
	@Override
	public ExhibitorsObjectResponse object(int page, int count, String sort, String record, 
			String search, Long gId) {
		Role role = Role.EXHIBITORS;
		List<Genre> gs = new ArrayList<>();
		if (gId == 0L) {
			gs = genreRepository.findAll();
		} else {
			gs.add(genreRepository.findByGid(gId));
		}
		List<ExhibitorsResponse> result = new ArrayList<>();
		List<Exhibitors> exs = new ArrayList<>();
		
		for (int i=0; i<gs.size(); i++) {
			exs = userSpecification.exhibitorsObject(search, gs.get(i).getGid(), role, 0);
			boolean tof = false;
			if (exs.size() == 8) tof = true;
			ExhibitorsResponse er = new ExhibitorsResponse(gs.get(i).getName(), gs.get(i).getEngName(), gs.get(i).getGid(), tof, exs);
			result.add(er);
		}
		return ExhibitorsObjectResponse.of("success", ResultCode.SUCCESS, result);
	}
	
	@Override
	public List<Exhibitors> participationArea(int page, int count, String sort, String record, String search,
			Long cId) {
		return exhibitorsSpecification.participationArea(page, count, sort, record, search, cId, 0);
	}
	
	@Override
	public List<Exhibitors> rp(int page, int count, String sort, String record, String search, Long gId) {
		if (page != 0) page = (page * count);
		return exhibitorsSpecification.rp(page, count, sort, record, search, gId, 0);
	}

}
