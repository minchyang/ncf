package com.ncf.web.service.anonymous;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.web.entity.Category;
import com.ncf.web.repository.CategoryRepository;
import com.ncf.web.repository.specification.CategorySpecification;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired private CategorySpecification categorySpecification;
	@Autowired private CategoryRepository categoryRepository;
	
	@Override
	public List<Category> rp(int page, int count, String sort, String record, String search, int status) {
		if (page != 0) page = (page * count);
		return categorySpecification.rp(page, count, sort, record, search, status);
	}
	
	@Override
	public Category ro(Long cId) {
		return categoryRepository.findByCid(cId);
	}

}
