package com.ncf.web.service.anonymous;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.web.entity.Content;
import com.ncf.web.repository.ContentRepository;

@Service
public class ContentServiceImpl implements ContentService {

	@Autowired private ContentRepository contentRepository;
	
	@Override
	public Content ro(Long cId) {
		return contentRepository.findByCid(cId);
	}
	
}
