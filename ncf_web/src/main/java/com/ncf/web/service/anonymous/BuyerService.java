package com.ncf.web.service.anonymous;

import java.util.List;

import com.ncf.web.entity.Buyer;
import com.ncf.web.response.BuyerObjectResponse;

public interface BuyerService {
	
	public BuyerObjectResponse object(int page, int count, String sort, String record, 
			String search, Long gId);
	public List<Buyer> rp(int page, int count, String sort, String record, String search, Long gId);

}
