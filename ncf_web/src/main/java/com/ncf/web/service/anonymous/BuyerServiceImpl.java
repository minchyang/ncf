package com.ncf.web.service.anonymous;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.web.entity.Buyer;
import com.ncf.web.entity.Genre;
import com.ncf.web.entity.Role;
import com.ncf.web.entity.User;
import com.ncf.web.repository.GenreRepository;
import com.ncf.web.repository.specification.BuyerSpecification;
import com.ncf.web.repository.specification.UserSpecification;
import com.ncf.web.response.BuyerObjectResponse;
import com.ncf.web.response.BuyerResponse;
import com.ncf.web.response.result.ResultCode;

@Service
public class BuyerServiceImpl implements BuyerService {

	@Autowired private UserSpecification userSpecification;
	@Autowired private BuyerSpecification buyerSpecification;
	@Autowired private GenreRepository genreRepository;
	
	@Override
	public BuyerObjectResponse object(int page, int count, String sort, String record, String search, Long gId) {
		Role role = Role.BUYER;
		List<Genre> gs = new ArrayList<>();
		if (gId == 0L) {
			gs = genreRepository.findAll();
		} else {
			gs.add(genreRepository.findByGid(gId));
		}
		List<BuyerResponse> result = new ArrayList<>();
		List<User> users = new ArrayList<>();
		
		for (int i=0; i<gs.size(); i++) {
			users = userSpecification.buyerObject(search, gs.get(i).getGid(), role, 0);
			boolean tof = false;
			if (users.size() == 8) tof = true;
			BuyerResponse er = new BuyerResponse(gs.get(i).getName(), gs.get(i).getEngName(), gs.get(i).getGid(), tof, users);
			result.add(er);
		}
		return BuyerObjectResponse.of("success", ResultCode.SUCCESS, result);
	}
	
	@Override
	public List<Buyer> rp(int page, int count, String sort, String record, String search, Long gId) {
		if (page != 0) page = (page * count);
		return buyerSpecification.rp(page, count, sort, record, search, gId, 0);
	}

}
