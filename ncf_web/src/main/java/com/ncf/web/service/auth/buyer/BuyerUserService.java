package com.ncf.web.service.auth.buyer;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.web.response.CommonResponse;

public interface BuyerUserService {

	public CommonResponse update(MultipartHttpServletRequest request);
	
}
