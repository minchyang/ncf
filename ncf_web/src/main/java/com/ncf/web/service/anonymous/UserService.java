package com.ncf.web.service.anonymous;

import java.util.List;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.web.entity.User;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.response.UserResponse;

public interface UserService {

	public User findByUserIdAndPassword(String userId, String password);
	public CommonResponse duplicateCheck(String column, String value);
	public UserResponse cud(MultipartHttpServletRequest request);
	public List<User> rpByExhibitors(int page, int count, String sort, String record, String search, Long[] cIds);
	public User findOne(String userId);
	public User findByExhibitors(Long eId);
	public User findByBuyer(Long bId);
	
}
