package com.ncf.web.service.anonymous;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ncf.web.common.DateUtil;
import com.ncf.web.common.FtpClientUtil;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.response.result.ResultCode;

import lombok.Getter;
import lombok.Setter;

@Service
public class UploadService {

	@Value("${property.ftp.host}")
	@Getter @Setter
	String ftpHost;
	
	@Value("${property.ftp.username}")
	@Getter @Setter
	String ftpUsername;
	
	@Value("${property.ftp.password}")
	@Getter @Setter
	String ftpPassword;
	
	public CommonResponse upload(MultipartFile mFile, String dir) {
		try {
			String uploadDir = dir+"/"+DateUtil.getDateString("yyyyMMdd")+"/";
			String ext = mFile.getOriginalFilename().substring(mFile.getOriginalFilename().indexOf("."));
			String name = "ncf_file";
			String fileName = name+"_"+DateUtil.getDateString("yyyyMMddHHmmssSSS")+ext;
			String path = uploadFile(mFile.getInputStream(), uploadDir, fileName);
			System.out.println("path : " + path);
			return CommonResponse.of(path, ResultCode.SUCCESS);
		} catch (IOException ie) {
			ie.printStackTrace();
			return CommonResponse.of("이미지 등록에 실패 했습니다.", ResultCode.FAIL);
		}
	}
	
	public CommonResponse uploadBlob(MultipartFile mFile, String fileName, String dir, String type) {
		try {
			String uploadDir = dir+"/"+DateUtil.getDateString("yyyyMMdd")+"/";
			String path = uploadFile(mFile.getInputStream(), uploadDir, fileName);
			System.out.println("path : " + path);
			return CommonResponse.of(path, ResultCode.SUCCESS);
		} catch (IOException ie) {
			ie.printStackTrace();
			return CommonResponse.of("이미지 등록에 실패 했습니다.", ResultCode.FAIL);
		}
	}
	
	public String uploadFile(InputStream file, String uploadDir, String filename) {
		System.out.println("ftpHost : " + ftpHost);
		System.out.println("username : " + ftpUsername);
		System.out.println("password : " + ftpPassword);
		FtpClientUtil f = new FtpClientUtil(ftpHost, 21, ftpUsername, ftpPassword);
		String result = null;
		System.out.println(file);
		System.out.println(uploadDir);
		System.out.println(filename);
		try {
			if (f.open()) {
				result = f.put(file, uploadDir, filename);
				f.close();
				f = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
}
