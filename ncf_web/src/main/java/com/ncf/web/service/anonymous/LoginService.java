package com.ncf.web.service.anonymous;

import javax.servlet.http.HttpServletRequest;

import com.ncf.web.response.UserResponse;

public interface LoginService {

	public UserResponse login(HttpServletRequest request);
	
}
