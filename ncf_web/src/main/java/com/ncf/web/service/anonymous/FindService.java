package com.ncf.web.service.anonymous;

import javax.servlet.http.HttpServletRequest;

import com.ncf.web.response.CommonResponse;

public interface FindService {

	public CommonResponse password(HttpServletRequest request);
	
}
