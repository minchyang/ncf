package com.ncf.web.service.auth.visitor;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncf.web.entity.User;
import com.ncf.web.entity.Visitor;
import com.ncf.web.repository.UserRepository;
import com.ncf.web.repository.VisitorRepository;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.response.result.ResultCode;

@Service
public class VisitorUserServiceImpl implements VisitorUserService {

	@Autowired private UserRepository userRepository;
	@Autowired private VisitorRepository visitorRepository;
	
	@Transactional
	@Override
	public CommonResponse update(HttpServletRequest request) {
		
		String userId = request.getParameter("userId");
		if (userId == null || userId.equals("")) return CommonResponse.of("아이디를 찾을 수 없습니다.", ResultCode.FAIL);
		User user = userRepository.findByUserId(userId);
		if (user == null) return CommonResponse.of("존재하지 않는 회원입니다.", ResultCode.FAIL);
		Visitor v = null;
		if (user.getVisitor() == null) v = new Visitor();
		else v = user.getVisitor();
		
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String position = request.getParameter("position");
		
		user.setEmail(email);
		v.setFrontPhone("010");
		v.setPhone(phone);
		v.setPosition(position);
		v = visitorRepository.save(v);
		user.setVisitor(v);
		userRepository.save(user);
		
		return CommonResponse.of("수정 되었습니다.", ResultCode.SUCCESS);
	}

}
