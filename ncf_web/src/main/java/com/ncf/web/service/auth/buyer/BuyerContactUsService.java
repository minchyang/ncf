package com.ncf.web.service.auth.buyer;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.ncf.web.entity.ContactUs;
import com.ncf.web.entity.User;
import com.ncf.web.response.CommonResponse;

public interface BuyerContactUsService {

	public CommonResponse insert(HttpServletRequest request, User questioner);
	public List<ContactUs> qRp(int page, int count, String sort, String record, String searchType, String search, User questioner);
	public List<ContactUs> aRp(int page, int count, String sort, String record, String searchType, String search, User answerer);
	
}
