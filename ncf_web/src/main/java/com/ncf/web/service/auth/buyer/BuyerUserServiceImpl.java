package com.ncf.web.service.auth.buyer;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.web.entity.Buyer;
import com.ncf.web.entity.BuyerGenre;
import com.ncf.web.entity.Genre;
import com.ncf.web.entity.User;
import com.ncf.web.repository.BuyerGenreRepository;
import com.ncf.web.repository.BuyerRepository;
import com.ncf.web.repository.GenreRepository;
import com.ncf.web.repository.UserRepository;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.response.result.ResultCode;
import com.ncf.web.service.anonymous.UploadService;

@Service
public class BuyerUserServiceImpl implements BuyerUserService {

	@Autowired private UploadService uploadService;
	@Autowired private UserRepository userRepository;
	@Autowired private BuyerGenreRepository buyerGenreRepository;
	@Autowired private GenreRepository genreRepository;
	@Autowired private BuyerRepository buyerRepository;
	
	@Transactional
	@Override
	public CommonResponse update(MultipartHttpServletRequest request) {
		
		String userId = request.getParameter("userId");
		if (userId == null || userId.equals("")) return CommonResponse.of("아이디를 찾을 수 없습니다.", ResultCode.FAIL);
		User user = userRepository.findByUserId(request.getParameter("userId"));
		if (user == null) return CommonResponse.of("존재하지 않는 회원입니다.", ResultCode.FAIL);
		
		Buyer b = user.getBuyer();
		
		String genreDefault = request.getParameter("genreDefault");
		String country = request.getParameter("country");
		String site = request.getParameter("site");
		MultipartFile logo = request.getFile("logo");
		String[] genre = request.getParameterValues("genre");
		String aboutUs = request.getParameter("aboutUs");
		String languageSpoken = request.getParameter("languageSpoken");
		String counselingPlace = request.getParameter("counselingPlace");
		String division = request.getParameter("division");
		String companyAddress = request.getParameter("companyAddress");
		String email = request.getParameter("email");
		String managerName = request.getParameter("managerName");
		String frontPhone = request.getParameter("frontPhone");
		String phone = request.getParameter("phone");
		String position = request.getParameter("position");
		String nationalCode = request.getParameter("nationalCode");
		String tel = request.getParameter("tel");
		MultipartFile businessCard = request.getFile("businessCard");
		
		System.out.println("userId : " + userId);
		System.out.println("genreDefault : " + genreDefault);
		System.out.println("site : " + site);
		System.out.println("genre : " + genre);
		System.out.println("logo : " + logo);
		System.out.println("aboutUs : " + aboutUs);
		System.out.println("languageSpoken : " + languageSpoken);
		System.out.println("counselingPlace : " + counselingPlace);
		System.out.println("division : " + division);
		System.out.println("companyAddress : " + companyAddress);
		System.out.println("email : " + email);
		System.out.println("managerName : " + managerName);
		System.out.println("frontPhone : " + frontPhone);
		System.out.println("phone : " + phone);
		System.out.println("position : " + position);
		System.out.println("nationalCode : " + nationalCode);
		System.out.println("tel : " + tel);
		System.out.println("buinessCard : " + businessCard);
		
		List<BuyerGenre> bgs = new ArrayList<>();
		if (genreDefault.equals("1")) {
			String parseGenre = genre[0];
			String[] genres = parseGenre.split(",");
			List<BuyerGenre> prevGenre = b.getBuyerGenre();
			b.setBuyerGenre(null);
			buyerRepository.save(b);
			for (int i=0; i<prevGenre.size(); i++) {
				BuyerGenre bg = buyerGenreRepository.findByBgId(prevGenre.get(i).getBgId());
				buyerGenreRepository.delete(bg);
			}
			
			for (int i=0; i<genres.length; i++) {
				Genre g = genreRepository.findByGid(Long.parseLong(genres[i]));
				BuyerGenre bg = new BuyerGenre();
				bg.setBuyer(b);
				bg.setGenre(g);
				bg = buyerGenreRepository.save(bg);
				bgs.add(bg);
			}
		}
		
		if (logo != null) {
			CommonResponse logo_res = uploadService.upload(logo, "/ncf/user/image");
			System.out.println("logo_res.getMessage()");
			System.out.println(logo_res.getMessage());
			if (logo_res.getResultCode().equals(ResultCode.SUCCESS)) b.setLogo(logo_res.getMessage());
			else return CommonResponse.of("로고 이미지 등록에 실패 했습니다.", ResultCode.FAIL);
		}
		if (businessCard != null) {
			CommonResponse business_res = uploadService.upload(businessCard, "/ncf/user/image");
			System.out.println("business_res.getMessage()");
			System.out.println(business_res.getMessage());
			if (business_res.getResultCode().equals(ResultCode.SUCCESS)) b.setBusinessCard(business_res.getMessage());
			else return CommonResponse.of("명함 이미지 등록에 실패 했습니다.", ResultCode.FAIL);
		}
		
		b.setCountry(country);
		b.setSite(site);
		if (genreDefault.equals("1")) b.setBuyerGenre(bgs);
		b.setAboutUs(aboutUs);
		b.setLanguageSpoken(Integer.parseInt(languageSpoken));
		b.setCounselingPlace(Integer.parseInt(counselingPlace));
		b.setDivision(Integer.parseInt(division));
		b.setCompanyAddress(companyAddress);
		user.setEmail(email);
		b.setManagerName(managerName);
		b.setFrontPhone(frontPhone);
		b.setPhone(phone);
		b.setPosition(position);
		b.setNationalCode(nationalCode);
		b.setTel(tel);
		b = buyerRepository.save(b);
		user.setBuyer(b);
		userRepository.save(user);
		return CommonResponse.of("수정 되었습니다.", ResultCode.SUCCESS);
	}

}
