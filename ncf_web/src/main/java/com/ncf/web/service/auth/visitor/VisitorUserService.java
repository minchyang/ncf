package com.ncf.web.service.auth.visitor;

import javax.servlet.http.HttpServletRequest;

import com.ncf.web.response.CommonResponse;

public interface VisitorUserService {

	public CommonResponse update(HttpServletRequest request);
	
}
