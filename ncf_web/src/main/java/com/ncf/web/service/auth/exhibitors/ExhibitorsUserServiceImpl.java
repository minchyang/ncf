package com.ncf.web.service.auth.exhibitors;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.web.entity.Category;
import com.ncf.web.entity.Exhibitors;
import com.ncf.web.entity.ExhibitorsCategory;
import com.ncf.web.entity.ExhibitorsGenre;
import com.ncf.web.entity.Genre;
import com.ncf.web.entity.User;
import com.ncf.web.repository.CategoryRepository;
import com.ncf.web.repository.ExhibitorsCategoryRepository;
import com.ncf.web.repository.ExhibitorsGenreRepository;
import com.ncf.web.repository.ExhibitorsRepository;
import com.ncf.web.repository.GenreRepository;
import com.ncf.web.repository.UserRepository;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.response.result.ResultCode;
import com.ncf.web.service.anonymous.UploadService;

@Service
public class ExhibitorsUserServiceImpl implements ExhibitorsUserService {

	@Autowired private UploadService uploadService;
	@Autowired private UserRepository userRepository;
	@Autowired private ExhibitorsRepository exhibitorsRepository;
	@Autowired private ExhibitorsGenreRepository exhibitorsGenreRepository;
	@Autowired private GenreRepository genreRepository;
	@Autowired private ExhibitorsCategoryRepository exhibitorsCategoryRepository;
	@Autowired private CategoryRepository categoryRepository;
	
	@Transactional
	@Override
	public CommonResponse update(MultipartHttpServletRequest request) {
		
		String userId = request.getParameter("userId");
		if (userId == null || userId.equals("")) return CommonResponse.of("아이디를 찾을 수 없습니다.", ResultCode.FAIL);
		User user = userRepository.findByUserId(request.getParameter("userId"));
		if (user == null) return CommonResponse.of("존재하지 않는 회원입니다.", ResultCode.FAIL);
		
		Exhibitors e = user.getExhibitors();
		
		String genreDefault = request.getParameter("genreDefault");
		MultipartFile logo = request.getFile("logo");
		String companyName = request.getParameter("companyName");
		String site = request.getParameter("site");
		String email = request.getParameter("email");
		String managerName = request.getParameter("managerName");
		String frontPhone = request.getParameter("frontPhone");
		String phone = request.getParameter("phone");
		String establishYear = request.getParameter("establishYear");
		String establishMonth = request.getParameter("establishMonth");
		String establishDay = request.getParameter("establishDay");
		String ceoName = request.getParameter("ceoName");
		String[] genre = request.getParameterValues("genre");
		String[] category = request.getParameterValues("category");
		String promotionalVideoType = request.getParameter("promotionalVideoType");
		String promotionalVideoUrl = request.getParameter("promotionalVideoUrl");
		String aboutUs = request.getParameter("aboutUs");
		MultipartFile engLogo = request.getFile("engLogo");
		MultipartFile brochure = request.getFile("brochure");
		String engCompanyName = request.getParameter("engCompanyName");
		String engManagerName = request.getParameter("engManagerName");
		String engCity = request.getParameter("engCity");
		String engCeoName = request.getParameter("engCeoName");
		String engAboutUs = request.getParameter("engAboutUs");
		String position = request.getParameter("position");
		String nationalCode = request.getParameter("nationalCode");
		String tel = request.getParameter("tel");
		
		System.out.println("genreDefault : " + genreDefault);
		System.out.println("logo : " + logo);
		System.out.println("companyName : " + companyName);
		System.out.println("site : " + site);
		System.out.println("email : " + email);
		System.out.println("managerName : " + managerName);
		System.out.println("frontPhone : " + frontPhone);
		System.out.println("phone : " + phone);
		System.out.println("establishYear : " + establishYear);
		System.out.println("establishMonth : " + establishMonth);
		System.out.println("establishDay : " + establishDay);
		System.out.println("cenName : " + ceoName);
		System.out.println("genre : ");
		System.out.println(genre);
		System.out.println("category : ");
		System.out.println(category);
		System.out.println("promotionalVideoType : " + promotionalVideoType);
		System.out.println("promotionalVideUrl : " + promotionalVideoUrl);
		System.out.println("aboutUs : " + aboutUs);
		System.out.println("engLogo : " + engLogo);
		System.out.println("engCompanyName : " + engCompanyName);
		System.out.println("engManagerName : " + engManagerName);
		System.out.println("engCity : " + engCity);
		System.out.println("engCeoName : " + engCeoName);
		System.out.println("engAboutUs : " + engAboutUs);
		System.out.println("position : " + position);
		System.out.println("nationalCode : " + nationalCode);
		System.out.println("tel : " + tel);
		
		List<ExhibitorsGenre> egs = new ArrayList<>();
		if (genreDefault.equals("1")) {
			String parseGenre = genre[0];
			String[] genres = parseGenre.split(",");
			List<ExhibitorsGenre> prevGenre = e.getExhibitorsGenre();
			e.setExhibitorsGenre(null);
			exhibitorsRepository.save(e);
			for (int i=0; i<prevGenre.size(); i++) {
				ExhibitorsGenre eg = exhibitorsGenreRepository.findByEgId(prevGenre.get(i).getEgId());
				exhibitorsGenreRepository.delete(eg);
			}
			
			for(int i=0; i<genres.length; i++) {
				Genre g = genreRepository.findByGid(Long.parseLong(genres[i]));
				ExhibitorsGenre eg = new ExhibitorsGenre();
				eg.setExhibitors(e);
				eg.setGenre(g);
				eg = exhibitorsGenreRepository.save(eg);
				egs.add(eg);
			}
		}
		
		String parseCategory = category[0];
		String[] categorys = parseCategory.split(",");
		List<ExhibitorsCategory> prevCategory = e.getExhibitorsCategory();
		List<ExhibitorsCategory> ecs = null;
		if (Long.parseLong(categorys[0]) != prevCategory.get(0).getCategory().getCid()) {
			e.setExhibitorsCategory(null);
			exhibitorsRepository.save(e);
			ExhibitorsCategory ec = exhibitorsCategoryRepository.findByEcId(prevCategory.get(0).getEcId());
			exhibitorsCategoryRepository.delete(ec);
			ec = new ExhibitorsCategory();
			ecs = new ArrayList<>();
			Category c = categoryRepository.findByCid(Long.parseLong(categorys[0]));
			ec.setExhibitors(e);
			ec.setCategory(c);
			ec = exhibitorsCategoryRepository.save(ec);
			ecs.add(ec);
		}
		
		if (logo != null) {
			CommonResponse logo_res = uploadService.upload(logo, "/ncf/user/image");
			if (logo_res.getResultCode().equals(ResultCode.SUCCESS)) e.setLogo(logo_res.getMessage());
			else return CommonResponse.of("로고 이미지 등록에 실패 했습니다.", ResultCode.FAIL);
		}
		if (brochure != null) {
			System.out.println("brochure not null");
			CommonResponse logo_res = uploadService.upload(brochure, "/ncf/user/image");
			if (logo_res.getResultCode().equals(ResultCode.SUCCESS)) e.setBrochure(logo_res.getMessage());
			else return CommonResponse.of("브로슈어 파일 등록에 실패 했습니다.", ResultCode.FAIL);
		}
		if (engLogo != null) {
			CommonResponse logo_res = uploadService.upload(engLogo, "/ncf/user/image");
			if (logo_res.getResultCode().equals(ResultCode.SUCCESS)) e.setEngLogo(logo_res.getMessage());
			else return CommonResponse.of("영문 로고 이미지 등록에 실패 했습니다.", ResultCode.FAIL);
		}
		
		e.setCompanyName(companyName);
		e.setSite(site);
		user.setEmail(email);
		e.setManagerName(managerName);
		e.setFrontPhone(frontPhone);
		e.setPhone(phone);
		e.setEstablishYear(establishYear);
		e.setEstablishMonth(establishMonth);
		e.setEstablishDay(establishDay);
		e.setCeoName(ceoName);
		if (ecs != null) e.setExhibitorsCategory(ecs);
		if (genreDefault.equals("1")) e.setExhibitorsGenre(egs);
		if (promotionalVideoType != null && !promotionalVideoType.equals("2") && !promotionalVideoType.equals("") && !promotionalVideoType.equals("null")) {
			e.setPromotionalVideoType(Integer.parseInt(promotionalVideoType));
			try {
				if (promotionalVideoType.equals("0")) { // youtube
					System.out.println("if");
					promotionalVideoUrl = promotionalVideoUrl.substring(promotionalVideoUrl.length()-11, promotionalVideoUrl.length());
				} else { // vimeo
					System.out.println("else");
					promotionalVideoUrl = promotionalVideoUrl.substring(promotionalVideoUrl.length()-9, promotionalVideoUrl.length());
				}
			} catch (Exception ex) {
				promotionalVideoUrl = "";
			}
			e.setPromotionalVideoUrl(promotionalVideoUrl);
		} else {
			e.setPromotionalVideoType(2);
			e.setPromotionalVideoUrl(null);
		}
		e.setAboutUs(aboutUs);
		e.setEngCompanyName(engCompanyName);
		e.setEngManagerName(engManagerName);
		e.setEngCity(engCity);
		e.setEngCeoName(engCeoName);
		e.setEngAboutUs(engAboutUs);
		if (position != null && !position.equals("")) e.setPosition(position);
		if (nationalCode != null && !nationalCode.equals("")) e.setNationalCode(nationalCode);
		if (tel != null && !tel.equals("")) e.setTel(tel);
		
		exhibitorsRepository.save(e);
		userRepository.save(user);
		
		return CommonResponse.of("수정 되었습니다.", ResultCode.SUCCESS);
	}

}
