package com.ncf.web.service.anonymous;

import java.util.List;

import com.ncf.web.entity.Genre;

public interface GenreService {

	public List<Genre> rp(int page, int count, String sort, String record, String search, int status);
	public Genre ro(Long gId);
	
}
