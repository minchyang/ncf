package com.ncf.web.service.auth.exhibitors;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.web.entity.ExhibitorsContent;
import com.ncf.web.entity.User;
import com.ncf.web.response.CommonResponse;

public interface ExhibitorsContentService {

	public ExhibitorsContent findOne(Long ecId);
	public CommonResponse cud(User user, MultipartHttpServletRequest request);
	
}
