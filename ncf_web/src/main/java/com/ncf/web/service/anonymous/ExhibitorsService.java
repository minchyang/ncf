package com.ncf.web.service.anonymous;

import java.util.List;

import com.ncf.web.entity.Exhibitors;
import com.ncf.web.response.ExhibitorsObjectResponse;

public interface ExhibitorsService {

	public Exhibitors ro(Long eId);
	public ExhibitorsObjectResponse object(int page, int count, String sort, String record, 
			String search, Long gId);
	public List<Exhibitors> participationArea(int page, int count, String sort, String record, 
			String search, Long cId);
	public List<Exhibitors> rp(int page, int count, String sort, String record, String search, Long gId);
	
}
