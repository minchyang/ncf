package com.ncf.web.common.externalAPI.kakao;

import java.io.IOException;

import com.google.gson.GsonBuilder;
import com.ncf.web.NcfWebConstants;
import com.ncf.web.domain.kakao.KakaoAccessToken;
import com.ncf.web.domain.kakao.KakaoUserResponse;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class KakaoClient {

	private static final String API_AUTH_URL = "https://kauth.kakao.com";
	private static final String API_URL = "https://kapi.kakao.com";
	private Kakao kakao = null;
	
	public KakaoClient(String type) {
		if (type.equals("auth")) this.kakao = this.create1();
		else if (type.equals("api")) this.kakao = this.create2();
	}
	
	public KakaoAccessToken token(String code) {
		System.out.println("???");
		System.out.println(NcfWebConstants.KAKAO_APP_KEY);
		System.out.println(NcfWebConstants.KAKAO_REDIRECT_URL);
		System.out.println(NcfWebConstants.KAKAO_CLIENT_SECRET);
		Call<KakaoAccessToken> call = this.kakao.token("application/x-www-form-urlencoded;charset=utf-8", 
				"authorization_code", NcfWebConstants.KAKAO_APP_KEY, 
				NcfWebConstants.KAKAO_REDIRECT_URL, code, NcfWebConstants.KAKAO_CLIENT_SECRET);
		try {
			Response<KakaoAccessToken> response = call.execute();
			System.out.println(response.toString());
			if (response.isSuccessful()) return response.body();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
		return null;
	}
	
	public KakaoUserResponse userData(String token) {
		Call<KakaoUserResponse> call = this.kakao.userData("Bearer "+token, "application/x-www-form-urlencoded;charset=utf-8");
		try {
			Response<KakaoUserResponse> response = call.execute();
			if (response.isSuccessful()) return response.body();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
		return null;
	}
	
	protected Kakao create1() {
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(API_AUTH_URL)
				.addConverterFactory(buildGsonConverter())
				.build();
		return retrofit.create(Kakao.class);
	}
	protected Kakao create2() {
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(API_URL)
				.addConverterFactory(buildGsonConverter())
				.build();
		return retrofit.create(Kakao.class);
	}
	protected GsonConverterFactory buildGsonConverter() {
		return GsonConverterFactory.create(new GsonBuilder().setLenient().create());
	}
	
}
