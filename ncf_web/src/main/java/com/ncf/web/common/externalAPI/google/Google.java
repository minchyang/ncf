package com.ncf.web.common.externalAPI.google;

import com.ncf.web.domain.google.GoogleAccessToken;
import com.ncf.web.domain.google.GoogleUserResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Google {

	@POST("/token")
	Call<GoogleAccessToken> token(
		@Header("Content-Type") String contentType, 
		@Query("code") String code, 
		@Query("client_id") String client_id, 
		@Query("client_secret") String client_secret, 
		@Query("redirect_uri") String redirect_uri, 
		@Query("grant_type") String grant_type
	);
	
	@GET("/oauth2/v1/userinfo")
	Call<GoogleUserResponse> userData(
		@Header("Authorization") String access_token, 
		@Header("Content-Type") String contentType
	);
	
}
