package com.ncf.web.common.externalAPI.google;

import java.io.IOException;

import com.google.gson.GsonBuilder;
import com.ncf.web.NcfWebConstants;
import com.ncf.web.domain.google.GoogleAccessToken;
import com.ncf.web.domain.google.GoogleUserResponse;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GoogleClient {

	private static final String API_AUTH_URL = "https://oauth2.googleapis.com";
	private static final String API_URL = "https://www.googleapis.com";
	
	private Google google = null;
	
	public GoogleClient(String type) {
		if (type.equals("auth")) this.google = this.create1();
		else if (type.equals("api")) this.google = this.create2();
	}
	
	
	public GoogleAccessToken token(String code) {
		Call<GoogleAccessToken> call = this.google.token("application/x-www-form-urlencoded", code, 
				NcfWebConstants.GOOGLE_CLIENT_ID, NcfWebConstants.GOOGLE_CLIENT_SECRET, 
				NcfWebConstants.GOOGLE_OAUTH_REDIRECT_URL, "authorization_code");
		try {
			Response<GoogleAccessToken> response = call.execute();
			System.out.println(response.toString());
			if (response.isSuccessful()) return response.body();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
		return null;
	}
	
	public GoogleUserResponse userData(String token_type, String token) {
		System.out.println("token : " + token);
		Call<GoogleUserResponse> call = this.google.userData(token_type+" "+token, "application/x-www-form-urlencoded");
		try {
			Response<GoogleUserResponse> response = call.execute();
			System.out.println(response.toString());
			if (response.isSuccessful()) return response.body();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
		return null;
	}
	
	
	
	protected Google create1() {
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(API_AUTH_URL)
				.addConverterFactory(buildGsonConverter())
				.build();
		return retrofit.create(Google.class);
	}
	protected Google create2() {
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(API_URL)
				.addConverterFactory(buildGsonConverter())
				.build();
		return retrofit.create(Google.class);
	}
	protected GsonConverterFactory buildGsonConverter() {
		return GsonConverterFactory.create(new GsonBuilder().setLenient().create());
	}
	
}
