package com.ncf.web.common.externalAPI.kakao;

import com.ncf.web.domain.kakao.KakaoAccessToken;
import com.ncf.web.domain.kakao.KakaoUserResponse;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Kakao {

	@POST("/oauth/token")
	Call<KakaoAccessToken> token(
		@Header("Content-type") String contentType, 
		@Query("grant_type") String grant_type, 
		@Query("client_id") String client_id, 
		@Query("redirect_uri") String redirect_uri, 
		@Query("code") String code, 
		@Query("client_secret") String client_secret
	);
	
	@POST("/v2/user/me")
	Call<KakaoUserResponse> userData(
		@Header("Authorization") String access_token, 
		@Header("Content-type") String contentType
	);
	
}
