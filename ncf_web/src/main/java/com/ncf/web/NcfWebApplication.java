package com.ncf.web;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@ServletComponentScan
@EnableAutoConfiguration
@EnableScheduling
@SpringBootApplication
public class NcfWebApplication extends SpringBootServletInitializer {
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		TimeZone.setDefault(TimeZone.getTimeZone("UCT"));
        return application.sources(NcfWebApplication.class);
    }

	public static void main(String[] args) {
		SpringApplication.run(NcfWebApplication.class, args);
		String contentVideoUrl = "https://youtu.be/watch?v=JJO92GevZ4E";
		contentVideoUrl = contentVideoUrl.substring(contentVideoUrl.length()-11, contentVideoUrl.length());
		contentVideoUrl = contentVideoUrl.replace("watch?v=", "");
		int idx1 = contentVideoUrl.indexOf("&t=");
		if (idx1 != -1) contentVideoUrl = contentVideoUrl.substring(0, idx1);
		int idx2 = contentVideoUrl.indexOf("?");
		if (idx2 != -1) contentVideoUrl = contentVideoUrl.substring(0, idx2);
		System.out.println(contentVideoUrl);
	}

}
