package com.ncf.web.domain.kakao;

import org.springframework.context.annotation.Profile;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class KakaoAccount {

	@SerializedName("profile")
	@Getter @Setter
	Profile profile;
	
	@SerializedName("email")
	@Getter @Setter
	String email;
	
	@SerializedName("age_range")
	@Getter @Setter
	String age_range;
	
	@SerializedName("birthday")
	@Getter @Setter
	String birthday;
	
	@SerializedName("birthyear")
	@Getter @Setter
	String birthyear;
	
	@SerializedName("gender")
	@Getter @Setter
	String gender;
	
	@SerializedName("phone_number")
	@Getter @Setter
	String phone_number;
	
	@SerializedName("ci")
	@Getter @Setter
	String ci;
	
}
