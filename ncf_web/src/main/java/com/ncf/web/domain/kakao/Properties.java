package com.ncf.web.domain.kakao;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Properties {

	@SerializedName("nickname")
	@Getter @Setter
	String nickname;
	
	@SerializedName("profile_image_url")
	@Getter @Setter
	String profile_image_url;
	
	@SerializedName("thumbnail_image_url")
	@Getter @Setter
	String thumbnail_image_url;
	
}
