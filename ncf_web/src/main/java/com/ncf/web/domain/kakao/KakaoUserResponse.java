package com.ncf.web.domain.kakao;

import java.util.Properties;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class KakaoUserResponse {

	@SerializedName("id")
	@Getter @Setter
	Long id;
	
	@SerializedName("kakao_account")
	@Getter @Setter
	KakaoAccount kakao_account;
	
	@SerializedName("properties")
	@Getter @Setter
	Properties properties;
	
	@SerializedName("synched_at")
	@Getter @Setter
	String synched_at;
	
	@SerializedName("connected_at")
	@Getter @Setter
	String connected_at;
	
}
