package com.ncf.web.domain.google;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class GoogleAccessToken {
	
	@SerializedName("access_token")
	@Getter @Setter
	String access_token;
	
	@SerializedName("expires_in")
	@Getter @Setter
	int expires_in;
	
	@SerializedName("id_token")
	@Getter @Setter
	String id_token;
	
	@SerializedName("scope")
	@Getter @Setter
	String scope;
	
	@SerializedName("token_type")
	@Getter @Setter
	String token_type;
	
	@SerializedName("refresh_token")
	@Getter @Setter
	String refresh_token;
	
}
