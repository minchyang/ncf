package com.ncf.web.domain.kakao;

import com.google.gson.annotations.SerializedName;

public class KakaoAccessToken {

	@SerializedName("token_type")
	String token_type;
	
	@SerializedName("access_token")
	String access_token;
	
	@SerializedName("refresh_token")
	String refresh_token;
	
	@SerializedName("scope")
	String scope;
	
	@SerializedName("expires_in")
	int expires_in;
	
	@SerializedName("refresh_token_expires_in")
	int refresh_token_expires_in;
	
	public String getToken_type() {
		return token_type;
	}
	
	public String getAccess_token() {
		return access_token;
	}
	
	public String getRefresh_token() {
		return refresh_token;
	}
	
	public String getScope() {
		return scope;
	}
	
	public int getExpires_in() {
		return expires_in;
	}
	
	public int getRefresh_token_expires_in() {
		return refresh_token_expires_in;
	}
	
}
