package com.ncf.web.domain.google;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class GoogleUserResponse {

	@SerializedName("id")
	@Getter @Setter
	String id;
	
	@SerializedName("verified_email")
	@Getter @Setter
	Boolean verified_email;
	
	@SerializedName("email")
	@Getter @Setter
	String email;
	
	@SerializedName("picture")
	@Getter @Setter
	String picture;
	
}
