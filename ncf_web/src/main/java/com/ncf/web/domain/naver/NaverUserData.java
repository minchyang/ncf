package com.ncf.web.domain.naver;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class NaverUserData {

	@SerializedName("id")
	@Getter @Setter
	String id;
	
	@SerializedName("email")
	@Getter @Setter
	String email;
	
	@SerializedName("name")
	@Getter @Setter
	String name;
	
	@SerializedName("nickname")
	@Getter @Setter
	String nickname;
	
	@SerializedName("gender")
	@Getter @Setter
	String gender;
	
	@SerializedName("age")
	@Getter @Setter
	String age;
	
}
