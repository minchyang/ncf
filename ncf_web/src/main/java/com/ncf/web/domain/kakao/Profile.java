package com.ncf.web.domain.kakao;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Profile {

	@SerializedName("nickname")
	@Getter @Setter
	String nickname;
	
	@SerializedName("profile_image")
	@Getter @Setter
	String profile_image;
	
	@SerializedName("thumbnail_image_url")
	@Getter @Setter
	String thumbnail_image_url;
	
	@SerializedName("profile_needs_agreement")
	@Getter @Setter
	Boolean profile_needs_agreement;
	
}
