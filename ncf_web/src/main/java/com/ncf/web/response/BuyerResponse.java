package com.ncf.web.response;

import java.util.List;

import com.ncf.web.entity.User;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

public class BuyerResponse {
	
	@ApiModelProperty(value = "genre name")
	@Getter
	private final String name;
	
	@ApiModelProperty(value = "english genre name")
	@Getter
	private final String engName;
	
	@ApiModelProperty(value = "genre id")
	@Getter
	private final Long gId;
	
	@ApiModelProperty(value = "more true or false")
	@Getter
	private final boolean tof;
	
	@ApiModelProperty(value = "buyer(user) list maximum 8, List<User>")
	@Getter
	private final List<User> users;
	
	public BuyerResponse(final String name, final String engName, final Long gId, final boolean tof, 
			final List<User> users) {
		this.name = name;
		this.engName = engName;
		this.gId = gId;
		this.tof = tof;
		this.users = users;
	}
	public static BuyerResponse of(String name, String engName, Long gId, boolean tof, List<User> users) {
		return new BuyerResponse(name, engName, gId, tof, users);
	}
	
}
