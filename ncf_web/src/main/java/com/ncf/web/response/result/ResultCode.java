package com.ncf.web.response.result;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ResultCode{
	SUCCESS(200), FAIL(99), EMPTY_PHONE(10);
	
    private int resultCode;
    
    private ResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    @JsonValue
    public int getResultCode() {
        return resultCode;
    }
    
}
