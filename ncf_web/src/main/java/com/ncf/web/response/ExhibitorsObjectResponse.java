package com.ncf.web.response;

import java.util.Date;
import java.util.List;

import com.ncf.web.response.result.ResultCode;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

public class ExhibitorsObjectResponse {

	@ApiModelProperty(value = "200 : success, 99 : failed")
	@Getter
	private final ResultCode resultCode;
	
	@ApiModelProperty(value = "result message")
	@Getter
	private final String message;
	
	@Getter
	private final Date timestamp;
	
	@ApiModelProperty(value = "exhibitors item list, List<ExhibitorsResponse>")
	@Getter
	private final List<ExhibitorsResponse> exhibitorsResponse;
	
	protected ExhibitorsObjectResponse(final String message, final ResultCode resultCode, final List<ExhibitorsResponse> exhibitorsResponse) {
		this.message = message;
		this.resultCode = resultCode;
		this.exhibitorsResponse = exhibitorsResponse;
		this.timestamp = new java.util.Date();
	}
	public static ExhibitorsObjectResponse of(final String message, final ResultCode resultCode, final List<ExhibitorsResponse> exhibitorsResponse) {
		return new ExhibitorsObjectResponse(message, resultCode, exhibitorsResponse);
	}
	
}
