package com.ncf.web.response;

import java.util.Date;
import java.util.List;

import com.ncf.web.entity.Exhibitors;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

public class ExhibitorsResponse {
	
	@Getter
	private final Date timestamp;
	
	@ApiModelProperty(value = "genre name")
	@Getter
	private final String name;
	
	@ApiModelProperty(value = "english genre name")
	@Getter
	private final String engName;
	
	@ApiModelProperty(value = "genre id")
	@Getter
	private final Long gId;
	
	@ApiModelProperty(value = "more true or false")
	@Getter
	private final boolean tof;
	
	@ApiModelProperty(value = "exhibitors(user) list maximum 8, List<User>")
	@Getter
	private final List<Exhibitors> exhibitors;
	
	public ExhibitorsResponse(final String name, final String engName, final Long gId, final boolean tof, 
			final List<Exhibitors> exhibitors) {
		this.name = name;
		this.engName = engName;
		this.gId = gId;
		this.tof = tof;
		this.exhibitors = exhibitors;
		this.timestamp = new java.util.Date();
	}
	public static ExhibitorsResponse of(String name, String engName, Long gId, boolean tof, List<Exhibitors> exhibitors) {
		return new ExhibitorsResponse(name, engName, gId, tof, exhibitors);
	}
	
}
