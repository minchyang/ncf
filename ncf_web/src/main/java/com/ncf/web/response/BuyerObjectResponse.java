package com.ncf.web.response;

import java.util.Date;
import java.util.List;

import com.ncf.web.response.result.ResultCode;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

public class BuyerObjectResponse {
	
	@ApiModelProperty(value = "200 : success, 99 : failed")
	@Getter
	private final ResultCode resultCode;
	
	@ApiModelProperty(value = "result message")
	@Getter
	private final String message;
	
	@Getter
	private final Date timestamp;
	
	@ApiModelProperty(value = "buyer item list, List<BuyerResponse>")
	@Getter
	private final List<BuyerResponse> buyerResponse;
	
	protected BuyerObjectResponse(final String message, final ResultCode resultCode, final List<BuyerResponse> buyerResponse) {
		this.message = message;
		this.resultCode = resultCode;
		this.buyerResponse = buyerResponse;
		this.timestamp = new java.util.Date();
	}
	public static BuyerObjectResponse of(final String message, final ResultCode resultCode, final List<BuyerResponse> buyerResponse) {
		return new BuyerObjectResponse(message, resultCode, buyerResponse);
	}

}
