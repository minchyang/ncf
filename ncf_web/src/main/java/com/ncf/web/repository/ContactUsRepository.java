package com.ncf.web.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.web.entity.ContactUs;
import com.ncf.web.entity.User;

@Repository
@Qualifier(value = "contactUsRepository")
@RestResource(exported = false)
public interface ContactUsRepository extends JpaRepository<ContactUs, Long>, JpaSpecificationExecutor<ContactUs> {
	
	public Page<ContactUs> findBySubjectContainingAndQuestioner(String search, User questioner, Pageable pageable);
	public Page<ContactUs> findByDescriptionContainingAndQuestioner(String search, User questioner, Pageable pageable);
	
	@Query("select c from ContactUs c left join c.answerer a left join c.questioner q where (a.exhibitors.companyName LIKE CONCAT ('%',:search,'%') OR a.buyer.companyName LIKE CONCAT ('%',:search,'%')) AND q.userId=:questioner")
	public Page<ContactUs> findAll(@Param("search") String search, @Param("questioner") String qeustioner, Pageable pageable);
	
	public Page<ContactUs> findBySubjectContainingAndAnswerer(String search, User answerer, Pageable pageable);
	public Page<ContactUs> findByDescriptionContainingAndAnswerer(String search, User answerer, Pageable pageable);
	
	@Query("select c from ContactUs c left join c.answerer a left join c.questioner q where a.exhibitors.companyName LIKE CONCAT ('%',:search,'%') AND q.userId=:answerer")
	public Page<ContactUs> findAllOrderByRegdateDesc(@Param("search") String search, @Param("answerer") String answerer, Pageable pageable);
	
}
