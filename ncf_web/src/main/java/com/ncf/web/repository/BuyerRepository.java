package com.ncf.web.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.web.entity.Buyer;

@Repository
@Qualifier(value = "buyerRepository")
@RestResource(exported = false)
public interface BuyerRepository extends JpaRepository<Buyer, Long>, JpaSpecificationExecutor<Buyer> {

	public Buyer findByBid(Long bId);
	
}
