package com.ncf.web.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.web.entity.ExhibitorsGenre;

@Repository
@Qualifier(value = "exhibitorsGenreRepository")
@RestResource(exported = false)
public interface ExhibitorsGenreRepository extends JpaRepository<ExhibitorsGenre, Long>, JpaSpecificationExecutor<ExhibitorsGenre> {

	public ExhibitorsGenre findByEgId(Long egId);
	
}
