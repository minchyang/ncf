package com.ncf.web.repository.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.ncf.web.entity.Exhibitors;

@Service
public class ExhibitorsSpecification {

	@PersistenceContext EntityManager em;
	
	public List<Exhibitors> participationArea(int page, int count, String sort, String record, 
			String search, Long cId, int status) {
		System.out.println("search : " + search);
		String jpql = "SELECT DISTINCT e.eid, e.companyName, e.engCompanyName, e.logo FROM Exhibitors e "
				+ "LEFT JOIN e.exhibitorsCategory ec "
				+ "WHERE (e.companyName LIKE CONCAT ('%',:search,'%') "
				+ "OR e.engCompanyName LIKE CONCAT ('%',:search,'%')) ";
				if (cId != 0) jpql += "AND ec.category.cid = :cId ";
				jpql += "AND e.status = :status "
				+ "ORDER BY e."+record+" "+sort;
		Query query = em.createQuery(jpql);
		query.setParameter("search", search);
		if (cId != 0) query.setParameter("cId", cId);
		query.setParameter("status", status);
		query.setFirstResult(page);
		query.setMaxResults(count);
		List<Object> list = query.getResultList();
		List<Exhibitors> resultList = new ArrayList<>();
		for (Object obj : list) {
			Object[] results = (Object[])obj;
			Exhibitors e = new Exhibitors();
			e.setEid(Long.parseLong(results[0].toString()));
			e.setCompanyName(results[1].toString());
			e.setEngCompanyName(results[2].toString());
			e.setLogo(results[3].toString());
			resultList.add(e);
		}
		return resultList;
	}
	
	public List<Exhibitors> rp(int page, int count, String sort, String record, String search, Long gId, int status) {
		String jpql = "SELECT DISTINCT e.eid, e.companyName, e.engCompanyName, e.logo FROM Exhibitors e "
				+ "LEFT JOIN e.exhibitorsGenre eg "
				+ "WHERE (e.companyName LIKE CONCAT ('%',:search,'%') "
				+ "OR e.engCompanyName LIKE CONCAT ('%',:search,'%')) "
				+ "AND eg.genre.gid = :gId "
				+ "AND e.status = :status "
				+ "ORDER BY e."+record+" "+sort;
		Query query = em.createQuery(jpql);
		query.setParameter("search", search);
		query.setParameter("gId", gId);
		query.setParameter("status", status);
		query.setFirstResult(page);
		query.setMaxResults(count);
		List<Object> list = query.getResultList();
		List<Exhibitors> resultList = new ArrayList<>();
		for (Object obj : list) {
			Object[] results = (Object[])obj;
			Exhibitors e = new Exhibitors();
			e.setEid(Long.parseLong(results[0].toString()));
			e.setCompanyName(results[1].toString());
			e.setEngCompanyName(results[2].toString());
			e.setLogo(results[3].toString());
			resultList.add(e);
		}
		return resultList;
	}
	
}
