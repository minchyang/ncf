package com.ncf.web.repository.specification;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import com.ncf.web.entity.ContactUs;

@Service
public class ContactUsSpecification {

	@PersistenceContext EntityManager em;
	
	public List<ContactUs> qRp(int page, int count, String sort, String record, String searchType, String search, 
			String questioner) {
		String jpql = "SELECT DISTINCT c FROM ContactUs c "
				+ "LEFT JOIN c.questioner q "
				+ "LEFT JOIN c.answerer a "
				+ "LEFT JOIN a.exhibitors e "
				+ "LEFT JOIN a.buyer b ";
				if (searchType.equals("subject")) {
					jpql += "WHERE c.subject LIKE CONCAT ('%',:search,'%') ";
				} else if (searchType.equals("description")) {
					jpql += "WHERE c.description LIKE CONCAT ('%',:search,'%') ";
				} else {
					jpql += "WHERE (e.companyName LIKE CONCAT ('%',:search,'%') "
							+ "OR b.companyName LIKE CONCAT ('%',:search,'%')) ";
				}
				jpql += "AND q.userId = :questioner";
		TypedQuery<ContactUs> query = em.createQuery(jpql, ContactUs.class);
		query.setParameter("search", search);
		query.setParameter("questioner", questioner);
		query.setFirstResult(page);
		query.setMaxResults(count);
		return query.getResultList();
	}
	
	public List<ContactUs> aRp(int page, int count, String sort, String record, String searchType, String search, 
			String answerer) {
		String jpql = "SELECT DISTINCT c FROM ContactUs c "
				+ "LEFT JOIN c.answerer a "
				+ "LEFT JOIN c.questioner q "
				+ "LEFT JOIN q.exhibitors e "
				+ "LEFT JOIN q.buyer b ";
				if (searchType.equals("subject")) {
					jpql += "WHERE c.subject LIKE CONCAT ('%',:search,'%') ";
				} else if (searchType.equals("description")) {
					jpql += "WHERE c.description LIKE CONCAT ('%',:search,'%') ";
				} else {
					jpql += "WHERE (e.companyName LIKE CONCAT ('%',:search,'%') "
							+ "OR b.companyName LIKE CONCAT ('%',:search,'%')) ";
				}
				jpql += "AND a.userId = :answerer";
		TypedQuery<ContactUs> query = em.createQuery(jpql, ContactUs.class);
		query.setParameter("search", search);
		query.setParameter("answerer", answerer);
		query.setFirstResult(page);
		query.setMaxResults(count);
		return query.getResultList();
	}
	
}
