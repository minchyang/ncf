package com.ncf.web.repository.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.ncf.web.entity.Buyer;
import com.ncf.web.entity.Exhibitors;
import com.ncf.web.entity.Role;
import com.ncf.web.entity.User;

@Service
public class UserSpecification {

	@PersistenceContext private EntityManager em;
	
	public List<Exhibitors> exhibitorsObject(String search, Long gId, Role role, int status) {
		System.out.println("specification : " + gId);
		String jpql = "SELECT e.eid, e.logo, e.companyName, e.engCompanyName FROM Exhibitors e "
				+ "LEFT JOIN e.exhibitorsGenre eg "
				+ "WHERE (e.companyName LIKE CONCAT('%',:search,'%') "
				+ "OR e.engCompanyName LIKE CONCAT('%',:search,'%')) "
				+ "AND e.status = :status "
				+ "AND eg.genre.gid = :gId "
				+ "ORDER BY e.regDate desc";
		Query query = em.createQuery(jpql);
		query.setParameter("search", search);
		query.setParameter("gId", gId);
		query.setParameter("status", status);
		query.setFirstResult(0);
		query.setMaxResults(8);
		List<Object> list = query.getResultList();
		List<Exhibitors> resultList = new ArrayList<>();
		for (Object obj : list) {
			Object[] results = (Object[])obj;
			Exhibitors e = new Exhibitors();
			e.setEid(Long.parseLong(results[0].toString()));
			e.setLogo(results[1].toString());
			e.setCompanyName(results[2].toString());
			e.setEngCompanyName(results[3].toString());
			resultList.add(e);
		}
		return resultList;
	}
	
	public List<User> buyerObject(String search, Long gId, Role role, int status) {
		String jpql = "SELECT b.bid, b.logo, b.companyName FROM User u "
				+ "LEFT JOIN u.buyer b "
				+ "LEFT JOIN b.buyerGenre bg "
				+ "WHERE b.companyName LIKE CONCAT('%',:search,'%') "
				+ "AND bg.genre.gid = :gId "
				+ "AND b.status = :status "
				+ "ORDER BY u.regDate desc";
		Query query = em.createQuery(jpql);
		query.setParameter("search", search);
		query.setParameter("gId", gId);
		query.setParameter("status", status);
		query.setFirstResult(0);
		query.setMaxResults(8);
		List<Object> list = query.getResultList();
		List<User> resultList = new ArrayList<>();
		for (Object obj : list) {
			Object[] results = (Object[])obj;
			User u = new User();
			Buyer b = new Buyer();
			b.setBid(Long.parseLong(results[0].toString()));
			b.setLogo(results[1].toString());
			b.setCompanyName(results[2].toString());
			u.setBuyer(b);
			resultList.add(u);
		}
		return resultList;
	}
	
}
