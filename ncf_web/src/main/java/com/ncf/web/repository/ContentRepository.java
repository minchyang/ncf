package com.ncf.web.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.web.entity.Content;

@Repository
@Qualifier(value = "contentRepository")
@RestResource(exported = false)
public interface ContentRepository extends JpaRepository<Content, Long>, JpaSpecificationExecutor<Content> {

	public Content findByCid(Long cId);
	
}
