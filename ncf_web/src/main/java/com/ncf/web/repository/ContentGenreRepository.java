package com.ncf.web.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.web.entity.ContentGenre;

@Repository
@Qualifier(value = "contentGenreRepository")
@RestResource(exported = false)
public interface ContentGenreRepository extends JpaRepository<ContentGenre, Long>, JpaSpecificationExecutor<ContentGenre> {

	public ContentGenre findByCgId(Long cgId);
	
}
