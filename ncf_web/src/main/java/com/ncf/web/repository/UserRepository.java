package com.ncf.web.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.web.entity.Buyer;
import com.ncf.web.entity.Exhibitors;
import com.ncf.web.entity.Role;
import com.ncf.web.entity.User;

@Repository
@Qualifier(value = "userRepository")
@RestResource(exported = false)
public interface UserRepository extends JpaRepository<User, String> {

	public User findByUserIdAndPassword(String userId, String password);
	public User findByUserId(String userId);
	public User findByEmail(String email);
	public User findByExhibitors(Exhibitors e);
	public User findByBuyer(Buyer b);
	public User findByUserIdAndEmail(String userId, String email);
	
	@Query("select u from User u left join fetch u.roles r where u.userId=:userId and u.password=:password and r.id.role=:role")
	public User findByUserIdAndPasswordAndRole(@Param("userId") String userId, @Param("password") String password, @Param("role") Role role);
	
}
