package com.ncf.web.repository.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.ncf.web.entity.Buyer;

@Service
public class BuyerSpecification {

	@PersistenceContext EntityManager em;
	
	public List<Buyer> rp(int page, int count, String sort, String record, String search, Long gId, int status) {
		System.out.println("page:  " + page);
		System.out.println("count : " + count);
		System.out.println("sort : " + sort);
		System.out.println("record : " + record);
		System.out.println("search : " + search);
		System.out.println("gId : " + gId);
		String jpql = "SELECT DISTINCT b.bid, b.companyName, b.logo FROM Buyer b "
				+ "LEFT JOIN b.buyerGenre bg "
				+ "WHERE b.companyName LIKE CONCAT ('%',:search,'%') "
				+ "AND bg.genre.gid = :gId "
				+ "AND b.status = :status "
				+ "ORDER BY b."+record+" "+sort;
		Query query = em.createQuery(jpql);
		query.setParameter("search", search);
		query.setParameter("gId", gId);
		query.setParameter("status", status);
		query.setFirstResult(page);
		query.setMaxResults(count);
		List<Object> list = query.getResultList();
		List<Buyer> resultList = new ArrayList<>();
		for (Object obj : list) {
			Object[] results = (Object[])obj;
			Buyer b = new Buyer();
			b.setBid(Long.parseLong(results[0].toString()));
			b.setCompanyName(results[1].toString());
			b.setLogo(results[2].toString());
			resultList.add(b);
		}
		return resultList;
	}
	
}
