package com.ncf.web.repository.specification;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import com.ncf.web.entity.Category;

@Service
public class CategorySpecification {

	@PersistenceContext EntityManager em;
	
	public List<Category> rp(int page, int count, String sort, String record, String search, int status) {
		String jpql = "SELECT DISTINCT c FROM Category c "
				+ "WHERE c.name LIKE CONCAT('%',:search,'%') "
				+ "AND c.status = :status "
				+ "ORDER BY c."+record+" "+sort;
		TypedQuery<Category> query = em.createQuery(jpql, Category.class);
		query.setParameter("search", search);
		query.setParameter("status", status);
		query.setFirstResult(page);
		query.setMaxResults(count);
		return query.getResultList();
	}
	
}
