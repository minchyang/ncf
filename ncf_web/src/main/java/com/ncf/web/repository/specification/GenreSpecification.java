package com.ncf.web.repository.specification;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import com.ncf.web.entity.Genre;

@Service
public class GenreSpecification {

	@PersistenceContext private EntityManager em;
	
	public List<Genre> rp(int page, int count, String sort, String record, String search, int status) {
		String jpql = "SELECT g FROM Genre g "
				+ "WHERE g.status = :status "
				+ "ORDER BY g."+record+" "+sort;
		TypedQuery<Genre> query = em.createQuery(jpql, Genre.class);
		query.setParameter("status", status);
		query.setFirstResult(page);
		query.setMaxResults(count);
		return query.getResultList();
	}
	
}
