package com.ncf.web.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.web.entity.Exhibitors;

@Repository
@Qualifier(value = "exhibitorsRepository")
@RestResource(exported = false)
public interface ExhibitorsRepository extends JpaRepository<Exhibitors, Long>, JpaSpecificationExecutor<Exhibitors> {

	public Exhibitors findByEidAndStatus(Long eId, int status);
	
}
