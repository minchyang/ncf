package com.ncf.web.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.web.entity.UserRole;

@Repository
@Qualifier(value = "userRoleRepository")
@RestResource(exported = false)
public interface UserRoleRepository extends JpaRepository<UserRole, String> {

}
