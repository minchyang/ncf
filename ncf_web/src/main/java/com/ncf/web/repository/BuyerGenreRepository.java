package com.ncf.web.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.ncf.web.entity.Buyer;
import com.ncf.web.entity.BuyerGenre;

@Repository
@Qualifier(value = "buyerGenreRepository")
@RestResource(exported = false)
public interface BuyerGenreRepository extends JpaRepository<BuyerGenre, Long>, JpaSpecificationExecutor<BuyerGenre> {

	public List<BuyerGenre> findByBuyer(Buyer b);
	public BuyerGenre findByBgId(Long Bgid);
	
}
