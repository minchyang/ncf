package com.ncf.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "CONTENT_GENRE")
@ApiModel
public class ContentGenre {

	@Id
	@ApiModelProperty(value = "content genre id [primary key]")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cg_id")
	@Getter @Setter
	private Long cgId;
	
	@ApiModelProperty(value = "content, 컨텐츠 [Content, entity]")
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "c_id")
	@Getter @Setter
	private Content content;
	
	@ApiModelProperty(value = "genre, 장르 [Genre, entity]")
	@ManyToOne
	@JoinColumn(name = "g_id")
	@Getter @Setter
	private Genre genre;
	
}
