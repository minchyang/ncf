package com.ncf.web.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "REPLY")
@ApiModel
public class Reply {

	@ApiModelProperty(value = "reply id [primary key]")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "r_id")
	@Getter @Setter
	private Long rid;
	
	@ApiModelProperty(value = "description, 내용")
	@Lob
	@Column(name = "description")
	@Getter @Setter
	private String description;
	
	@ApiModelProperty(value = "registration date, 등록일")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "regDate")
	@Getter @Setter
	private Date regDate = new Date();
	
}
