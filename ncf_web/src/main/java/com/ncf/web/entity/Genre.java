package com.ncf.web.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "GENRE")
@ApiModel
public class Genre {

	@ApiModelProperty(value = "genre id [primary key]")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "g_id")
	@Getter @Setter
	private Long gid;
	
	@ApiModelProperty(value = "name, 장르명")
	@Column(name = "name")
	@Getter @Setter
	private String name;
	
	@ApiModelProperty(value = "english name, 영문 장르명")
	@Column(name = "engName")
	@Getter @Setter
	private String engName;
	
	@ApiModelProperty(value = "status, 상태값 [0:사용, 1:미사용]")
	@Column(name = "status", columnDefinition = "INT(1) default '0'")
	@Getter @Setter
	private int status;
	
	@ApiModelProperty(value = "sort, 정렬")
	@Column(name = "sort")
	@Getter @Setter
	private int sort;
	
	@ApiModelProperty(value = "registration date")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "regDate")
	@Getter @Setter
	private Date regDate = new Date();
	
	@ApiModelProperty(value = "update date")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updateDate")
	@Getter @Setter
	private Date updateDate = new Date();
	
}
