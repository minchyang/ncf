package com.ncf.web.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "CONTACT_US")
@ApiModel
public class ContactUs {

	@ApiModelProperty(value = "contactUs id [primary key]")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "c_id")
	@Getter @Setter
	private Long cid;
	
	@ApiModelProperty(value = "questioner, 질문자")
	@OneToOne
	@JoinColumn
	@Getter @Setter
	private User questioner;
	
	@ApiModelProperty(value = "answerer, 답변자")
	@OneToOne
	@JoinColumn
	@Getter @Setter
	private User answerer;
	
	@ApiModelProperty(value = "subject, 제목")
	@Column(name = "subject")
	@Getter @Setter
	private String subject;
	
	@ApiModelProperty(value = "description, 내용")
	@Lob
	@Column(name = "description")
	@Getter @Setter
	private String description;
	
	@ApiModelProperty(value = "status, 상태값 [0:노출, 1:미노출]")
	@Column(name = "status", columnDefinition = "INT(1) default '0'")
	@Getter @Setter
	private int status;
	
	@ApiModelProperty(value = "reply, 답변 [Reply, Entity]")
	@OneToOne
	@JoinColumn(name = "r_id")
	@Getter @Setter
	private Reply reply;
	
	@ApiModelProperty(value = "registration date, 등록일")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "regDate")
	@Getter @Setter
	private Date regDate = new Date();
	
}
