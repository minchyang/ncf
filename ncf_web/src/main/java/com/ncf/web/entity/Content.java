package com.ncf.web.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "CONTENT")
@ApiModel
public class Content {

	@Id
	@ApiModelProperty(value = "content id [primary key]")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "c_id")
	@Getter @Setter
	private Long cid;
	
	@ApiModelProperty(value = "name, 컨텐츠명")
	@Column(name = "name")
	@Getter @Setter
	private String name;
	
	@ApiModelProperty(value = "thumbnail, 썸네일")
	@Column(name = "thumbnail")
	@Getter @Setter
	private String thumbnail;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ApiModelProperty(value = "content genre, 컨텐츠 장르 [List<ContentGenre>, Entity]")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "content")
	@Getter @Setter
	private List<ContentGenre> contentGenre;
	
	@ApiModelProperty(value = "year of production, 제작년도")
	@Column(name = "yearOfProduction", length = 4)
	@Getter @Setter
	private String yearOfProduction;
	
	@ApiModelProperty(value = "content introduction, 컨텐츠 소개")
	@Lob
	@Column(name = "contentIntroduction")
	@Getter @Setter
	private String contentIntroduction;
	
	@ApiModelProperty(value = "content video type, 컨텐츠 영상 유형 [0:youtube, 1:vimeo]")
	@Column(name = "contentVideoType", columnDefinition = "INT(1) default '0'")
	@Getter @Setter
	private int contentVideoType;
	
	@ApiModelProperty(value = "content video url, 홍보 영상 URL")
	@Column(name = "contentVideoUrl")
	@Getter @Setter
	private String contentVideoUrl;
	
	@ApiModelProperty(value = "engName, 영문 컨텐츠명")
	@Column(name = "engName")
	@Getter @Setter
	private String engName;
	
	@ApiModelProperty(value = "engContentIntroduction, 영문 컨텐츠 소개")
	@Lob
	@Column(name = "engContentIntroduction")
	@Getter @Setter
	private String engContentIntroduction;
	
}
