package com.ncf.web.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "USER")
@ApiModel
public class User {

	@Id
	@ApiModelProperty(value = "user id, 아이디")
	@Column(name = "user_id", nullable = false, length = 100)
	@Getter @Setter
	private String userId;
	
	@ApiModelProperty(value = "password, 비밀번호")
	@JsonIgnore
	@Column(name = "password", nullable = false, length = 255)
	@Getter @Setter
	private String password;
	
	@ApiModelProperty(value = "email, 이메일")
	@Column(name = "email", length = 100)
	@Getter @Setter
	private String email;
	
	@ApiModelProperty(value = "join type, [D:기본, N:네이버, K:카카오, G:구글]")
	@Column(name = "joinType")
	@Getter @Setter
	private String joinType;
	
	@ApiModelProperty(value = "role type, 가입 권한 [0:visitor, 1:exhibitors, 2:buyer]")
	@Column(name = "roleType", columnDefinition = "INT(1) default '0'")
	@Getter @Setter
	private int roleType;
	
	@ApiModelProperty(value = "user status ["
			+ "0:가입, 1:정지(강퇴), 2:탈퇴(자진)"
			+ "]")
	@Column(name = "status", columnDefinition = "INT(1) default '0'")
	@Getter @Setter
	private int status;
	
	@ApiModelProperty(value = "visitor, 참관객 [Visitor, entity]")
	@OneToOne
	@JoinColumn(name = "v_id")
	@Getter @Setter
	private Visitor visitor;
	
	@ApiModelProperty(value = "exhibitors, 참가사 [Exhibitors, entity]")
	@OneToOne
	@JoinColumn(name = "e_id")
	@Getter @Setter
	private Exhibitors exhibitors;
	
	@ApiModelProperty(value = "buyer, 바이어 [Buyer, entity]")
	@OneToOne
	@JoinColumn(name = "b_id")
	@Getter @Setter
	private Buyer buyer;
	
	@ApiModelProperty(value = "userRole, 회원권한 [List<UserRole>, entity]")
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "role_user_id", referencedColumnName = "user_id")
	@Getter @Setter
	private List<UserRole> roles;
	
	@ApiModelProperty(value = "registration date")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "regDate")
	@Getter @Setter
	private Date regDate = new Date();
	
	@ApiModelProperty(value = "update date")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updateDate")
	@Getter @Setter
	private Date updateDate = new Date();
	
}
