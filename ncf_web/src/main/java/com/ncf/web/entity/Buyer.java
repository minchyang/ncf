package com.ncf.web.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "BUYER")
@ApiModel
public class Buyer {

	@Id
	@ApiModelProperty(value = "buyer id [primary key]")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "b_id")
	@Getter @Setter
	private Long bid;
	
	@ApiModelProperty(value = "company name, 회사명")
	@Column(name = "companyName")
	@Getter @Setter
	private String companyName;
	
	@ApiModelProperty(value = "country, 국가")
	@Column(name = "country")
	@Getter @Setter
	private String country;
	
	@ApiModelProperty(value = "site")
	@Column(name = "site")
	@Getter @Setter
	private String site;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ApiModelProperty(value = "buyer genre, 바이어 장르 [List<BuyerGenre>, Entity]")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "buyer")
	@Getter @Setter
	private List<BuyerGenre> buyerGenre;
	
	@ApiModelProperty(value = "logo, 로고 이미지")
	@Column(name = "logo")
	@Getter @Setter
	private String logo;
	
	@ApiModelProperty(value = "about us, 회사 소개")
	@Lob
	@Column(name = "aboutUs")
	@Getter @Setter
	private String aboutUs;
	
	@ApiModelProperty(value = "language spoken, 사용언어 [0:kor, 1:usa, 2:chn]")
	@Column(name = "languageSpoken", columnDefinition = "INT(1) default '0'")
	@Getter @Setter
	private int languageSpoken;
	
	@ApiModelProperty(value = "division, 구분 [0:kor buyer, 1:foreign buyer]")
	@Column(name = "division", columnDefinition = "INT(1) default '0'")
	@Getter @Setter
	private int division;
	
	@ApiModelProperty(value = "counseling place, 상담장소 [0:online, 1:site visit]")
	@Column(name = "counselingPlace", columnDefinition = "INT(1) default '0'")
	@Getter @Setter
	private int counselingPlace;
	
	@ApiModelProperty(value = "company address, 회사 주소")
	@Column(name = "companyAddress")
	@Getter @Setter
	private String companyAddress;
	
	@ApiModelProperty(value = "manager name, 담당자명")
	@Column(name = "managerName")
	@Getter @Setter
	private String managerName;
	
	@ApiModelProperty(value = "position, 직책")
	@Column(name = "position")
	@Getter @Setter
	private String position;
	
	@ApiModelProperty(value = "front phone, 휴대폰 앞자리")
	@Column(name = "frontPhone")
	@Getter @Setter
	private String frontPhone;
	
	@ApiModelProperty(value = "phone, 휴대폰 번호")
	@Column(name = "phone")
	@Getter @Setter
	private String phone;
	
	@ApiModelProperty(value = "national code, 국가 번호")
	@Column(name = "nationalCode")
	@Getter @Setter
	private String nationalCode;
	
	@ApiModelProperty(value = "tel, 전화번호")
	@Column(name = "tel")
	@Getter @Setter
	private String tel;
	
	@ApiModelProperty(value = "business card, 명함")
	@Column(name = "businessCard")
	@Getter @Setter
	private String businessCard;
	
	@ApiModelProperty(value = "lang, 언어 [kor, eng]")
	@Column(name = "lang")
	@Getter @Setter
	private String lang;
	
	@ApiModelProperty(value = "privacy agree, 개인정보처리 방침 동의 [0:동의, 1:미동의]")
	@Column(name = "privacyAgree", columnDefinition = "INT(1) default '0'")
	@Getter @Setter
	private int privacyAgree;
	
	@ApiModelProperty(value = "status [0:노출, 1:미노출]")
	@Column(name = "status", columnDefinition = "INT(1) default '1'")
	@Getter @Setter
	private int status;
	
	@ApiModelProperty(value = "registration date, 등록일")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "regDate")
	@Getter @Setter
	private Date regDate = new Date();
	
}
