package com.ncf.web;

public class NcfWebConstants {

	// [TOKEN]
	public static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
	public static final String TOKEN_SUFFIX = "Bearer ";
	
	// [NAVER LOCAL]
	//public static String NAVER_CLIENT_ID = "RlwAW14VctkpYVKnmeHX";
	//public static String NAVER_SECRET_ID = "kahQygTaY7";
	//public static String NAVER_REDIRECT_URL = "http://localhost:10004/naver/callback";
	//public static String NAVER_LOGIN_REDIRECT_URL = "/naver/callback";
	
	// [NAVER PROD]
	public static String NAVER_CLIENT_ID = "72OBYoLzKMvsy6ZkqmOD";
	public static String NAVER_SECRET_ID = "Fg8WBzYQdT";
	public static String NAVER_REDIRECT_URL = "http://ncf2021.co.kr/naver/callback";
	public static String NAVER_LOGIN_REDIRECT_URL = "/naver/callback";
	
	// [KAKAO LOCAL]
	//public static String KAKAO_APP_KEY = "726549ab1c4114b9144053468b4838af";
	//public static String KAKAO_JS_KEY = "ceef4ec9f9ca50c208f1a4b5d58cd391";
	//public static String KAKAO_ADMIN_KEY = "b0d1b4d5ab05d665d33492a3ddc0834e";
	//public static String KAKAO_REDIRECT_URL = "http://localhost:10004/kakao/callback";
	//public static String KAKAO_CLIENT_SECRET = "JjxhOk9FrXITm2hw1AfBrXFWSfmV8F78";
	//public static String KAKAO_API_KEY = "AIzaSyCJZkpfvfG2NoGTzCTgPmK8Jexn";
	
	// [KAKAO PROD]
	public static String KAKAO_APP_KEY = "97590d11e8718f37141c69036461c247";
	public static String KAKAO_JS_KEY = "7b034fffa58287c62cefd009f87a7ac0";
	public static String KAKAO_ADMIN_KEY = "6d6a8950db86765c532eba0e2382c953";
	public static String KAKAO_REDIRECT_URL = "http://175.118.126.44/kakao/callback";
	public static String KAKAO_CLIENT_SECRET = "acyo5QUgjaoEOZ4vaPYfliXk76mIuyb3";
	public static String KAKAO_API_KEY = "7d1768fc5302a2c97bc85bbced7d91f2";
	
	// [GOOGLE LOCAL]
	//public static String GOOGLE_API_KEY = "AIzaSyCJZkpfvfG2NoGTzCTgPmK8Jexn-bIsk2M";
	//public static String GOOGLE_CLIENT_ID = "630930178707-u3opi3t8o7q63svrn6pvqe2rdr3dh6co.apps.googleusercontent.com";
	//public static String GOOGLE_CLIENT_SECRET = "yu5-VOmJvdHaPh-vuzpjw603";
	//public static String GOOGLE_OAUTH_REDIRECT_URL = "http://localhost:10004/google/oauth/callback";
	
	// [GOOGLE PROD]
	public static String GOOGLE_API_KEY = "AIzaSyCMnrK_YvlM8Dc3hHpd9IxcD2ALmyj8Pbg";
	public static String GOOGLE_CLIENT_ID = "487979659625-a40k7jb1m3jel18pigsmgoop2qjoff6k.apps.googleusercontent.com";
	public static String GOOGLE_CLIENT_SECRET = "uHvorxtJQ8HEuJTmSxgL9fVa";
	public static String GOOGLE_OAUTH_REDIRECT_URL = "http://ncf2021.co.kr/google/oauth/callback";
	
}
