package com.ncf.web.controller.auth.exhibitors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.web.entity.User;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.service.anonymous.UserService;
import com.ncf.web.service.auth.exhibitors.ExhibitorsContentService;

@Controller
@RequestMapping(value = "/auth/exhibitors/content")
public class ExhibitorsContentController {

	@Autowired private ExhibitorsContentService exhibitorsContentService;
	@Autowired private UserService userService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(Model model, HttpSession session) {
		User user = (User)session.getAttribute("user");
		model.addAttribute("obj", userService.findOne(user.getUserId()));
		return "/auth/exhibitors/mypage/content/list";
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String form(Model model, HttpSession session, 
			@RequestParam(value = "ecId", required = false) Long ecId) {
		User user = (User)session.getAttribute("user");
		model.addAttribute("obj", userService.findOne(user.getUserId()));
		if (ecId != null) model.addAttribute("ec", exhibitorsContentService.findOne(ecId));
		return "/auth/exhibitors/mypage/content/form";
	}
	
	@RequestMapping(value = "/cud", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse cud(MultipartHttpServletRequest request, HttpSession session) {
		User user = (User)session.getAttribute("user");
		return exhibitorsContentService.cud(user, request);
	}
	
}
