package com.ncf.web.controller.auth.exhibitors.buyer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncf.web.entity.Buyer;
import com.ncf.web.response.BuyerObjectResponse;
import com.ncf.web.service.anonymous.BuyerService;
import com.ncf.web.service.anonymous.CategoryService;
import com.ncf.web.service.anonymous.GenreService;
import com.ncf.web.service.anonymous.UserService;

@Controller
@RequestMapping(value = "/auth/exhibitors/buyer")
public class BuyerController {
	
	@Autowired private BuyerService buyerService;
	@Autowired private UserService userService;
	@Autowired private GenreService genreService;
	@Autowired private CategoryService categoryService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String buyer() {
		return "/auth/exhibitors/buyer/index";
	}
	
	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public String detail(Model model, 
			@RequestParam(value = "bId", required = true) Long bId, 
			@RequestParam(value = "gId", required = false) Long gId, 
			@RequestParam(value = "cId", required = false) Long cId) {
		model.addAttribute("obj", userService.findByBuyer(bId));
		model.addAttribute("genre", genreService.ro(gId));
		model.addAttribute("category", categoryService.ro(cId));
		model.addAttribute("bId", bId);
		return "/auth/exhibitors/buyer/detail";
	}
	
	@RequestMapping(value = "/object", method = RequestMethod.POST)
	@ResponseBody
	public BuyerObjectResponse object(
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "20") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "desc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "regDate") String record, 
			@RequestParam(value = "search", required = false, defaultValue = "") String search, 
			@RequestParam(value = "gId", required = true, defaultValue = "0") Long gId) {
		return buyerService.object(page, count, sort, record, search, gId);
	}
	
	@RequestMapping(value = "/more", method = RequestMethod.GET)
	public String more(Model model, 
			@RequestParam(value = "gId", required = true) Long gId) {
		model.addAttribute("gId", gId);
		return "/auth/exhibitors/buyer/more";
	}
	
	@RequestMapping(value = "/rp", method = RequestMethod.POST)
	@ResponseBody
	public List<Buyer> rp(
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "20") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "desc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "regDate") String record, 
			@RequestParam(value = "search", required = true, defaultValue = "") String search, 
			@RequestParam(value = "gId", required = true) Long gId) {
		return buyerService.rp(page, count, sort, record, search, gId);
	}
	
}
