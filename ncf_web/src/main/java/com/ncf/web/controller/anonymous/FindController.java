package com.ncf.web.controller.anonymous;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncf.web.response.CommonResponse;
import com.ncf.web.service.anonymous.FindService;

@Controller
@RequestMapping(value = "/anonymous/find")
public class FindController {

	@Autowired private FindService findService;
	
	@RequestMapping(value = "/password", method = RequestMethod.GET)
	public String password() {
		return "/anonymous/find/password";
	}
	
	@RequestMapping(value = "password", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse password(HttpServletRequest request) {
		return findService.password(request);
	}
	
}
