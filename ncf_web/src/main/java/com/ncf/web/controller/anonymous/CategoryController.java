package com.ncf.web.controller.anonymous;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncf.web.entity.Category;
import com.ncf.web.service.anonymous.CategoryService;

@Controller
@RequestMapping(value = "/anonymous/category")
public class CategoryController {

	@Autowired private CategoryService categoryService;
	
	@RequestMapping(value = "/rp", method = RequestMethod.POST)
	@ResponseBody
	public List<Category> rp(
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "100") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "asc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "sort") String record, 
			@RequestParam(value = "search", required = true, defaultValue = "") String search, 
			@RequestParam(value = "status", required = true, defaultValue = "0") int status) {
		return categoryService.rp(page, count, sort, record, search, status);
	}
	
}
