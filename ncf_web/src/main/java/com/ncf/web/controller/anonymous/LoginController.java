package com.ncf.web.controller.anonymous;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.SecureRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncf.web.NcfWebConstants;
import com.ncf.web.common.externalAPI.google.GoogleClient;
import com.ncf.web.common.externalAPI.kakao.KakaoClient;
import com.ncf.web.common.externalAPI.naver.NaverClient;
import com.ncf.web.domain.google.GoogleAccessToken;
import com.ncf.web.domain.google.GoogleUserResponse;
import com.ncf.web.domain.kakao.KakaoAccessToken;
import com.ncf.web.domain.kakao.KakaoUserResponse;
import com.ncf.web.domain.naver.NaverAccessToken;
import com.ncf.web.domain.naver.NaverUserResponse;
import com.ncf.web.entity.Role;
import com.ncf.web.entity.User;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.response.UserResponse;
import com.ncf.web.response.result.ResultCode;
import com.ncf.web.service.anonymous.LoginService;
import com.ncf.web.service.anonymous.UserService;

@Controller
public class LoginController {

	@Autowired private UserService userService;
	@Autowired private LoginService loginService;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(
			@RequestParam(value = "type", required = true, defaultValue = "visitor") String type) {
		if (type.equals("exhibitors")) return "/anonymous/user/exhibitors/login";
		else if (type.equals("buyer")) return "/anonymous/user/buyer/login";
		else return "/anonymous/user/visitor/login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse login(HttpServletRequest request, HttpSession session) {
		
		UserResponse ur = loginService.login(request);
		if (ur.getResultCode().equals(ResultCode.SUCCESS)) {
			
			Object obj = session.getAttribute("user");
			
			if (obj != null) {
				session.removeAttribute("user");
				session.removeAttribute("role");
				session.invalidate();
			}
			
			session.setAttribute("user", ur.getUser());
			session.setAttribute("role", ur.getUser().getRoles().get(0).getRole());
			
			return CommonResponse.of("로그인 되었습니다.", ResultCode.SUCCESS);
			
		} else {
			return CommonResponse.of(ur.getMessage(), ResultCode.FAIL);
		}
	}
	
	@SuppressWarnings("deprecation")
	@CrossOrigin
	@RequestMapping(value = "/naver/login", method = RequestMethod.GET)
	public String naverLogin(HttpServletRequest request, HttpSession session) {
		try {
			InetAddress local;
			local = InetAddress.getLocalHost();
			String ip = local.getHostAddress();
			String uri = "";
			if (ip.contains("192.168.")) {
				uri = request.getScheme()+"://"+request.getServerName()+":10004";
			} else {
				uri = request.getScheme()+"://"+request.getServerName();
			}
			String redirectUrl = java.net.URLEncoder.encode(uri+NcfWebConstants.NAVER_LOGIN_REDIRECT_URL);
			String state = generateState();
			
			String url = "https://nid.naver.com/oauth2.0/authorize?client_id=" + NcfWebConstants.NAVER_CLIENT_ID + "&response_type=code&redirect_uri=" + redirectUrl + "&state=" + state;
			request.getSession().setAttribute("state", state);
			return "redirect:"+url;
		} catch (UnknownHostException uhe) {
			uhe.printStackTrace();
			return "/anonymous/error/901";
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/naver/callback", method = RequestMethod.GET)
	public String naverCallback(HttpServletRequest request, Model model, HttpSession session) throws Exception {
		System.out.println("naver callback");
		NaverClient client = new NaverClient(NcfWebConstants.NAVER_CLIENT_ID);
		System.out.println("code : " + request.getParameter("code"));
		NaverAccessToken accessToken = client.getAuth("authorization_code", request.getParameter("state"), request.getParameter("code"));
		System.out.println(accessToken.getAccess_token());
		NaverUserResponse userData = client.getUserData(accessToken.getAccess_token());
		String deviceType = (String) session.getAttribute("deviceType");
		
		if(deviceType.equals("mobile")) model.addAttribute("dType", "mobile");
		else model.addAttribute("dType", "pc");
		
		if(userData != null) {
			
			User user = userService.findByUserIdAndPassword(userData.getResponse().getId(), userData.getResponse().getId());
			
			if(user != null) {
				
				if (user.getStatus() == 1 || user.getStatus() == 2) {
					model.addAttribute("sns_status", "100");
					model.addAttribute("sns_message", "탈퇴된 회원입니다.");
				} else {
					request.getSession().setAttribute("user", user);
					request.getSession().setAttribute("role", user.getRoles().get(0).getRole());
					model.addAttribute("sns_id", user.getUserId());
					model.addAttribute("sns_join_status", "1");
					model.addAttribute("sns_type", "N");
					model.addAttribute("sns_status", "200");
				}
				return "/externalAPI/snsLogin/callback";
			} else {
				model.addAttribute("sns_id", userData.getResponse().getId());
				model.addAttribute("sns_join_status", "0");
				model.addAttribute("sns_type", "N");
				model.addAttribute("sns_status", "200");
				
				return "/externalAPI/snsLogin/callback";
			}
		} else {
			model.addAttribute("sns_status", "200");
			model.addAttribute("sns_message", "회원 정보를 찾을 수 없습니다.");
			
			return "/externalAPI/snsLogin/callback";
		}
		
	}
	
	@CrossOrigin
	@RequestMapping(value = "/kakao/callback", method = RequestMethod.GET)
	public String kakaoCallback(HttpServletRequest request, Model model, HttpSession session) throws Exception {
		System.out.println("kakao callback");
		KakaoClient client = new KakaoClient("auth");
		System.out.println("code : " + request.getParameter("code"));
		KakaoAccessToken accessToken = client.token(request.getParameter("code"));
		client = new KakaoClient("api");
		KakaoUserResponse userData = client.userData(accessToken.getAccess_token());
		String deviceType = (String) session.getAttribute("deviceType");
		
		if(deviceType.equals("mobile")) model.addAttribute("dType", "mobile");
		else model.addAttribute("dType", "pc");
		
		if(userData != null) {
			User user = userService.findByUserIdAndPassword(Long.toString(userData.getId()), Long.toString(userData.getId()));
			
			if(user != null) {
				
				if (user.getStatus() == 1 || user.getStatus() == 2) {
					model.addAttribute("sns_status", "100");
					model.addAttribute("sns_message", "탈퇴된 회원입니다.");
				} else {
					request.getSession().setAttribute("user", user);
					request.getSession().setAttribute("role", user.getRoles().get(0).getRole());
					model.addAttribute("sns_id", userData.getId());
					model.addAttribute("sns_join_status", "1");
					model.addAttribute("sns_type", "K");
					model.addAttribute("sns_status", "200");
				}
				return "/externalAPI/snsLogin/callback";
			} else {
				model.addAttribute("sns_id", userData.getId());
				model.addAttribute("sns_join_status", "0");
				model.addAttribute("sns_type", "K");
				model.addAttribute("sns_status", "200");
				
				return "/externalAPI/snsLogin/callback";
			}
		} else {
			model.addAttribute("sns_status", "200");
			model.addAttribute("sns_message", "회원 정보를 찾을 수 없습니다.");
			
			return "/externalAPI/snsLogin/callback";
		}
		
	}
	
	@CrossOrigin
	@RequestMapping(value = "/google/oauth/callback", method = RequestMethod.GET)
	public String googleOauthCallback(HttpServletRequest request, Model model, HttpSession session) throws Exception {
		System.out.println("google callback");
		GoogleClient client = new GoogleClient("auth");
		System.out.println("code : " + request.getParameter("code"));
		GoogleAccessToken accessToken = client.token(request.getParameter("code"));
		System.out.println("accessToken : " + accessToken.getAccess_token());
		client = new GoogleClient("api");
		GoogleUserResponse userData = client.userData(accessToken.getToken_type(), accessToken.getAccess_token());
		String deviceType = (String) session.getAttribute("deviceType");
		
		if(deviceType.equals("mobile")) model.addAttribute("dType", "mobile");
		else model.addAttribute("dType", "pc");
		
		if(userData != null) {
			
			User user = userService.findByUserIdAndPassword(userData.getId(), userData.getId());
			
			if(user != null) {
				if (user.getStatus() == 1 || user.getStatus() == 2) {
					model.addAttribute("sns_status", "100");
					model.addAttribute("sns_message", "탈퇴된 회원입니다.");
				} else {
					request.getSession().setAttribute("user", user);
					request.getSession().setAttribute("role", user.getRoles().get(0).getRole());
					model.addAttribute("sns_id", user.getUserId());
					model.addAttribute("sns_join_status", "1");
					model.addAttribute("sns_type", "G");
					model.addAttribute("sns_status", "200");
				}
				return "/externalAPI/snsLogin/callback";
			} else {
				model.addAttribute("sns_id", userData.getId());
				model.addAttribute("sns_join_status", "0");
				model.addAttribute("sns_type", "G");
				model.addAttribute("sns_status", "200");
				
				return "/externalAPI/snsLogin/callback";
			}
		} else {
			model.addAttribute("sns_status", "200");
			model.addAttribute("sns_message", "회원 정보를 찾을 수 없습니다.");
			
			return "/externalAPI/naver/callback";
		}
		
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
		
		Object obj = session.getAttribute("user");
		
		if (obj != null) {
			session.removeAttribute("user");
			session.removeAttribute("role");
			session.invalidate();
		}
		
		return "redirect:/";
	}
	
	// CSRF defence token
	// token save(verification)
	public String generateState()
	{
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
		// 130th random number. toString(32)??
	}
	
}
