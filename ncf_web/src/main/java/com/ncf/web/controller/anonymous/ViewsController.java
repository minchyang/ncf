package com.ncf.web.controller.anonymous;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ViewsController {

	@RequestMapping(value = {"/exhibitors"}, method = RequestMethod.GET)
	public String exhibitors() {
		return "/anonymous/views/exhibitors";
	}
	
	@RequestMapping(value = {"/pavilion"}, method = RequestMethod.GET)
	public String pavilion() {
		return "/anonymous/views/pavilion";
	}
	
	@RequestMapping(value = {"/buyer"}, method = RequestMethod.GET)
	public String buyer() {
		return "/anonymous/views/buyer";
	}
	@RequestMapping(value = {"/detail_exhibitors"}, method = RequestMethod.GET)
	public String detail_exhibitors() {
		return "/anonymous/views/detail_exhibitors";
	}
	@RequestMapping(value = {"/detail_buyer"}, method = RequestMethod.GET)
	public String detail_buyer() {
		return "/anonymous/views/detail_buyer";
	}
	@RequestMapping(value = {"/intro"}, method = RequestMethod.GET)
	public String intro() {
		return "/anonymous/views/intro";
	}
	@RequestMapping(value = {"/error404"}, method = RequestMethod.GET)
	public String error404() {
		return "/anonymous/views/error404";
	}
	
	
	@RequestMapping(value = {"/buyer/user_page"}, method = RequestMethod.GET)
	public String buyer_page() {
		return "/anonymous/user/buyer/user_page";
	}
	@RequestMapping(value = {"/exhibitors/user_page"}, method = RequestMethod.GET)
	public String exhibitors_page() {
		return "/anonymous/user/exhibitors/user_page";
	}
	@RequestMapping(value = {"/visitor/user_page"}, method = RequestMethod.GET)
	public String visitor_page() {
		return "/anonymous/user/visitor/user_page";
	}
	
	@RequestMapping(value = {"/buyer/user_modify"}, method = RequestMethod.GET)
	public String buyer_modify() {
		return "/anonymous/user/buyer/user_modify";
	}
	@RequestMapping(value = {"/exhibitors/user_modify"}, method = RequestMethod.GET)
	public String exhibitors_modify() {
		return "/anonymous/user/exhibitors/user_modify";
	}
	@RequestMapping(value = {"/visitor/user_modify"}, method = RequestMethod.GET)
	public String visitor_modify() {
		return "/anonymous/user/visitor/user_modify";
	}
	
	@RequestMapping(value = {"/main/ajax/pc_ajax"}, method = RequestMethod.GET)
	public String pc_bg() {
		return "/anonymous/main/ajax/pc_ajax";
	}
	@RequestMapping(value = {"/main/ajax/mobile_ajax"}, method = RequestMethod.GET)
	public String mobile_bg() {
		return "/anonymous/main/ajax/mobile_ajax";
	}
}
