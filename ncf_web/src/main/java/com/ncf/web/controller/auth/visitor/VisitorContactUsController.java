package com.ncf.web.controller.auth.visitor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncf.web.entity.ContactUs;
import com.ncf.web.entity.User;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.service.anonymous.UserService;
import com.ncf.web.service.auth.visitor.VisitorContactUsService;

@Controller
@RequestMapping(value = "/auth/visitor/contactUs")
public class VisitorContactUsController {

	@Autowired private UserService userService;
	@Autowired private VisitorContactUsService visitorContactUsService;
	
	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String form(Model model, 
			@RequestParam(value = "type", required = true) String type, 
			@RequestParam(value = "id", required = true) Long id) {
		model.addAttribute("type", type);
		model.addAttribute("id", id);
		return "/auth/visitor/contactUs/form"; 
	}
	
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse insert(HttpServletRequest request, HttpSession session) {
		User questioner = (User)session.getAttribute("user");
		return visitorContactUsService.insert(request, questioner);
	}
	
	@RequestMapping(value = "/questioner/list", method = RequestMethod.GET)
	public String qList(Model model, HttpSession session) {
		User user = (User)session.getAttribute("user");
		model.addAttribute("obj", userService.findOne(user.getUserId()));
		return "/auth/visitor/contactUs/questioner/list";
	}
	
	@RequestMapping(value = "/questioner/rp", method = RequestMethod.POST)
	@ResponseBody
	public List<ContactUs> qRp(HttpSession session, 
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "10") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "desc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "regDate") String record, 
			@RequestParam(value = "searchType", required = true, defaultValue = "subject") String searchType, 
			@RequestParam(value = "search", required = true, defaultValue = "") String search) {
		User questioner = (User)session.getAttribute("user");
		return visitorContactUsService.qRp(page, count, sort, record, searchType, search, questioner);
	}
	
}
