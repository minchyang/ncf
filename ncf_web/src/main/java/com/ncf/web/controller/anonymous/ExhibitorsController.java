package com.ncf.web.controller.anonymous;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncf.web.entity.Exhibitors;
import com.ncf.web.response.ExhibitorsObjectResponse;
import com.ncf.web.service.anonymous.CategoryService;
import com.ncf.web.service.anonymous.ExhibitorsService;
import com.ncf.web.service.anonymous.GenreService;
import com.ncf.web.service.anonymous.UserService;

@Controller
@RequestMapping(value = "/anonymous/exhibitors")
public class ExhibitorsController {

	@Autowired private ExhibitorsService exhibitorsService;
	@Autowired private UserService userService;
	@Autowired private GenreService genreService;
	@Autowired private CategoryService categoryService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String exhibitors() {
		return "/anonymous/exhibitors/index";
	}
	
	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public String detail(Model model, 
			@RequestParam(value = "eId", required = true) Long eId, 
			@RequestParam(value = "gId", required = false) Long gId, 
			@RequestParam(value = "cId", required = false) Long cId) {
		model.addAttribute("obj", userService.findByExhibitors(eId));
		model.addAttribute("genre", genreService.ro(gId));
		model.addAttribute("category", categoryService.ro(cId));
		model.addAttribute("eId", eId);
		return "/anonymous/exhibitors/detail";
	}
	
	@RequestMapping(value = "/object", method = RequestMethod.POST)
	@ResponseBody
	public ExhibitorsObjectResponse object(
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "20") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "desc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "regDate") String record, 
			@RequestParam(value = "search", required = false, defaultValue = "") String search, 
			@RequestParam(value = "gId", required = true, defaultValue = "0") Long gId) {
		return exhibitorsService.object(page, count, sort, record, search, gId);
	}
	
	@RequestMapping(value = "/more", method = RequestMethod.GET)
	public String more(Model model, 
			@RequestParam(value = "gId", required = true) Long gId) {
		model.addAttribute("gId", gId);
		return "/anonymous/exhibitors/more";
	}
	
	@RequestMapping(value = "/rp", method = RequestMethod.POST)
	@ResponseBody
	public List<Exhibitors> rp(
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "20") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "desc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "regDate") String record, 
			@RequestParam(value = "search", required = true, defaultValue = "") String search, 
			@RequestParam(value = "gId", required = true) Long gId) {
		return exhibitorsService.rp(page, count, sort, record, search, gId);
	}
	
	@RequestMapping(value = "/ro", method = RequestMethod.POST)
	@ResponseBody
	public Exhibitors ro(
			@RequestParam(value = "eId", required = true) Long eId) {
		return exhibitorsService.ro(eId);
	}
	
	@RequestMapping(value = "/participationArea", method = RequestMethod.POST)
	@ResponseBody
	public List<Exhibitors> participationArea(
			@RequestParam(value = "page", required = true, defaultValue = "0") int page, 
			@RequestParam(value = "count", required = true, defaultValue = "100") int count, 
			@RequestParam(value = "sort", required = true, defaultValue = "desc") String sort, 
			@RequestParam(value = "record", required = true, defaultValue = "regDate") String record, 
			@RequestParam(value = "search", required = false, defaultValue = "") String search, 
			@RequestParam(value = "cId", required = true, defaultValue = "0") Long cId) {
		return exhibitorsService.participationArea(page, count, sort, record, search, cId);
	}
	
}
