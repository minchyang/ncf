package com.ncf.web.controller.anonymous;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/anonymous/participationArea")
public class ParticipationAreaController {

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model, 
			@RequestParam(value = "cId", required = true, defaultValue = "13") int cId) {
		System.out.println(cId);
		if (cId == 13 || cId == 14 || cId == 15 || cId == 16 || cId == 17 || cId == 18 || cId == 19 || cId == 20 ||
				cId == 21 || cId == 22 || cId == 23 || cId == 24 || cId == 25 || cId == 26 || cId == 27 || cId == 28 ||
				cId == 29 || cId == 30) {
			System.out.println("if");
			model.addAttribute("cId", cId);
		} else {
			System.out.println("else");
			model.addAttribute("cId", "13");
		}
		return "/anonymous/participationArea/index";
	}
	
}
