package com.ncf.web.controller.anonymous;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.web.entity.Role;
import com.ncf.web.entity.User;
import com.ncf.web.response.CommonResponse;
import com.ncf.web.response.UserResponse;
import com.ncf.web.response.result.ResultCode;
import com.ncf.web.service.anonymous.UserService;

@Controller
@RequestMapping(value = "/anonymous/user")
public class UserController {

	@Autowired private UserService userService;
	
	@RequestMapping(value = "/cud", method = RequestMethod.POST)
	@ResponseBody
	public UserResponse cud(MultipartHttpServletRequest request) {
		
		UserResponse ur = userService.cud(request);
		String joinType = request.getParameter("joinType");
		if (ur.getResultCode().equals(ResultCode.SUCCESS)) {
			if (joinType.equals("D")) {
				return ur;
			} else {
				request.getSession().setAttribute("user", ur.getUser());
				request.getSession().setAttribute("role", ur.getUser().getRoles().get(0).getRole());
				return ur;
			}
		} else {
			return ur;
		}
	}
	
	@RequestMapping(value = "/join/type", method = RequestMethod.GET)
	public String joinType() {
		return "/anonymous/user/join/sign_user";
	}
	
	@RequestMapping(value = "/join", method = RequestMethod.GET)
	public String join(
			@RequestParam(value = "type", required = true, defaultValue = "visitor") String type, 
			@RequestParam(value = "lang", required = false, defaultValue = "kor") String lang) {
		if (type.equals("exhibitors")) {
			return "/anonymous/user/exhibitors/exhibitors";
		} else if (type.equals("buyer")) {
			if (lang.equals("kor")) {
				return "/anonymous/user/buyer/kor_buyer";
			} else {
				return "/anonymous/user/buyer/eng_buyer";
			}
		} else {
			return "/anonymous/user/visitor/visitor";
		}
	}
	
	@RequestMapping(value = "/duplicateCheck", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse dulicateCheck(
			@RequestParam(value = "column", required = true) String column, 
			@RequestParam(value = "value", required = true) String value) {
		return userService.duplicateCheck(column, value);
	}
	
	@RequestMapping(value = "/join/complete", method = RequestMethod.POST)
	public void complete() {}
	
	@RequestMapping(value = "/join/complete_advertiser", method = RequestMethod.POST)
	public void complete_advertiser() {}
	
	@RequestMapping(value = "/exhibitors/ajax", method = RequestMethod.GET)
	public String ajax() {
		return "/anonymous/user/exhibitors/ajax/contents_add";
	}
	
	@RequestMapping(value = "/json/country.json", method = RequestMethod.GET, produces = "application/json")
	public String country() {
		return "/static/json/country.json";
	}
}
