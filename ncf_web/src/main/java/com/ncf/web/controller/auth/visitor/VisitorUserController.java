package com.ncf.web.controller.auth.visitor;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncf.web.response.CommonResponse;
import com.ncf.web.service.auth.visitor.VisitorUserService;

@Controller
@RequestMapping(value = "/auth/visitor/user")
public class VisitorUserController {

	@Autowired private VisitorUserService visitorUserService;
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse update(HttpServletRequest request) {
		return visitorUserService.update(request);
	}
	
}
