package com.ncf.web.controller.anonymous;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

	@RequestMapping(value = {"/", "/index", "/main"}, method = RequestMethod.GET)
	public String main() {
		return "/anonymous/main/index";
	}
	
	@RequestMapping(value = {"/temporary",}, method = RequestMethod.GET)
	public String temporary() {
		return "/anonymous/main/index";
	}
}