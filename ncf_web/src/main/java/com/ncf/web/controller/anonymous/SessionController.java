package com.ncf.web.controller.anonymous;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncf.web.response.CommonResponse;
import com.ncf.web.response.result.ResultCode;

@Controller
@RequestMapping(value = "/anonymous/session")
public class SessionController {

	@RequestMapping(value = "/lang", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse lang(HttpSession session, 
			@RequestParam(value = "lang", required = true) String lang) {
		if (lang.equals("kr") || lang.equals("한국어")) lang = "kr";
		else lang = "eng";
		session.setAttribute("lang", lang);
		return CommonResponse.of("success", ResultCode.SUCCESS);
	}
	
}
