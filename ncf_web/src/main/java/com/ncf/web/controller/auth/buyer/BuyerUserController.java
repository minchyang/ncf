package com.ncf.web.controller.auth.buyer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.web.response.CommonResponse;
import com.ncf.web.service.auth.buyer.BuyerUserService;

@Controller
@RequestMapping(value = "/auth/buyer/user")
public class BuyerUserController {

	@Autowired private BuyerUserService buyerUserService;
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse update(MultipartHttpServletRequest request) {
		return buyerUserService.update(request);
	}
	
}
