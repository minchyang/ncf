package com.ncf.web.controller.auth.visitor;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ncf.web.entity.User;
import com.ncf.web.service.anonymous.UserService;

@Controller
@RequestMapping(value = "/auth/visitor/mypage")
public class VisitorMypageController {

	@Autowired private UserService userService;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(Model model, HttpSession session) {
		User user = (User)session.getAttribute("user");
		model.addAttribute("obj", userService.findOne(user.getUserId()));
		return "/auth/visitor/mypage/index";
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String edit(Model model, HttpSession session) {
		User user = (User)session.getAttribute("user");
		model.addAttribute("obj", userService.findOne(user.getUserId()));
		return "/auth/visitor/mypage/edit";
	}
	
}
