package com.ncf.web.controller.anonymous;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncf.web.entity.Content;
import com.ncf.web.service.anonymous.ContentService;

@Controller
@RequestMapping(value = "/anonymous/content")
public class ContentController {

	@Autowired private ContentService contentService;
	
	@RequestMapping(value = "/ro", method = RequestMethod.POST)
	@ResponseBody
	public Content ro(
			@RequestParam(value = "cId", required = true) Long cId) {
		return contentService.ro(cId);
	}
	
}
