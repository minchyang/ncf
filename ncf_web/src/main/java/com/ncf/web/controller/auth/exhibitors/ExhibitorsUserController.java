package com.ncf.web.controller.auth.exhibitors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ncf.web.response.CommonResponse;
import com.ncf.web.service.auth.exhibitors.ExhibitorsUserService;

@Controller
@RequestMapping(value = "/auth/exhibitors/user")
public class ExhibitorsUserController {

	@Autowired private ExhibitorsUserService exhibitorsUserService;
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse update(MultipartHttpServletRequest request) {
		return exhibitorsUserService.update(request);
	}
	
}
