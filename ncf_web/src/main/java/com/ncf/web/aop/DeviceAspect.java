package com.ncf.web.aop;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import lombok.Getter;
import lombok.Setter;

@Aspect
@Component
public class DeviceAspect {
	
	@Before("execution(* com.ncf.web.controller.anonymous..*.*(..))"
			+ "|| execution(* com.ncf.web.controller.auth..*.*(..))")
	public void getDeviceCheck() throws Exception {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		Device device = DeviceUtils.getCurrentDevice(request);
		
        String deviceType = "unknown";
        if (device.isNormal()) {
            deviceType = "normal";
        } else if (device.isMobile()) {
            deviceType = "mobile";
        } else if (device.isTablet()) {
            deviceType = "tablet";
        }
        request.getSession().setAttribute("uri", request.getRequestURI());
        request.getSession().setAttribute("deviceType", deviceType);
	}
	
}