package com.ncf.web.aop;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import lombok.Getter;
import lombok.Setter;

@Aspect
@Component
public class SessionAspect {

	@Value("${property.image.server.host}")
	@Getter @Setter
	String imageHost;
	
	@Before("execution (* com.ncf.web.controller..*.*(..))")
	public void sessions() throws Exception {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		request.getSession().setAttribute("imageServer", imageHost);
		String lang = (String)request.getSession().getAttribute("lang");
		if (lang == null) request.getSession().setAttribute("lang", "kr");
		lang = (String)request.getSession().getAttribute("lang");
	}
	
}
