package com.ncf.web.aop;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.ncf.web.entity.Role;
import com.ncf.web.entity.User;

@Aspect
@Component
public class RoleAspect {

	@Before("(execution(* com.ncf.web.controller.auth.visitor..*.*(..))"
			+ "|| execution (* com.ncf.web.service.auth.visitor..*.*(..)))")
	public void beforeRolesVisitor() throws Exception {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		User user = (User)request.getSession().getAttribute("user");
		if (user == null) {
			String msg = "|er|세션이 만료 되었습니다.</br>다시 로그인 해주세요.|er|";
			throw new Exception(msg);
		}
		if (!user.getRoles().get(0).getRole().equals(Role.VISITOR) || user.getStatus() != 0) {
			String msg = "|er|접근 권한이 없습니다.</br>다시 시도 해주세요.|er|";
			throw new Exception(msg);
		}
	}
	
	@Before("(execution(* com.ncf.web.controller.auth.exhibitors..*.*(..))"
			+ "|| execution (* com.ncf.web.service.auth.exhibitors..*.*(..)) "
			+ "|| execution (* com.ncf.web.controller.auth.exhibitors.buyer..*.*(..)) "
			+ "|| execution (* com.ncf.web.service.auth.exhibitors.buyer..*.*(..)))")
	public void beforeRolesExhibitors() throws Exception {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		User user = (User)request.getSession().getAttribute("user");
		if (user == null) {
			String msg = "|er|세션이 만료 되었습니다.</br>다시 로그인 해주세요.|er|";
			throw new Exception(msg);
		}
		if (!user.getRoles().get(0).getRole().equals(Role.EXHIBITORS) || user.getStatus() != 0) {
			String msg = "|er|접근 권한이 없습니다.</br>다시 시도 해주세요.|er|";
			throw new Exception(msg);
		}
	}
	
	@Before("(execution(* com.ncf.web.controller.auth.buyer..*.*(..))"
			+ "|| execution (* com.ncf.web.service.auth.buyer..*.*(..)))")
	public void beforeRolesBuyer() throws Exception {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		User user = (User)request.getSession().getAttribute("user");
		if (user == null) {
			String msg = "|er|세션이 만료 되었습니다.</br>다시 로그인 해주세요.|er|";
			throw new Exception(msg);
		}
		if (!user.getRoles().get(0).getRole().equals(Role.BUYER) || user.getStatus() != 0) {
			String msg = "|er|접근 권한이 없습니다.</br>다시 시도 해주세요.|er|";
			throw new Exception(msg);
		}
	}
	
}
