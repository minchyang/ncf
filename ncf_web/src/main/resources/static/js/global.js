// 공백
var veriBlank = /\s/g;
// 특수문자
var veriSpecial = /[!?@#$%^&*():;+-=~{}<>\_\[\]\|\\\"\'\,\.\/\`\₩]/g;
// 한자 특수문자
var veriCnSpecial = ".*[\u2e80-\u2eff\u31c0-\u31ef\u3200-\u32ff\u3400-\u4dbf\u4e00-\u9fbf\uf900-\ufaff].*";
// 한자
var veriChinese = /一-龥/g;
// 일본어
var veriJapanese = /[ぁ-ゔ]+|[ァ-ヴー]+[々〆〤]/g;
// 숫자
var veriNumber = /[^0-9]/g;
// 한글
var veriKorean = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
// 영문
var veriEnglish = /[a-z|A-Z+]/;
// 핸드폰 and 일반전화
var veriPhone = /^\d{2,3}-\d{3,4}-\d{4}$/;
// 이메일
var veriEmail = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;

function javaDateToString(date, type) {
	var idx = date.indexOf('.');
	var parse = date.substring(0, idx);
	var result = parse.replace('T', ' ');
	if (type == 'yyyy-MM-dd') return result.substring(0, 10);
	else return result;
}
function millisecondToDateTime(millisecond) {
	var date = new Date(Number(millisecond));
	//var dateTime = date.format('yyyy-MM-dd HH:mm:ss');
	var year = date.getFullYear();
	var month = (date.getMonth()+1)>9?''+(date.getMonth()+1):'0'+(date.getMonth()+1);
	var day = date.getDate()>9?''+date.getDate():'0'+date.getDate();
	var hour = date.getHours()+1>9?''+date.getHours():'0'+date.getHours();
	var minute = date.getMinutes()>9?''+date.getMinutes():'0'+date.getMinutes();
	var second = date.getSeconds()>9?''+date.getSeconds():'0'+date.getSeconds();
	
	if (hour.length == 1) {
		return year+'-'+month+'-'+day+' 0'+hour+':'+minute+':'+second;
	} else {
		return year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;
	}
}
$(function(){
    $('.nav_btn').on('click',function(){
        var children = $(this).find('a'),
            hasClass = $(this).hasClass('active'),
            href = children.attr('href');
            children.on('click',function(e){e.preventDefault()})
        if(hasClass){
        	$('body').removeClass('on');
			$(this).removeClass('active');
			$(href).removeClass('on');
			setTimeout(function(){
				$(href).removeClass('active');
			},500)
        }else{
        	$('body').addClass('on');
			$(this).addClass('active');
			$(href).addClass('active');
			setTimeout(function(){
				$(href).addClass('on');
			},10)
        }
    });
    
    $(document).on('click', '.lang', function() {
    	var lang = $(this).text();
    	$.ajax ({type : 'POST', url : '/anonymous/session/lang', async : false, 
    		data : {lang:lang}, 
    		success : function(res) {
    			if (res.resultCode == '200') {
    				location.reload();
    			} else {
    				alert(res.message);
    				return false;
    			}
    		}, 
    		error : function(request, status, error) {
    			alert('서버 에러입니다. 관리자에게 문의 해주세요.\n070-7124-7055\nNCF 온라인 전시관 담당자');
    			return false;
    		}
    	});
    });
})


